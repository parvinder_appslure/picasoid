import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
    TouchableOpacity,
    TextInput,
    Image,
    ImageBackground,
    Linking,
    FlatList,
    Dimensions,
    ActivityIndicator
} from 'react-native';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import React, {Component} from 'react';
import Button from 'react-native-button';
import ImagePicker from 'react-native-image-picker';
import Header from './Header.js';
import StarRating from 'react-native-star-rating';


class AskConfirmation extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            avatarSource: null,
            image:'',
            addmore:[],
            startedadd:0,
            loading:false,
            disabled:false,
            timer: 10
        }        
    }


    componentDidMount(){
      this.interval = setInterval(
        () => this.setState((prevState)=> ({ timer: prevState.timer - 1 })),
        1000
      );
    }

    componentDidUpdate(){
      if(this.state.timer === 0){ 
        this.props.navigation.navigate('Ccavenue')
        clearInterval(this.interval);
        
      }
    }

    componentWillUnmount(){
     clearInterval(this.interval);
    }


    static navigationOptions = ({ navigation }) => {
        return {
              header: () => null,

        }
    }


    showLoading() {
        this.setState({loading: true})
    }

    hideLoading() {
        this.setState({loading: false})
    }


    proceedPay=()=>{
        this.props.navigation.navigate('Ccavenue')
    }

    render() {
        var speciality = GLOBAL.appointmentArray.speciality_detail_array

//        console.log(GLOBAL.reviews)
        return(
            <View style={{flex:1, backgroundColor:'#efefef'}}>
            <Header navigation={this.props.navigation}
            headerName={'CONFIRM BOOKING'}/>

            <ScrollView contentContainerStyle={{flex:1}}>

{/*                        <View style = {{flexDirection:'row',width :'100%'}}>

                            <View>
                                <Image style = {{width :60 ,height :60,borderRadius: 30,margin:10}}
                                       source={{ uri: GLOBAL.appointmentArray.image }}/>


                                <View style = {{backgroundColor:'#800000',borderRadius:4,width:40,height:20,marginTop:2,flexDirection:'row',justifyItems:'center',alignItems:'center', alignSelf:'center'}}>
                                    <Image style = {{width :8 ,height :8,marginLeft:4,resizeMode:'contain'}}
                                           source={require('./star.png')}/>

                                    <Text style={{marginLeft : 5,fontSize : 8,marginTop:3,color :'white',fontFamily:'Konnect-Medium',}}>

                                        {GLOBAL.appointmentArray.ratting}
                                    </Text>
                                </View>
                                {GLOBAL.appointmentArray.doctor_avail_status == 1 && (

                                    <Text style={{fontSize : 9,color :'#3DBA56',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                                        Online
                                    </Text>
                                )}
                                {GLOBAL.appointmentArray.doctor_avail_status != 1 && (

                                    <Text style={{fontSize : 9,color :'red',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                                        Offline
                                    </Text>
                                )}


                            </View>

                            <View>

                                <View style = {{flexDirection:'row',width:'100%'}}>
                                    <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'70%',marginTop:10}}>

                                        {GLOBAL.appointmentArray.name}
                                    </Text>



                                </View>

                                <View style = {{flexDirection:'row'}}>
                                    <Text style={{marginLeft : 5,fontSize : 12,color :'#8F8F8F',height:'auto',fontFamily:'Konnect-Medium',width :'80%', }}>

                                        {speciality}
                                    </Text>


                                </View>




                                <View style = {{flexDirection:'row'}}>
                                    <Image style = {{width :20 ,height :20,resizeMode:'contain'}}
                                           source={require('./location.png')}/>

                                    <Text style={{marginLeft : 5,width:window.width - 150,height:30,fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',}}>

                                        Hospital/Clinic: {GLOBAL.appointmentArray.lat_long_address}
                                    </Text>

                                </View>

                                <View style = {{flexDirection:'row',justifyContent:'space-between'}}>

                                    <View>
                                        <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                            Experience
                                        </Text>
                                        <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                            {GLOBAL.appointmentArray.experience} Years
                                        </Text>
                                    </View>

                                    <View>
                                        <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                            Likes
                                        </Text>
                                        <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                            {GLOBAL.appointmentArray.like}
                                        </Text>
                                    </View>

                                    <View style = {{marginRight:50}}>
                                        <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                            Reviews
                                        </Text>
                                        <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                            {GLOBAL.appointmentArray.total_review}
                                        </Text>
                                    </View>

                                </View>


                                <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',marginBottom:10}}>

                                    Consult for ₹ {GLOBAL.appointmentArray.online_consult_chat_price}/- onwards
                                </Text>

                            </View>

                        </View>
*/}

            <View style={{width:'95%', backgroundColor:'white', height:220, alignSelf:'center', margin:10}}>
            <Text style={{textAlign:'center',fontSize : 18,color :'#9f9f9f',fontFamily:'Konnect-Regular',width :'80%',marginTop:18,alignSelf:'center'}}>
            Confirm booking with <Text style={{textAlign:'center',fontSize : 20,color :'#800000',fontFamily:'Konnect-Medium',width :'80%',marginTop:18,alignSelf:'center'}}>
            {GLOBAL.appointmentArray.name}</Text> at the scheduled time mentioned below:
            </Text>

            <Text style={{textAlign:'center',fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'80%',marginTop:18,alignSelf:'center'}}>
            Date: {GLOBAL.hbooking_date}
            </Text>

            <Text style={{textAlign:'center',fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'80%',marginTop:18,alignSelf:'center'}}>
            Time: {GLOBAL.hbooking_time}
            </Text>

            </View>
            <Text style={{textAlign:'center',fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'80%',marginTop:18,alignSelf:'center'}}>
            We will redirect you to the payment page in {this.state.timer} seconds automatically.
            </Text>

            <Button
                style={{padding:15,marginTop:'10%',fontSize: 20, color: 'white',backgroundColor:'#800000',
                width:'80%',height:45,fontFamily:'Konnect-Medium',borderRadius:30, alignSelf:'center'}}
                styleDisabled={{color: 'black',backgroundColor:'grey'}}
                disabled={this.state.disabled}
                onPress={() => this.proceedPay()}>
                PROCEED TO PAYMENT
            </Button>

            <TouchableOpacity style={{marginTop:50,width :'90%',alignSelf:'center',}}
            onPress={()=> this.props.navigation.goBack()}>
            <Text style={{textAlign:'center',fontSize : 18,color :'#3A3A3A',
            fontFamily:'Konnect-Medium', 
            borderBottomColor:'#3A3A3A', borderBottomWidth:1}}>
            Go back to select another date and time
            </Text>
            </TouchableOpacity>
            </ScrollView>
            </View>
        );
    }
}

export default AskConfirmation;