import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    Platform,
    TextInput,
    View,
    Image,
    Alert,
    AsyncStorage,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    FlatList
} from 'react-native';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import Button from 'react-native-button';
import ImagePicker from 'react-native-image-picker';
import DatePicker from 'react-native-datepicker';
import moment from 'moment';
import Header from './Header.js';
import { Dropdown } from 'react-native-material-dropdown';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
const options = {
    title: 'Select Avatar',
    maxWidth : 500,
    maxHeight : 500,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
var radio_props = [
  {label: 'Male', value: 0 },
  {label: 'Female', value: 1 },
  {label: 'Transgender', value: 2 }

 ];

var radio_propss = [
 {label: 'Single', values: 0 },
 {label: 'Married', values: 1 },
];


type Props = {};
export default class EditProfile extends Component {
    state = {
        text: '',
        passwordtext :'',
        isSecure : true,
        username: '',
        city:'',
        address:'',
        password: '',
        status :'',
        ipAdd : '',
        name:'',
        care:'',
        date:'',
        mobile : '',
        description:'',
        value:GLOBAL.mygenderedit,
        values:GLOBAL.mymaritaledit,
        newsHeading :[],
        loading:'',
        states:'',
        results: [],
        avatarSource:'',
        image :'',username:'',
        rdata:[{'value':'Brother', 'id':'1'},
        {'value':'Sister', 'id':'2'},
        {'value':'Uncle', 'id':'3'},
        {'value':'Aunt', 'id':'4'},
        {'value':'Grandfather', 'id':'5'},
        {'value':'Grandmother', 'id':'6'},
        {'value':'Daughter-in-law', 'id':'7'},
        {'value':'Son-in-law', 'id':'8'},
        {'value':'Brother-in-law', 'id':'9'},
        {'value':'Nephew', 'id':'10'},
        {'value':'Niece', 'id':'11'},
        {'value':'Cousin', 'id':'12'},
         ],
         rcity:'', relation:'', flag:0


    };

    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
            animations: {
                setRoot: {
                    waitForRender: false
                }
            }
        }
    }



    showLoading() {
        this.setState({loading: true})
    }

    getNewsUpdate(){
        this.showLoading()
        const url = GLOBAL.BASE_URL +  'get_profile'


        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                user_id : GLOBAL.user_id,

            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                this.hideLoading()
            //  console.log(JSON.stringify(responseJson))
                if (responseJson.status == true) {

                  var getgender = responseJson.user_detail.gender
                 if(getgender =='Male'){
                   this.setState({value:0})
                 }else if(getgender == 'Female'){
                   this.setState({value:1})
                 }else if (getgender =='Transgender') {
                   this.setState({value:2})
                 }
                    this.setState({name: responseJson.user_detail.name,
                        getData :responseJson.user_detail,
                        address: responseJson.user_detail.address,
                        mobile: responseJson.user_detail.mobile,
                        care : responseJson.user_detail.co,
                        description :responseJson.user_detail.email,
                        states: responseJson.user_detail.state,
                        image :responseJson.user_detail.image,
                        relation: responseJson.user_detail.relationship,
                        username: responseJson.user_detail.username,
                        date: responseJson.user_detail.dob,
                        city: responseJson.user_detail.city,
                        rcity: responseJson.user_detail.city
                    })
//                    this.setState({name: responseJson.user_detail.name})                    
                    GLOBAL.profileImage = responseJson.user_detail.image
                    this.getCities(responseJson.user_detail.state_id)
                }else {
                    alert('No Data Found')
                }
            })
            .catch((error) => {
                console.error(error);
            });

    }


        getRelations(){
            const url =  GLOBAL.BASE_URL  + 'relationships'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },


                body: JSON.stringify({
                        "key":"relation"
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
              //      alert(JSON.stringify(responseJson))
                    if (responseJson.status == true) {
//                        console.log('yes')
                        var rece = responseJson.relation
                        const transformed = rece.map(({ id, name }) => ({ label: name, value: id }));
                        console.log(transformed)
                        this.setState({rdata:transformed})
//                        arrayholder = responseJson.list

                    }else{
                        this.setState({rdata:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });
    }

    changeImage=()=>{
        ImagePicker.showImagePicker(options, (response) => {
//            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };

                GLOBAL.profileImage = response.uri
                // You can also display the image using data:
                // const source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({
                    avatarSource: source,
                });
                this.setState({flag:1})
            }
        });

    }



    _handleStateChange = state => {
        this.setState({newsHeading:GLOBAL.array})
    };


        getCities=(value)=>{
            const url =  GLOBAL.BASE_URL  + 'city_list'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    "state_id":value
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.status == true) {
//                        console.log('yes')
                        var rece = responseJson.list
                        const transformed = rece.map(({ id, city_name }) => ({ label: city_name, value: id }));
//                        console.log(transformed)
                        this.setState({resultcities:transformed})
//                        arrayholder = responseJson.list

                    }else{
//                        this.setState({resultstates:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });

    }

    buttonClickListener = () =>{


        var gender =''
          if(this.state.value==0){
            gender ='Male'
          }else if (this.state.value==1) {
            gender ='Female'
          }else if (this.state.value==2) {
            gender ='Transgender'
          }

        var mstatus =''
        if(this.state.values == 0){
          mstatus='Single'
        }else {
          mstatus ='Married'
        }
        console.log(mstatus)
        if (this.state.name == ''){
            alert('Please Enter Name')
        } else if (this.state.description == ''){
            alert('Please Enter Email')
        }


        else {
               this.showLoading()
            const url = GLOBAL.BASE_URL +  'update_profile'
            const data = new FormData();
            data.append('user_id', GLOBAL.user_id);
            data.append('name', this.state.name);
            data.append('email', this.state.description);
            data.append('gender', gender);
            data.append('dob', this.state.date);
            data.append('flag',this.state.flag);
            data.append('marital_status', mstatus);
            data.append('address', this.state.address);
            data.append('city', this.state.rcity)
            data.append('co', this.state.care);
            data.append('relationship', this.state.relation)
            data.append('area','')
//            console.log(data)
            // you can append anyone.
            data.append('image', {
                uri: GLOBAL.profileImage,
                type: 'image/jpeg', // or photo.type
                name: 'image.png'
            });
            fetch(url, {
                method: 'post',
                body: data,
                headers: {
                    'Content-Type': 'multipart/form-data',
                }

            }).then((response) => response.json())
                .then((responseJson) => {
                           this.hideLoading()
//                    alert(JSON.stringify(responseJson))
                    const { navigation } = this.props;
                    navigation.goBack();

                    alert('Profile Updated Successfully.')



                });
        }

    }
    delete = (index) => {
        alert(index)
        let { newsHeading } = this.state;
        let targetPost = newsHeading[index];

        // Flip the 'liked' property of the targetPost


        var array = [...this.state.newsHeading]; // make a separate copy of the array

        if (index !== -1) {
            array.splice(index, 1);
            this.setState({newsHeading: array});
            GLOBAL.array = array
        }

    }




    componentWillMount() {
        this.getNewsUpdate()
        this.getRelations()

    }

    componentWillUnmount() {

    }

    handleConnectionChange = (isConnected) => {

        this.setState({ status: isConnected });
        if (this.state.status == false){
            alert('You are not connected to Internet')
        }
        console.log(`is connected: ${this.state.status}`);
    }

    getIndex = (index) => {

        this.setState({relation:this.state.rdata[index].label})
//        alert(this.state.relation)
    }


    getIndexss = (index) => {

        this.setState({rcity:this.state.resultcities[index].label})
//        alert(this.state.rcity)

    }



    hideLoading() {
        this.setState({loading: false})
    }

    componentDidMount(){
//      this.getNewsUpdate()

        this.props.navigation.addListener('willFocus',this._handleStateChange);
        this.setState({newsHeading:GLOBAL.array})
    }


    render() {
        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }


        return (
            <View style={styles.container}>

            <Header navigation={this.props.navigation}
            headerName={'EDIT PROFILE'}
            /> 

                <View style={{flex:1,marginTop:0,width:window.width,}}>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

{/*                        <View style = {{flexDirection:'row',width:window.width,marginTop:50}}>

                            <TouchableOpacity style = {{width :40 ,height : 40, zIndex:1 }}
                                              onPress={() => this.props.navigation.goBack()}>
                                <Image style = {{width :30 ,height : 30 ,marginLeft: 10,resizeMode: 'contain'}}
                                       source={require('./back-arrow.png')}/>

                            </TouchableOpacity>

                            <Text style = {{marginLeft:30,fontSize: 20,color:'#800000',fontFamily: 'Konnect-Medium', width:'70%', marginTop:3}}>
                                Edit Profile
                            </Text>
                        </View>
*/}



                        <View style = {{flexDirection:'column',width:window.width,marginTop:40, alignSelf:'center',alignItems:'center'}}>
                            <TouchableOpacity activeOpacity={0.99} onPress={()=> this.changeImage()}
                            >

                                {this.state.avatarSource != '' && (

                                    <Image style = {{width :100 ,height : 100 ,borderRadius:50,borderColor:'#800000', borderWidth:1}}
                                           source={this.state.avatarSource} />
                                )}
                                {this.state.avatarSource == '' && (

                                    <Image style = {{width :100 ,height : 100 ,borderRadius:50,borderColor:'#800000', borderWidth:1}}
                                           source={{uri:this.state.image}}/>
                                )}

                            </TouchableOpacity>

                            <Text style = {{marginLeft:10,fontSize: 20,color:'grey',marginTop:25, fontFamily:'Konnect-Regular'}}>
                                Edit Profile Pic
                            </Text>
                        </View>


                    <View style={{width:window.width -40,alignSelf:'center', marginTop:40, }}>
                      <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey'}}>FULL NAME</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular'  }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Enter Name"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            editable={true}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.name}
                            onChangeText={(text) => this.setState({name:text})}
                        />
                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>ADDRESS</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular'  }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Address"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.address}
                            onChangeText={(text) => this.setState({address:text})}
                        />

                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>STATE</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular'  }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="State"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            editable={false}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.states}
                            onChangeText={(text) => this.setState({states:text})}
                        />


                        <View style = {{width:'100%',marginTop:10,flexDirection:'column', marginBottom:20}}>

                            <Dropdown containerStyle={{width:'100%', height:50, marginTop:-20,}}
                                      fontSize={18}
                                      labelFontSize={13}
                                      dropdownPosition = {-4.2}
                                      onChangeText ={ (value,index) => this.getIndexss(index) }
                                      baseColor={'lightgrey'}
                                      label={'CITY'}
                                      value={this.state.city}
                                      data={this.state.resultcities}
                                      itemTextStyle={{fontFamily:'Konnect-Regular',}}
                                      labelTextStyle={{fontFamily:'Konnect-Regular',}}
                                      style={{fontFamily:'Konnect-Regular'}}
                        />
                        </View>

                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:20}}>EMAIL</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular'  }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Enter Email"
                            placeholderTextColor = 'grey'
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            editable={false}
                            onChangeText={(text) => this.setState({description:text.replace(/\s+/g,'')})}
                            value = {this.state.description}
                        />


                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>C/O</Text>
                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular'  }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="C/O"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.care}
                            onChangeText={(text) => this.setState({care:text})}
                        />

                        {this.state.care == '' && (
                            <View style={{height:1}}/>
                            )}

                        {this.state.care!='' && (
                            <View style = {{width:'100%',flexDirection:'column', marginBottom:20}}>

                            <Dropdown containerStyle={{width:'100%', height:50, marginTop:-10}}
                                      fontSize={18}
                                      labelFontSize={13}
                                      dropdownPosition = {-4.2}
                                      baseColor={'lightgrey'}
                                      onChangeText ={ (value,index) => this.getIndex(index) }
                                      label={'RELATION'}
                                      value={this.state.relation}
                                      data={this.state.rdata}
                                      itemTextStyle={{fontFamily:'Konnect-Regular',}}
                                      labelTextStyle={{fontFamily:'Konnect-Regular',}}
                                      style={{fontFamily:'Konnect-Regular'}}


                            />

                            </View>
    )}



                        <View style = {{width:'90%',flexDirection:'column'}}>
                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>GENDER</Text>
                         <View style={{marginTop:13}}>
                        <RadioForm
                            radio_props={radio_props}
                            initial={this.state.value}
                            buttonSize={10}
                            buttonColor={'#800000'}
                            formHorizontal={true}
                            buttonOuterColor = {'#800000'}
                            selectedButtonColor = {'#800000'}
                            animation={false}
                            labelColor={'grey'}
                            buttonStyle={{marginTop:20}}
                            buttonWrapStyle={{marginTop:20}}
                            labelStyle = {{fontSize:16,fontFamily:'Konnect-Regular',paddingLeft:10, paddingRight:10,color:'grey'}}
                            onPress={(value) => {this.setState({value:value})}}
                        />
                        </View>

                        </View>

                        <View style = {{backgroundColor:'#f3f3f4',height:1.2,width:'100%',marginTop:10}}>

                        </View>


                        <View style = {{width:'90%',marginTop:'2%',flexDirection:'column'}}>
                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>MARITAL STATUS</Text>
                        <View style={{marginTop:13}}>
                        <RadioForm
                            radio_props={radio_propss}
                            initial={this.state.values}
                            buttonSize={10}
                            buttonColor={'#800000'}
                            buttonOuterColor = {'#800000'}
                            selectedButtonColor = {'#800000'}
                            animation={false}
                            formHorizontal={true}
                            labelColor={'black'}
                            buttonStyle={{marginTop:20}}
                            buttonWrapStyle={{marginTop:20}}
                            labelStyle = {{fontSize:16,fontFamily:'Konnect-Regular',paddingLeft:10, paddingRight:10, color:'grey'}}
                            onPress={(value, key) => {this.setState({values:key})}}
                        />
                        </View>

                        </View>

                        <View style = {{backgroundColor:'#f3f3f4',height:1,width:'100%',marginTop:10}}>

                        </View>

                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:20}}>MOBILE</Text>

                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular'  }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Mobile Number"
                            placeholderTextColor = 'black'
                            maxLength={35}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.mobile}
                            editable={false}
                            onChangeText={(text) => this.setState({mobile:text})}
                        />

                        <View style = {{width:'90%',marginTop:0,flexDirection:'column'}}>
                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>DATE OF BIRTH</Text>

                        <DatePicker
                          style={{width: 200,}}
                          date={this.state.date}
                          mode="date"
                          showIcon={false}
                          placeholder={this.state.dob}
                          format="DD-MM-YYYY"
                          minDate="01-01-1950"
                          maxDate= {moment().format('DD-MM-YYYY')}
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          customStyles={{
                            dateInput: {
                              marginLeft: -110, borderWidth:0, color:'black'
                            },
                            dateText:{
                                fontFamily:'Konnect-Regular', fontSize:16
                            }                            
                          }}
                          onDateChange={(date) => {
                            this.setState({date: date})
                          }}
                        />
                        </View>
                        <View style = {{backgroundColor:'#f3f3f4',height:1,width:'100%',marginTop:0}}>

                        </View>


                        <View style = {{width:'100%',marginTop:0,flexDirection:'column', marginTop:20}}>
                     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey', marginTop:10}}>PiCaSoid ID</Text>
                        <TextInput
                            style={{ height: 50, borderColor: '#f3f3f4',fontSize:18, borderBottomWidth: 1, marginTop:0 ,marginBottom: 20 ,width:'100%',color:'black',fontFamily:'Konnect-Regular'  }}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Mobile Number"
                            placeholderTextColor = 'black'
                            maxLength={35}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {GLOBAL.pica_id}
                            editable={false}
                            onChangeText={(text) => this.setState({mobile:text})}
                        />

                        </View>

                    <TouchableOpacity style={{marginTop:'10%',alignSelf:'center',marginBottom:'10%'}}
                    onPress={() => this.buttonClickListener()}>
                    <View style = {{backgroundColor:'#800000',height:55,borderRadius:27.5,alignSelf:'center',width:300,
                        borderBottomWidth: 0,
                        shadowColor: 'black',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        flexDirection:'row'}}>
                        <Text style= {{width:'100%',alignSelf:'center',textAlign:'center',fontSize:20,fontFamily:'Konnect-Medium',color:'white',padding:11}} >
                            UPDATE
                        </Text>

                        <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginLeft:-50,alignSelf:'center'}}
                               source={require('./right.png')}/>
                    </View>
                    </TouchableOpacity>


                        </View>



                    </KeyboardAwareScrollView>

                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,

    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    }
})