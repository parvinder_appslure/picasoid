import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    SafeAreaView,
    AsyncStorage,
    Platform,
    ImageBackground
} from 'react-native';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import Button from 'react-native-button';
import { TextField } from 'react-native-material-textfield';
type Props = {};
import moment from 'moment';
var DeviceInfo = require('react-native-device-info');

var randomString = require('random-string');
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import DatePicker from 'react-native-datepicker';
import { Dropdown } from 'react-native-material-dropdown';



import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class Register extends Component {
    state = {
        text: '',
        passwordtext :'',
        isSecure : true,
        username: '',
        password: '',
        email : '',
        mobile : '',
        status : '',
        iPAddress : '',
        loading:'',
        results: [],
        company:'',rstate:'',resultcities:[],
        visible:false,value: 0,care:'',date:"",values:0,address:'',relation:'',rcity:'',
        rdata:[]

    };

    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
            animations: {
                setRoot: {
                    waitForRender: false
                }
            }
        }
    }



    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }


    getIndex = (index) => {

        this.setState({relation:this.state.rdata[index].label})
//        alert(this.state.relation)
    }

    getIndexs = (index) => {

        this.setState({rstate:this.state.resultstates[index].label})
//        alert(this.state.rstate)
//        alert(this.state.resultstates[index].value)
        this.timeoutCheck = setTimeout(() => {
                this.getCities(this.state.resultstates[index].value);
            }, 400);

    }

    getIndexss = (index) => {
        if(this.state.resultcities[index].label == ''){
        this.setState({rcity:''})            
        }else{            
        this.setState({rcity:this.state.resultcities[index].label})
        }
//        alert(this.state.rcity)

    }


    getCities=(value)=>{
//        alert(value+'asdsa')
                    const url =  GLOBAL.BASE_URL  + 'city_list'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },


                body: JSON.stringify({
                    "state_id":value
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
//                    alert(JSON.stringify(responseJson.list))
                    if (responseJson.status == true) {
//                        console.log('yes')
                        var rece = responseJson.list
                        const transformed = rece.map(({ id, city_name }) => ({ label: city_name, value: id }));
                      //  console.log(transformed)
                        this.setState({resultcities:transformed})
//                        arrayholder = responseJson.list

                    }else{
//                        this.setState({resultstates:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });

    }

    getRelations(){
            const url =  GLOBAL.BASE_URL  + 'relationships'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },


                body: JSON.stringify({
                        "key":"relation"
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
              //      alert(JSON.stringify(responseJson))
                    if (responseJson.status == true) {
//                        console.log('yes')
                        var rece = responseJson.relation
                        const transformed = rece.map(({ id, name }) => ({ label: name, value: id }));
                        console.log(transformed)
                        this.setState({rdata:transformed})
//                        arrayholder = responseJson.list

                    }else{
                        this.setState({rdata:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });
    }

    componentDidMount(){
        this.getRelations()

        var valuesf =  AsyncStorage.getItem('token');
        valuesf.then((f)=> {
            GLOBAL.firebaseToken = f
        })
        
            const url =  GLOBAL.BASE_URL  + 'state_list'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },


                body: JSON.stringify({
                        "key":"state"
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
//                    alert(JSON.stringify(responseJson.list))
                    if (responseJson.status == true) {
//                        console.log('yes')
                        var rece = responseJson.list
                        const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
                    //    console.log(transformed)
                        this.setState({resultstates:transformed})
//                        arrayholder = responseJson.list

                    }else{
                        this.setState({resultstates:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });


    }


    login = () => {
//        alert(this.state.value + '---'+this.state.values)
        GLOBAL.myname = this.state.username
        GLOBAL.mymobile= this.state.mobile
        GLOBAL.myemail= this.state.email
        GLOBAL.mypassword= this.state.password
        GLOBAL.mydeviceID= DeviceInfo.getUniqueId()
        GLOBAL.mydeviceType= Platform.OS
        GLOBAL.mydeviceToken= GLOBAL.firebaseToken
        GLOBAL.mymodel_name= ''
        GLOBAL.mycarrier_name= ''
        GLOBAL.mydevice_country= ''
        GLOBAL.mydevice_memory= ''
        GLOBAL.mycareof = this.state.care
        GLOBAL.mycity =this.state.rcity
        GLOBAL.myaddress = this.state.address
        GLOBAL.myrelation = this.state.relation
        GLOBAL.mystate = this.state.rstate
        GLOBAL.mydob= this.state.date
            if(this.state.value==0){
            GLOBAL.mygender= 'Male'
            }else if(this.state.value==1){
            GLOBAL.mygender= 'Female'

            }else if(this.state.value==2){
            GLOBAL.mygender= 'Transgender'

            }

            if(this.state.values == 0){
                GLOBAL.mymarital = 'Single'
            }else if(this.state.values ==1){
                GLOBAL.mymarital = 'Married'

            }


        GLOBAL.is_refer_verify = this.state.values
        GLOBAL.referral_code_other = this.state.company
//EmailValidator.validate("test@email.com");
        if (this.state.username == ''){
            alert('Please Enter Username')
        }
        else if (this.state.mobile == ''){
            alert('Please Enter Mobile')
        }   else if (this.state.email == ''){
            alert('Please Enter Email')
        }


        else if (this.state.password == '') {
            alert('Please Enter Password')
        }  else {
            var x = randomString({
                length: 4,
                numeric: true,
                letters: false,
                special: false,
            });
            if (this.state.username == ''){
                alert('Please Enter Mobile Number')
            }    else {
                const url = GLOBAL.BASE_URL +  'otp'

                this.showLoading()
                fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        email : this.state.email,
                        mobile: this.state.mobile,
                        otp:x
                    }),
                }).then((response) => response.json())
                    .then((responseJson) => {
                        this.hideLoading()

                        if (responseJson.status == true) {
                            // alert(JSON.stringify(responseJson))
                            GLOBAL.otps =  x;
                            GLOBAL.fmobile= this.state.mobile;
                            GLOBAL.isScreen = '0';
                            //  alert(responseJson.msg)
                            this.props.navigation.replace('Otp')
                        }else {
                            alert(responseJson.msg)
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });
            }
        }
    }




    render() {

var radio_props = [
  {label: 'Male', value: 0 },
  {label: 'Female', value: 1 },
  {label: 'Transgender', value: 2 }

];

var radio_propss = [
  {label: 'Single', values: 0 },
  {label: 'Married', values: 1 },
];

        let { mobile } = this.state;
        let { email } = this.state;
        let { username } = this.state;
        let { password } = this.state;
        let { company } = this.state;
        let { care } = this.state;
        let { address } = this.state;

        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

                <View style={styles.container}>
                    <KeyboardAwareScrollView keyboardShouldPersistTaps='handled'>

                        <ImageBackground source={require('./background.png')}style={{width: '100%', height:'100%'}}>

                            <Image style = {{width :300 ,height: 140,alignSelf:'center',marginTop:'5%',resizeMode: 'contain'}}
                                   source={require('./picalogo.png')}/>


                            <Text style={{textAlign:'left', marginLeft:30,width:'100%',color :'black',fontFamily:'Konnect-Medium',fontSize: 30,marginTop:10}} >

                                Create Account
                            </Text>


                            <View style = {{marginLeft:'5%',width:'80%',marginTop:10,flexDirection:'row'}}>

                                <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginTop:28}}
                                       source={require('./username.png')}/>

                                <View style = {{width:'90%',marginLeft:'2%'}}>

                                    <TextField
                                        label= 'FULL NAME'
                                        value={username}
                                        lineWidth={0}
                                        fontSize={15}
                                        activeLineWidth={0}
                                        baseColor={'#000000'}
                                        onChangeText={ (username) => this.setState({ username }) }
                                        tintColor = {'#800000'}
                                    />

                                </View>


                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>
                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'3%',flexDirection:'column'}}>
                                <Text style= {{fontSize:18,color:'black', fontFamily:'Konnect-Regular'}} >
                                    Gender
                                </Text>
                             <View style={{marginTop:10}}>
                            <RadioForm
                                radio_props={radio_props}
                                initial={0}
                                buttonSize={10}
                                buttonColor={'#800000'}
                                formHorizontal={true}
                                buttonOuterColor = {'#800000'}
                                selectedButtonColor = {'#800000'}
                                animation={false}
                                labelColor={'black'}
                                buttonStyle={{marginTop:20}}
                                buttonWrapStyle={{marginTop:20}}
                                labelStyle = {{fontSize:16,fontFamily:'Konnect-Regular',paddingLeft:10, paddingRight:10,color:'black'}}
                                onPress={(value) => {this.setState({value:value})}}
                            />
                            </View>

                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:8}}>

                            </View>


                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'3%',flexDirection:'column'}}>
                                <Text style= {{fontSize:18,color:'black', fontFamily:'Konnect-Regular'}} >
                                Marital Status
                                </Text>
                            <View style={{marginTop:10}}>
                            <RadioForm
                                radio_props={radio_propss}
                                initial={0}
                                buttonSize={10}
                                buttonColor={'#800000'}
                                buttonOuterColor = {'#800000'}
                                selectedButtonColor = {'#800000'}
                                animation={false}
                                formHorizontal={true}
                                labelColor={'black'}
                                buttonStyle={{marginTop:20}}
                                buttonWrapStyle={{marginTop:20}}
                                labelStyle = {{fontSize:16,fontFamily:'Konnect-Regular',paddingLeft:10, paddingRight:10, color:'black'}}
                                onPress={(value, key) => {this.setState({values:key})}}
                            />
                            </View>

                            </View>
                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:8}}>

                            </View>



                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'0.5%',flexDirection:'row'}}>

                                <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginTop:30}}
                                       source={require('./care.png')}/>

                                <View style = {{width:'90%',marginLeft:'2%'}}>

                                    <TextField
                                        label= 'C/O'
                                        value={care}
                                        lineWidth={0}
                                        fontSize={15}                                        
                                        activeLineWidth={0}
                                        onChangeText={ (care) => this.setState({ care }) }
                                        tintColor = {'#800000'}
                                        baseColor={'#000000'}

                                    />

                                </View>


                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>

                            {this.state.care!='' && (
                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'3%',flexDirection:'column'}}>

                            <Dropdown containerStyle={{width:'100%', height:50, marginTop:-10}}
                                      fontSize={15}
                                      fontColor={'#000000'}
                                      labelFontSize={13}

                                      onChangeText ={ (value,index) => this.getIndex(index) }

                                      label={'Select Relationship'}
                                      data={this.state.rdata}
                            />
                        </View>



                            )}



                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'5%',flexDirection:'column'}}>
                                <Text style= {{fontSize:18,color:'#000000', fontFamily:'Konnect-Regular'}} >
                                Date of Birth
                                </Text>

                            <DatePicker
                              style={{width: 200,}}
                              date={this.state.date}
                              mode="date"
                              showIcon={false}
                              placeholder={this.state.dob}
                              format="YYYY-MM-DD"
                              minDate="1950-01-01"
                              maxDate= {moment().format('YYYY-MM-DD')}
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              customStyles={{
                                dateInput: {
                                  marginLeft: -120, borderWidth:0, color:'black', fontSize:15
                                }
                              }}
                              onDateChange={(date) => {this.setState({date: date})}}
                            />
                            </View>
                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>


                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'1%',flexDirection:'row'}}>

                                <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginTop:30}}
                                       source={require('./address.png')}/>

                                <View style = {{width:'90%',marginLeft:'2%'}}>

                                    <TextField
                                        label= 'ADDRESS'
                                        value={email}
                                        lineWidth={0}
                                        activeLineWidth={0}
                                        baseColor={'#000000'}
                                        fontSize={15}
                                        onChangeText={ (address) => this.setState({ address }) }
                                        tintColor = {'#800000'}
                                    />

                                </View>


                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>


                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'3%',flexDirection:'column'}}>

                            <Dropdown containerStyle={{width:'100%', height:50, marginTop:-10}}
                                      fontSize={15}
                                      labelFontSize={13}
                                      dropdownPosition = {-4.2}
                                      onChangeText ={ (value,index) => this.getIndexs(index) }
                                      baseColor={'#000000'}
                                      label={'Select State'}
                                      data={this.state.resultstates}
                            />
                        </View>


                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'5%',flexDirection:'column'}}>

                            <Dropdown containerStyle={{width:'100%', height:50, marginTop:-10}}
                                      fontSize={15}
                                      labelFontSize={13}
                                      dropdownPosition = {-4.2}
                                      onChangeText ={ (value,index) => this.getIndexss(index) }
                                      baseColor={'#000000'}
                                      label={'Select City'}
                                      data={this.state.resultcities}
                            />
                        </View>


                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'4%',flexDirection:'row'}}>

                                <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginTop:30}}
                                       source={require('./email.png')}/>

                                <View style = {{width:'90%',marginLeft:'2%'}}>

                                    <TextField
                                        label= 'EMAIL'
                                        value={email}
                                        lineWidth={0}
                                        activeLineWidth={0}
                                        fontSize={15}
                                        baseColor={'#000000'}
                                        onChangeText={ (email) => this.setState({ email }) }
                                        tintColor = {'#800000'}
                                    />

                                </View>


                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>



                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'1%',flexDirection:'row'}}>

                                <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginTop:30}}
                                       source={require('./mobile.png')}/>

                                <View style = {{width:'90%',marginLeft:'2%'}}>

                                    <TextField
                                        label= 'MOBILE NO'
                                        value={mobile}
                                        lineWidth={0}
                                        keyboardType={'numeric'}
                                        maxLength={10}
                                        fontSize={15}                                        
                                        activeLineWidth={0}
                                        baseColor={'#000000'}
                                        onChangeText={ (mobile) => this.setState({ mobile }) }
                                        tintColor = {'#800000'}
                                    />

                                </View>


                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%'}}>

                            </View>


                            <View style = {{marginLeft:'5%',width:'90%',marginTop:'1%',flexDirection:'row'}}>

                                <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginTop:28}}
                                       source={require('./password.png')}/>

                                <View style = {{width:'90%',marginLeft:'2%'}}>

                                    <TextField
                                        label= 'PASSWORD'
                                        value={password}
                                        lineWidth={0}
                                        fontSize={15}                                        
                                        activeLineWidth={0}
                                        secureTextEntry={true}
                                        baseColor={'#000000'}
                                        onChangeText={ (password) => this.setState({ password }) }
                                        tintColor = {'#800000'}
                                    />

                                </View>


                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:'5%',width:'90%',marginTop:0}}>

                            </View>

                    <TouchableOpacity onPress={() => this.login()
                    }
                    style={{marginTop:40,}}>
                    <View style = {{backgroundColor:'#800000',height:55,borderRadius:27.5,alignSelf:'center',width:300,
                        borderBottomWidth: 0,
                        shadowColor: 'black',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        flexDirection:'row'}}>


                        <Text style= {{width:'100%',alignSelf:'center',textAlign:'center',fontSize:20,fontFamily:'Konnect-Medium',color:'white',padding:11}} >
                            SUBMIT
                        </Text>

                        <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginLeft:-50,alignSelf:'center'}}
                               source={require('./right.png')}/>
                    </View>
                    </TouchableOpacity>


                            <TouchableOpacity onPress={() => this.props.navigation.replace('Login')
                            }>
                                <Text style={styles.createaccount} >

                                    <Text style={styles.account} >
                                        Already have an account?
                                    </Text>


                                    <Text style={styles.createaccounts} >
                                        &nbsp;Login Now
                                    </Text>


                                </Text>

                            </TouchableOpacity>
                        </ImageBackground>

                    </KeyboardAwareScrollView>

                </View>

        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    container: {
        flex:1,
        backgroundColor :'white'
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
    account :{
        marginTop : 20,
        textAlign : 'center',
        fontSize: 17,
        justifyContent:'center',
        color : '#c6c6c6',
        fontFamily:'Konnect-Regular',
    } ,
    createaccount :{
        marginLeft : 5,
        fontSize: 17,
        textAlign : 'center',
        marginTop : 30,
        color : '#800000',
        marginBottom:30
    } ,

    createaccounts :{
        marginLeft : 5,
        fontSize: 17,
        textAlign : 'center',
        marginTop : 30,
        color : '#800000',
        textDecorationLine: 'underline',
        fontFamily:'Konnect-Regular'

    } ,
})