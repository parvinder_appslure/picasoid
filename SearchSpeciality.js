import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    Modal,
    StatusBar,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import store from 'react-native-simple-store';
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
import Header from './Header.js'
const GLOBAL = require('./Global');
import { TextField } from 'react-native-material-textfield';
type Props = {};
var dict = {};

import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class SearchSpeciality extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            recognized: '',
            started: '',
            text :'',
            department :[],
            speciality :[],
            hospital:[],
            price:[],
            type :'',
            results: [],
            value:0
        };

    }




    top=(get)=>{
        GLOBAL.appointmentArray = dict

        if (get == "Online") {
            GLOBAL.type = "4"
            GLOBAL.price = dict.online_consult_video_price
            GLOBAL.type = "4"

            this.props.navigation.navigate('OnlineBooking')

        }
        else {

            GLOBAL.price = dict.normal_appointment_price
            GLOBAL.type = "5"
            GLOBAL.onlinetype = "normal"
            this.props.navigation.navigate('BookingAppointmentDetail')
        }

    }

    setModalVisible=(visible,get)=> {


        this.setState({modalVisible : visible})

        this.timeoutCheck = setTimeout(() => {

            this.top(get)
        }, 400);



    }
 

    static navigationOptions = ({ navigation }) => {
        return {
               header: () => null,
         }
    }



    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }

    _handleStateChange = (state) => {

    }





    login = (s,item) => {
        console.log(JSON.stringify(item))
        // GLOBAL.appointmentArray = item
        // GLOBAL.speciality = s
        // GLOBAL.price = item.normal_appointment_price
        // GLOBAL.type = "5"
        // GLOBAL.onlinetype = "normal"


        // // dict = item

        // // GLOBAL.appointmentArray = item
        // // GLOBAL.speciality = s
        // this.dialogComponent.show();

        // if (item.online_consult == "3" && item.normal_appointment == "1" ){
        //     this.setState({modalVisible:true})

        // } else

        GLOBAL.pica_booking_charge_online = item.booking_charge_for_online
        GLOBAL.pica_booking_charge_offline = item.booking_charge_for_offline

         if (GLOBAL.myonlinetype == "online") {
            GLOBAL.type = "4"
            GLOBAL.finalType =4
            GLOBAL.appointmentArray = item
            GLOBAL.speciality = s

            GLOBAL.price = item.online_consult_video_price
            this.props.navigation.navigate('OnlineBooking')
        }
        else{
            GLOBAL.price = item.normal_appointment_price
            GLOBAL.type = "5"
            GLOBAL.appointmentArray = item
            GLOBAL.speciality = s
            GLOBAL.finalType =5
            GLOBAL.onlinetype = "normal"
            this.props.navigation.navigate('BookingAppointmentDetail')
        }

    }

    check = () => {
        this.setState({isSecure :!this.state.isSecure})
    }

    selectedFirst = (item,speciality) => {
//       alert(JSON.stringify(item))
        GLOBAL.speciality = speciality
        GLOBAL.appointmentArray = item

        GLOBAL.pica_booking_charge_online = item.booking_charge_for_online
        GLOBAL.pica_booking_charge_offline = item.booking_charge_for_offline

        this.props.navigation.navigate('DoctorDetail')

    }


    _renderItems = ({item,index}) => {


        var speciality = item.speciality_detail_array


        return (

            <TouchableOpacity onPress={() => this.selectedFirst(item,speciality)
            }>
                {this.state.type == "doctor_result" && (
                    <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>

                        <View style = {{flexDirection:'row',width :'100%'}}>

                            <View>
                                <Image style = {{width :60 ,height :60,borderRadius: 30,margin:10}}
                                       source={{ uri: item.image }}/>
                                <View style = {{backgroundColor:'#800000',borderRadius:4,width:40,height:20,marginTop:2,flexDirection:'row',justifyItems:'center',alignItems:'center', alignSelf:'center'}}>
                                    <Image style = {{width :8 ,height :8,marginLeft:4,resizeMode:'contain'}}
                                           source={require('./star.png')}/>

                                    <Text style={{marginLeft : 5,fontSize : 10,color :'white',fontFamily:'Konnect-Medium',}}>

                                        {item.ratting}
                                    </Text>
                                </View>
                                {item.doctor_avail_status == 1 && (

                                    <Text style={{marginTop:5,fontSize : 11,color :'#3DBA56',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                                        Online
                                    </Text>
                                )}
                                {item.doctor_avail_status != 1 && (

                                    <Text style={{marginTop:5,fontSize : 11,color :'red',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                                        Offline
                                    </Text>
                                )}


                            </View>

                            <View>

                                <View style = {{flexDirection:'row',width:'100%'}}>
                                    <Text style={{marginLeft : 5,fontSize : 15,color :'black',fontFamily:'Konnect-Medium',width :'80%',marginTop:18}}>

                                        {item.name}
                                    </Text>



                                </View>

                                <View style = {{flexDirection:'row'}}>
                                    <Text style={{marginLeft : 5,fontSize : 12,color :'grey',height:'auto',fontFamily:'Konnect-Medium',width :window.width -100}}>

                                        {speciality}
                                    </Text>


                                </View>





                                <View style = {{flexDirection:'row'}}>
                                    <Image style = {{width :20 ,height :20,resizeMode:'contain'}}
                                           source={require('./location.png')}/>

                                    <Text style={{marginLeft : 5,width:'80%',height:'auto',fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',}}>

                                        Hospital/Clinic: {item.lat_long_address}
                                    </Text>

                                </View>

                                <View style = {{flexDirection:'row',justifyContent:'space-between'}}>

                                    <View>
                                        <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                            Experience
                                        </Text>
                                        <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                            {item.experience} Years
                                        </Text>
                                    </View>

                                    <View>
                                        <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                            Likes
                                        </Text>
                                        <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                            {item.like}
                                        </Text>
                                    </View>

                                    <View style = {{marginRight:50}}>
                                        <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                            Reviews
                                        </Text>
                                        <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                            {item.total_review}
                                        </Text>
                                    </View>

                                </View>

                     {item.online_consult!='3' &&  item.online_consult =='1' &&(
                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>

                                       Consult for ₹ {item.online_consult_chat_price}/- onwards
                                   </Text>
                        )}
                     {item.online_consult!='3' &&  item.online_consult =='2' &&(
                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>

                                       Consult for ₹ {item.online_consult_video_price}/- onwards
                                   </Text>
                        )}
                     {item.online_consult=='3' && (
                        
                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>

                                       Consult for ₹ {item.online_consult_video_price}/- onwards
                                   </Text>

                        )}




                     {item.online_consult=='0' && (
                        
                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>

                                       Consult for ₹ {item.normal_appointment_price}/- onwards
                                   </Text>

                        )}



                            </View>

                        </View>

                        <Button
                            style={{padding:7,marginTop:14,fontSize: 15, color: 'white',backgroundColor:'#800000',marginLeft:'55%',width:'20%',height:34,fontFamily:'Konnect-Medium',borderRadius:12,marginBottom: 20}}
                            styleDisabled={{color: 'red'}}
                            onPress={() => this.login(speciality,item)}>
                            BOOK
                        </Button>

                    </View>
                )}
                {this.state.type == "speciality" && (
                    <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>

                        <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'70%'}}>

                            {item.specialty_name}
                        </Text>
                    </View>
                )}




            </TouchableOpacity>
        )
    }

   componentDidMount() {

console.log(GLOBAL.myonlinetype+'--->'+ GLOBAL.myStatefrom+'---'+GLOBAL.searchSpeciality)
        const url =  GLOBAL.BASE_URL  + 'search_doctor_by_spciality'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "patient_id":GLOBAL.user_id,
                "lat":GLOBAL.lat,
                "long":GLOBAL.long,
                "search_keyword":GLOBAL.searchSpeciality,
                "doctor_condition": GLOBAL.myonlinetype,
                "state": GLOBAL.myStatefrom
          }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify('isonline'+responseJson.booking_price_for_online))
                console.log(JSON.stringify('isoffline'+responseJson.booking_price_for_offline))

                if (responseJson.status == true) {
                    GLOBAL.hDoctorBookingAmountOnline = responseJson.booking_price_for_online
                    GLOBAL.hDoctorBookingAmountOffline = responseJson.booking_price_for_offline
                    this.setState({results:responseJson.doctor_list_s})

                    this.setState({type:responseJson.type})

                }else{
                    this.setState({results:[]})
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });


    }

    render() {

        var radio_props = [
            {label: 'Online Consultation', value: 0 },
            {label: 'Offline Consultation', value: 1 }
        ];

        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

                <View style={styles.container}>

                <Header navigation={this.props.navigation}
                headerName={'SPECIALITY'}/>

{this.state.results.length==0 && (
                <Text style={{fontSize : 13,marginTop:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center', textAlign:'center'}}>
            No Doctors found!
            </Text>

    )}

    {this.state.results.length!=0 && (
                    <FlatList style= {{flexGrow:0,height:window.height -80}}
                              data={this.state.results}
                              numColumns={1}
                              keyExtractor = { (item, index) => index.toString() }
                              renderItem={this._renderItems}
                              extraData={this.state}
                    />
        )}



                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
//             Alert.alert('Modal has been closed.');
                            this.setModalVisible(!this.state.modalVisible)
                        }}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            alignItems: 'center'}}>
                            <View style={{
                                width: 300,backgroundColor: 'white',
                                height: 300}}>
                                <View style={{width: '95%', margin: 10}}>
                                    <Text style={{fontSize: 30, color:'black', fontFamily: 'Konnect-Regular', borderBottomWidth: 1, borderBottomColor: '#bfbfbf'}}>BOOK APPOINTMENT</Text>

                                    <View style={{marginTop: 10, flexDirection: 'column'}}>
                                        <TouchableOpacity onPress={()=>this.setModalVisible(false,'Online')}>
                                            <Text style={{fontSize: 16, color:'black', fontFamily: 'Konnect-Regular'}}>Online</Text>
                                            <View style = {{backgroundColor:'#e1e1e1',width:'100%',height:1,marginTop: 10,marginBottom:10}}>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>this.setModalVisible(false,'Offline')}>
                                            <Text style={{fontSize: 16, color:'black', fontFamily: 'Konnect-Regular'}}>Offline</Text>
                                            <View style = {{backgroundColor:'#e1e1e1',width:'100%',height:1,marginTop: 10,marginBottom:10}}>
                                            </View>
                                        </TouchableOpacity>


                                    </View>

                                </View>


                            </View>

                        </View>
                    </Modal>




                </View>

        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    container: {
        flex:1,
        backgroundColor :'#f1f1f1',

    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },
appBar: {
   backgroundColor:'#800000',
   height: APPBAR_HEIGHT,
 },

})
