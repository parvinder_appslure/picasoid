import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
    Modal
} from 'react-native';
const GLOBAL = require('./Global');
import Button from 'react-native-button';
const window = Dimensions.get('window');
import DatePicker from 'react-native-datepicker';

import Header from './Header.js';
import { TextField } from 'react-native-material-textfield';
import ImagePicker from 'react-native-image-picker';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
type Props = {};
const options = {
    title: 'Select Photo',
    maxWidth:300,
    maxHeight:500,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
let customDatesStyles = [];

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class OnlineChat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recognized: '',
            started: '',
            results: [],
            showMemberDetails:0,
            name :GLOBAL.myname,
            myimages:[],
            value:GLOBAL.mygenderedit,
            dob:GLOBAL.myDobfrom,
            address:GLOBAL.myAddfrom,
            area :'',
            city:GLOBAL.myCityfrom,
            path :'',
            avatarSource:'',
            member:[],
            issue:'',
            modalVisible: false,    
            showMemDetails: 0,        
            memDetails:''            
        };

    }

    componentWillUnmount() {

    }


    static navigationOptions = ({ navigation }) => {
        return {
               header: () => null,
        }
    }


    setModalVisible=(visible)=> {
        this.setState({modalVisible: visible});
    }

    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }


    _handleStateChange = (state) => {
        this.setState({showMemberDetails: GLOBAL.showMemDetailss})
                console.log(this.state.showMemberDetails)

        const interest = this.state.member.concat(GLOBAL.mymember)

        this.setState({member:interest})


    }

    componentDidMount(){
//        GLOBAL.showMemDetailss=0
//        this.setState({showMemberDetails:0})
        console.log(this.state.showMemberDetails)
        this.props.navigation.addListener('willFocus',this._handleStateChange);
//         const url = GLOBAL.BASE_URL + 'list_upload_images'

//         fetch(url, {
//             method: 'POST',
//             headers: {
//                 'Content-Type': 'application/json',
//             },

//             body: JSON.stringify({
//                 "user_id": GLOBAL.user_id,
//             }),
//         }).then((response) => response.json())
//             .then((responseJson) => {

//                 if (responseJson.status == true) {

//                     this.setState({myimages:responseJson.list})
// //                    alert(JSON.stringify(responseJson.list))
//                     this.setState({path:responseJson.path})

//                 } else {


//                 }
//             })
//             .catch((error) => {
//                 console.error(error);
//                 this.hideLoading()
//             });
        this.getMembers()
    }

    getMembers=()=>{
        const url = GLOBAL.BASE_URL +  'list_member'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "user_id":GLOBAL.user_id,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
             // s  alert(JSON.stringify(responseJson))

                if (responseJson.status == true) {
                    this.setState({results:responseJson.member_list})

                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

    _handlePress() {


        if (this.state.value == 0){
            GLOBAL.onlinegender = "Male";
        }else{
            GLOBAL.onlinegender = "Female";
        }
        GLOBAL.hproblem = this.state.issue
        GLOBAL.hDoctor_id = GLOBAL.appointmentArray.id

    if(this.state.showMemDetails == 0){
    
    GLOBAL.hbooking_for = 'self'
    GLOBAL.hmember_id = 0

    }else{
    GLOBAL.hbooking_for = 'member'
    GLOBAL.hmember_id = this.state.memDetails.id

    }

        GLOBAL.onlinename = "";
        GLOBAL.onlinedob = "";
        GLOBAL.onlineaddress = "";
        GLOBAL.onlinearea = "";
        GLOBAL.onlinecity = "";


        GLOBAL.myDobfrom = this.state.dob


        GLOBAL.onlineMember = this.state.member
        var imgid = ""

        for (var i = 0; i< this.state.myimages.length ; i ++){
            imgid = imgid + this.state.myimages[i].image + '|'
        }
        if (imgid == ""){
            GLOBAL.listofimages = ""
        } else{
            imgid = imgid.slice(0,-1)
            GLOBAL.listofimages = imgid
        }
    // alert(GLOBAL.listofimages)
        console.log('BookingDetailFinal')

        this.props.navigation.navigate('BookingDetailFinal')
    }

    login = () => {
        this.props.navigation.navigate('NurseTime')
    }


    selectedFirst = (indexs) => {

    if (GLOBAL.appointmentArray.can_book_doctor_free == 0){
    } else {


        this.props.navigation.navigate('ListMember')
    }

    }

    _handlePressd = () => {
        if (this.state.myimages.length >= 3){
            alert('Already added 3 images')
            return
        }
        ImagePicker.showImagePicker(options, (response) => {
//            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };


                const url = GLOBAL.BASE_URL +  'image_attchment_upload'
                const data = new FormData();
                data.append('user_id', GLOBAL.user_id);
                data.append('flag',1);


                // you can append anyone.
                data.append('image', {
                    uri: response.uri,
                    type: 'image/jpeg', // or photo.type
                    name: 'image.png'
                });
                fetch(url, {
                    method: 'post',
                    body: data,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }

                }).then((response) => response.json())
                    .then((responseJson) => {
                        //       this.hideLoading()
                        console.log(JSON.stringify(responseJson))
                        this.setState({myimages:responseJson.images})
                        this.setState({path:responseJson.path})
                        var imageName=responseJson.images[0].image;
                        for(let i=1 ; i<responseJson.images.length; i++){
//                            console.log('for'+ JSON.stringify(responseJson.images[i]))
                            imageName = imageName + ','+ responseJson.images[i].image
                        }
                        console.log(imageName)
                        GLOBAL.hBookingImagesOnline = imageName
                    });
            }

        });
    }

    selectedFirstd  = (item) => {

        const url = GLOBAL.BASE_URL + 'delete_images'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({


                "user_id": GLOBAL.user_id,
                "id":item.id


            }),
        }).then((response) => response.json())
            .then((responseJson) => {


             //   alert(JSON.stringify(responseJson))

                //  this.rajorPay()
                if (responseJson.status == true) {

             this.setState({myimages:responseJson.list_of_images})

                } else {


                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }
    _renderItemsd  = ({item,index}) => {
        var uri = `${this.state.path}${item.image}`;

        return (



                <View style = {{backgroundColor:'transparent',margin:1}}>
                    <Image style = {{width :60 ,height :60,margin:10,resizeMode:'contain'}}
                           source={{uri:uri}}/>
                    <TouchableOpacity style = {{width :20 ,height :20,position:'absolute',right:2}} onPress={() => this.selectedFirstd(item)
                    }>

                    <Image style = {{width :20 ,height :20,resizeMode:'contain', transform:[{rotate:'45deg'}]}}
                           source={require('./add.png')}/>
                    </TouchableOpacity>

                </View>
        )

    }

    selectMember = (item, index)=>{
        this.setModalVisible(false)        

        this.setState({memDetails: item})
        this.setState({showMemDetails: 1})

        console.log(JSON.stringify(item))

    }

addMemberButton=()=>{
    alert('hi')
}

    _renderMembers = ({item,index}) => {

        return (
            <TouchableOpacity style={{marginLeft : 5,width:'95%', backgroundColor: 'white',marginRight:5,
                marginTop: 5,marginBottom:5,borderWidth:1,borderColor:'#800000',borderRadius:0,height:100}}
            onPress={() => this.selectMember(item, index)} activeOpacity={0.99}>
                <View style={{width:'100%', backgroundColor: 'white',flexDirection:'row', alignItems:'center'}}>

                <Image style={{width:50, height:50,  marginLeft:10, borderRadius:25, borderColor:'#800000', borderWidth:2}}
                source={require('./user.png')}/>

                <View style={{flexDirection:'column', width:'80%', margin:10}}>
                    <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',}}>
                        {item.member_name}
                    </Text>

                    <Text style={{marginLeft : 5,fontSize : 13,color :'#555755',fontFamily:'Konnect-Medium',}}>
                        {item.email}
                    </Text>
                   <Text style={{marginLeft : 5,fontSize : 13,color :'#555755',fontFamily:'Konnect-Medium',}}>
                        {item.member_mobile}
                    </Text>
                   <Text style={{marginLeft : 5,fontSize : 13,color :'#555755',fontFamily:'Konnect-Medium',}}>
                        {item.relation}
                    </Text>

                </View>                    
                </View>
            </TouchableOpacity>
        )
    }

    _renderItems = ({item,index}) => {

        return (

            <TouchableOpacity onPress={() => this.selectedFirst(index)
            }>

                <View style = {{backgroundColor:'transparent',margin:1}}>
                    <Image style = {{width :60 ,height :60,margin:10,resizeMode:'contain'}}
                           source={require('./myself.png')}/>

                    <Text style={{fontSize : 14,color :'#800000',fontFamily:'Konnect-Regular',textAlign:'center'}}>

                        {item.member_name}
                    </Text>
                </View>


            </TouchableOpacity>
        )
    }
    render() {
        var speciality = GLOBAL.appointmentArray.speciality_detail_array

        var radio_props_one = [
            {label: 'Male', value: 0 },
            {label: 'Female', value: 1 }
        ];
        let { name } = this.state;
        let { dob } = this.state;
        let { address } = this.state;
        let { area } = this.state;
        let { city } = this.state;
        let { issue } = this.state;

        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

                <View style={styles.container}>
            <Header navigation={this.props.navigation}
                headerName={'CHAT CONSULT BOOKING'}/>

                    <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

                        <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>


                            <View style = {{flexDirection:'row',width :'100%'}}>

                                <View>
                                    <Image style = {{width :60 ,height :60,borderRadius: 30,margin:10}}
                                           source={{ uri: GLOBAL.appointmentArray.image }}/>
                                    {GLOBAL.appointmentArray.doctor_avail_status == 1 && (

                                        <Text style={{fontSize : 11,color :'#3DBA56',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                                            Online
                                        </Text>
                                    )}
                                    {GLOBAL.appointmentArray.doctor_avail_status != 1 && (

                                        <Text style={{fontSize : 11,color :'red',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                                            Offline
                                        </Text>
                                    )}


                                </View>

                                <View>

                                    <View style = {{flexDirection:'row',width:'100%'}}>
                                        <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'70%',marginTop:18}}>

                                            {GLOBAL.appointmentArray.name}
                                        </Text>

                                        <View style = {{backgroundColor:'#F1AE42',borderRadius:4,width:45,height:25,marginTop:18,flexDirection:'row',justifyItems:'center',alignItems:'center'}}>
                                            <Image style = {{width :15 ,height :15,marginLeft:4,resizeMode:'contain'}}
                                                   source={require('./star.png')}/>

                                            <Text style={{marginLeft : 5,fontSize : 12,marginTop:3,color :'white',fontFamily:'Konnect-Medium',}}>

                                                {GLOBAL.appointmentArray.ratting}
                                            </Text>
                                        </View>

                                    </View>

                                    <View style = {{flexDirection:'row'}}>
                                        <Text style={{marginLeft : 5,fontSize : 12,color :'#8F8F8F',height:'auto',fontFamily:'Konnect-Medium',width :'80%'}}>

                                            {speciality}
                                        </Text>


                                    </View>




                                    <View style = {{flexDirection:'row'}}>
                                        <Image style = {{width :20 ,height :20,resizeMode:'contain'}}
                                               source={require('./location.png')}/>

                                        <Text style={{marginLeft : 5,width:window.width - 150,height:30,fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',}}>

                                            Hospital/Clinic: {GLOBAL.appointmentArray.lat_long_address}
                                        </Text>

                                    </View>

                                    <View style = {{flexDirection:'row',justifyContent:'space-between'}}>

                                        <View>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Experience
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.experience} Years
                                            </Text>
                                        </View>

                                        <View>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Likes
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.like}
                                            </Text>
                                        </View>

                                        <View style = {{marginRight:50}}>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Reviews
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.total_review}
                                            </Text>
                                        </View>

                                    </View>

                     {GLOBAL.appointmentArray.online_consult!='3' &&  GLOBAL.appointmentArray.online_consult =='1' &&(
                                <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',marginBottom:10}}>

                                    Consult online for ₹ {GLOBAL.appointmentArray.online_consult_chat_price}/- onwards
                                </Text>

                        )}
                     {GLOBAL.appointmentArray.online_consult!='3' &&  GLOBAL.appointmentArray.online_consult =='2' &&(
                                        <Text style={{fontSize : 12,color :'#800000',marginTop:5,marginBottom:'5%',fontFamily:'Konnect-Medium',}}>

                                            Consult online for ₹ {GLOBAL.appointmentArray.online_consult_video_price}/- onwards
                                        </Text>

                    )}

                     {GLOBAL.appointmentArray.online_consult == '3' && (
                                        <Text style={{fontSize : 12,color :'#800000',marginTop:5,fontFamily:'Konnect-Medium',}}>

                                            Consult online for ₹ {GLOBAL.appointmentArray.online_consult_video_price}/- onwards
                                        </Text>
                    )}                                        

                                        <Text style={{fontSize : 12,color :'#800000',marginTop:5,marginBottom:'2%',fontFamily:'Konnect-Medium',}}>
                                        Booking Charge is ₹ {GLOBAL.pica_booking_charge_online}/- only
                                        </Text>



                                </View>

                            </View>
                        </View>

                       <TouchableOpacity onPress={()=> {GLOBAL.typelists='';
                        this.setModalVisible(true)}}>

                        <View style={{flexDirection:'row',width:'94%',height:60,alignSelf:'center',marginTop:'1.5%',borderRadius:7,backgroundColor:'white'}}>
 
                        <TextInput
                            style={{ height: 46,fontSize:18,marginLeft:20,width:'60%',color:'#0000004D',marginTop:7,marginLeft:8}}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Select Members"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            editable={false}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.select}
                            onChangeText={(text) => this.setState({select:text})}
                        />

                          <Image style={{width:22,height:22,marginTop:19,marginLeft:'28%'}}
                            source={require('./rights.png')}/>


                        </View>
                        </TouchableOpacity>

                        <Text style={{fontSize : 20,color :'#132439',fontFamily:'Konnect-Regular',margin:10}}>

                            Basic Information
                        </Text>
 {this.state.showMemDetails==0 && (
                        <View style = {{backgroundColor:'white',borderRadius:8,marginLeft:10,width:window.width - 20}}>
                            <View style = {{marginLeft:10}}>
                                <TextField
                                    label= 'Name'
                                    value={name}
                                    onChangeText={ (name) => this.setState({ name }) }
                                    tintColor = {'#800000'}
                                />
                                <Text style={{fontSize : 12,color :'rgba(0,0,0,0.5)',fontFamily:'Konnect-Medium',}}>

                                    Gender
                                </Text>

                                <RadioForm style={{ marginTop:12}}
                                           labelStyle={{paddingRight:20}}
                                           radio_props={radio_props_one}
                                           initial={this.state.value}
                                           buttonSize={10}
                                           formHorizontal={true}
                                           buttonColor={'#800000'}
                                           labelHorizontal={true}
                                           animation={false}
                                           labelColor={'black'}
                                           selectedButtonColor={'#800000'}
                                           onPress={(value) => {this.setState({value:value})}}
                                />
{/*                                <TextField
                                    label= 'Date of Birth'
                                    value={dob}
                                    onChangeText={ (dob) => this.setState({ dob }) }
                                    tintColor = {'#800000'}
                                />
*/}
                            <View style={{backgroundColor:'#bfbfbf', height:0.4, marginTop:5}}/>


                            <Text style={{fontSize : 12,color :'rgba(0,0,0,0.5)',fontFamily:'Konnect-Medium',marginTop:10}}>
                             Date of Birth
                            </Text>
                             <DatePicker
                              style={{width: 200,}}
                              date={this.state.dob}
                              mode="date"
                              showIcon={false}
                              placeholder={this.state.dob}
                              format="YYYY-MM-DD"
                              minDate="1960-01-01"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              customStyles={{
                                dateInput: {
                                  marginLeft: -130, borderWidth:0, color:'black'
                                }
                              }}
                              onDateChange={(dob) => {this.setState({dob: dob})}}
                            />

                            <View style={{backgroundColor:'#bfbfbf', height:0.4}}/>

                                <TextField
                                    label= 'Address'
                                    value={address}
                                    onChangeText={ (address) => this.setState({ address }) }
                                    tintColor = {'#800000'}
                                />

                                <TextField
                                    label= 'City'
                                    value={city}
                                    onChangeText={ (city) => this.setState({ city }) }
                                    tintColor = {'#800000'}
                                />

                            </View>
                        </View>
 )}

  {this.state.showMemDetails ==1 &&(
 <View style = {{backgroundColor:'white',borderRadius:8,marginLeft:10,width:window.width - 20}}>
                            <View style = {{marginLeft:10, marginTop:10}}>
                            <TextInput style={{color:'black'}}
                                label= 'Name'
                                value={this.state.memDetails.member_name}
                                editable={false}
                                onChangeText={ (name) => this.setState({ name }) }
                                tintColor = {'#800000'}
                            />
                            <View style={{backgroundColor:'#bfbfbf', height:1}}/>

                            <TextInput style={{color:'black'}}
                                label= 'Gender'
                                value={this.state.memDetails.gender}
                                editable={false}
                                onChangeText={ (gender) => this.setState({ gender }) }
                                tintColor = {'#800000'}
                            />
                            <View style={{backgroundColor:'#bfbfbf', height:1}}/>

                            <TextInput style={{color:'black'}}
                                label= 'Date of Birth'
                                value={this.state.memDetails.member_dob}
                                editable={false}
                                onChangeText={ (dob) => this.setState({ dob }) }
                                tintColor = {'#800000'}
                            />

                            <View style={{backgroundColor:'#bfbfbf', height:1}}/>
                            <TextInput style={{color:'black'}}
                                label= 'Relation'
                                value={this.state.memDetails.relation}
                                onChangeText={ (relation) => this.setState({ relation }) }
                                tintColor = {'#800000'}
                            />
                            <View style={{backgroundColor:'#bfbfbf', height:1, marginBottom:10}}/>

                        </View>
                        </View>
    )}

                        <Button
                            style={{padding:7,marginTop:18,fontSize: 20, color: 'white',backgroundColor:'#800000',marginLeft:'5%',width:'90%',height:40,fontFamily:'Konnect-Medium',borderRadius:4, marginBottom:10}}
                            styleDisabled={{color: 'red'}}
                            onPress={() => this._handlePress()}>
                            PROCEED
                        </Button>
                    </KeyboardAwareScrollView>
         <Modal
           animationType="slide"
           transparent={true}
           visible={this.state.modalVisible}
           onRequestClose={() => {
//             Alert.alert('Modal has been closed.');
             this.setModalVisible(!this.state.modalVisible)
           }}>
                         <TouchableOpacity style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            alignItems: 'center', borderRadius:8}}
                            activeOpacity={1}
                            onPressOut={() => {this.setModalVisible(false)}}
                            >
                            <View style={{width: '80%',backgroundColor: 'white',height: 350, borderRadius:8}}>
                              <View style={{width: '100%',  backgroundColor:'white', borderRadius:8}}>

                              <View style={{flexDirection:'row', width:'100%', backgroundColor:'#800000', height:60,
                               borderTopLeftRadius:8, borderTopRightRadius:8, borderTopLeftWidth:1,justifyContent:'space-between',alignItems:'center',
                               borderTopRightWidth:1, borderTopRightColor:'transparent', borderTopLeftColor:'transparent'}}>
                              <Text style={{fontSize: 17, color:'white', fontFamily: 'Konnect-Regular',margin:10 }}>Select Member</Text>
                              <TouchableOpacity onPress={()=> this.setModalVisible(false)}>
                              <Image style={{width:25, height:25, resizeMode:'contain', marginRight:10}} source={require('./cross.png')}/>
                              </TouchableOpacity>

                                </View>
                {this.state.results.length == 0 && (
            <Text style={{fontSize : 13,marginTop:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center', textAlign:'center'}}>
            No Members added yet!
            </Text>
                )}

                {this.state.results.length!=0 &&(
                                    <FlatList style= {{flexGrow:0,marginBottom:10}}
                                              data={this.state.results}
                                              numColumns={1}
                                              extraData={this.state}
                                              keyExtractor = { (item, index) => index.toString() }
                                              renderItem={this._renderMembers}
                                    />
                                )}

{/*                    <Button
                        style={{padding:7,marginTop:20,marginBottom:10,fontSize: 20, color: 'white',backgroundColor:'#800000',marginLeft:'5%',width:'90%',height:40,fontFamily:'Konnect-Medium',borderRadius:4}}
                        styleDisabled={{color: 'red'}}
                        onPress={() => this.addMemberButton()}>
                        ADD MEMBER
                    </Button>
*/}
                                </View>
                            </View>
                        </TouchableOpacity>
         </Modal>

                </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor :'#f1f1f1',
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },

})
