import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    SafeAreaView,
    AsyncStorage
} from 'react-native';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import Button from 'react-native-button';
import Header from './Header.js'
import { TextField } from 'react-native-material-textfield';
type Props = {};
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class LabHistory extends Component {
    state = {
        name :'',
        email:'',
        phone :'',
        company :'',
        loading:false,
        visible:false,
        results:[],
    };


    static navigationOptions = ({ navigation }) => {
        return {
              header: () => null,
        }
    }


    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }



    componentDidMount() {


        const url = GLOBAL.BASE_URL + 'lab_booking_list'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "user_id": GLOBAL.user_id,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson))
                if (responseJson.status == true) {
                    this.setState({results: responseJson.list})


                } else {
                    this.setState({results: []})
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });
    }
    selectedFirst = (item) => {
     GLOBAL.labdetail = item
     this.props.navigation.navigate('LabHistoryDetail')

    }

    renderItem=({item}) => {
//        alert(JSON.stringify(item))
        return(
            <TouchableOpacity onPress={() => this.selectedFirst(item)
            }>
                <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>


                    <View style = {{flexDirection:'row',width :'100%'}}>
                        <Image style = {{width :60 ,height :60,borderRadius: 30,margin:10}}
                               source={{uri:item.lab_image}}/>

                        <View style = {{width :window.width -120, alignSelf:'center', marginLeft:5}}>

                            <Text  style = {{fontFamily:"Konnect-Medium",color:'black',fontSize:16,marginLeft:4,}}>
                                {item.lab_name}

                            </Text>

                            {item.status == "1" && (
                                <Text  style = {{fontFamily:"Konnect-Regular",color:'green',fontSize:14,marginLeft:4,marginTop:1}}>
                                    Completed

                                </Text>
                            )}

                            {item.status == "0" && (
                                <Text  style = {{fontFamily:"Konnect-Regular",color:'red',fontSize:14,marginLeft:4,marginTop:1}}>
                                    Pending

                                </Text>
                            )}
                        </View>
                    </View>
                </View>
        </TouchableOpacity>

        );
    }

    _keyExtractor=(item, index)=>item.key;



    render() {


        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }
        return (
            <View style={{width : Dimensions.get('window').width,flex:1,backgroundColor:'#F2F5F7'}}>
                            <Header navigation={this.props.navigation}
                            headerName={'LAB BOOKING'}/>

            {this.state.results.length == 0 && (

            <Text style={{fontSize : 13,marginTop:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center', textAlign:'center'}}>
            No Lab Bookings done yet!
            </Text>

                )}

                {this.state.results.length!=0 &&(

                <FlatList
                    data={this.state.results}
                    keyExtractor={(item, index) => index.toString() }
                    renderItem={this.renderItem}
                    extraData={this.state}
                />

                    )}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    container: {

        backgroundColor :'#f1f1f1',
        flex:1
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },

})