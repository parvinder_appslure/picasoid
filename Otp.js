import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    SafeAreaView,
    AsyncStorage,
    Platform, ImageBackground
} from 'react-native';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import Button from 'react-native-button';
var randomString = require('random-string');
import { TextField } from 'react-native-material-textfield';
type Props = {};
var DeviceInfo = require('react-native-device-info');
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class Otp extends Component {
    state = {
        name :'',
        email:'',
        phone :'',
        company :'',
        loading:false,
        visible:false,
        selected:false,
        data:[],
        results:[],

    };


    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
            animations: {
                setRoot: {
                    waitForRender: false
                }
            }
        }
    }



    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }




    componentDidMount(){

    }


    login = () => {
        console.log(GLOBAL.mycareof+'-'+
                    GLOBAL.myrelation+'-'+
                    GLOBAL.mydob+'-'+
                    GLOBAL.myaddress+'-'+
                    GLOBAL.mystate+'-'+
                    GLOBAL.mygender+'-'+
                    GLOBAL.mymarital+'-'+
                    GLOBAL.is_refer_verify+'-'+
                    GLOBAL.apply_to+'-'
)
        if (this.state.email == ''){
            alert('Please Enter OTP')
        }    else if(GLOBAL.otps==this.state.email){
//            console.log(GLOBAL.otps)
            const url = GLOBAL.BASE_URL +  'Signup'
            this.showLoading()
            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },

                body: JSON.stringify({
                    name: GLOBAL.myname,
                    mobile: GLOBAL.mymobile,
                    email: GLOBAL.myemail,
                    password: GLOBAL.mypassword,
                    deviceID: DeviceInfo.getUniqueId(),
                    deviceType: Platform.OS,
                    deviceToken: GLOBAL.firebaseToken,
                    model_name: '',
                    carrier_name: '',
                    device_country: '',
                    device_memory:'',
                    has_notch: '',
                    auth:'normal',
                    manufacture: '',
                    ip_address: '',
                    co:GLOBAL.mycareof,
                    relationship:GLOBAL.myrelation,
                    dob:GLOBAL.mydob,
                    address:GLOBAL.myaddress,
                    state: GLOBAL.mystate,
                    city:GLOBAL.mycity,
                    gender:GLOBAL.mygender,
                    marital_status: GLOBAL.mymarital,
                    is_refer_verify:GLOBAL.is_refer_verify,
                    apply_to :GLOBAL.apply_to,
                }),
            }).then((response) => response.json())
                .then((responseJson) => {

                    if (responseJson.status == true) {
                        this.setState({ results: responseJson.user_detail })
                        GLOBAL.user_id = this.state.results.user_id
                        AsyncStorage.setItem('userID', this.state.results.user_id);
                        AsyncStorage.setItem('image', this.state.results.image);
                        AsyncStorage.setItem('name', this.state.results.name);
                        AsyncStorage.setItem('email', this.state.results.email);
                        AsyncStorage.setItem('mobile', this.state.results.mobile);
                        this.props.navigation.replace('DrawerNavigator')
                    }
                    this.hideLoading()
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });

        }
        else {
            alert('Invalid OTP!')
        }

    }


    resendOtp=()=>{
//        alert('hi')
            var x = randomString({
                length: 4,
                numeric: true,
                letters: false,
                special: false,
            });

               const url = GLOBAL.BASE_URL +  'otp'

//                this.showLoading()
                fetch(url, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        email : GLOBAL.myemail,
                        mobile: GLOBAL.mymobile,
                        otp:x
                    }),
                }).then((response) => response.json())
                    .then((responseJson) => {
  //                      this.hideLoading()
//                        console.log(JSON.stringify(responseJson))
                        if (responseJson.status == true) {
                            GLOBAL.otps = x
                            alert('OTP sent again!')
                            // alert(JSON.stringify(responseJson))
                            // GLOBAL.otps =  x;
                            // GLOBAL.fmobile= this.state.mobile;
                            // GLOBAL.isScreen = '0';
                            //  alert(responseJson.msg)
                            // this.props.navigation.replace('Otp')
                        }else {
//                            alert(responseJson.msg)
                            alert('Something went wrong!')
                        }
                    })
                    .catch((error) => {
                        console.error(error);
                    });

    }

    render() {


        let { phone } = this.state;
        let { email } = this.state;
        let { name } = this.state;
        let { company } = this.state;
        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}
                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

                <View style={styles.container}>
                        <ImageBackground source={require('./background.png')}style={{width: '100%', height: '100%'}}>

                    <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>


                            <Image style = {{width :300 ,height: 140,alignSelf:'center',marginTop:'10%',resizeMode: 'contain'}}
                                   source={require('./logo_ch.png')}/>

                            <Text style={{marginLeft:30,textAlign:'left',width:'100%',color :'black',fontFamily:'Konnect-Medium',fontSize: 30,marginTop:40}} >
                                Verify Your Number
                            </Text>

                            <Text style={{marginLeft:30,textAlign:'left',width:'85%',color :'#c6c6c6',fontFamily:'Konnect-Regular',fontSize: 19,marginTop:12}} >
                                Please enter the verification code sent to you given mobile number
                            </Text>


                            <View style = {{marginLeft:30,width:'80%',marginTop:'8%',flexDirection:'row'}}>

                                <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginTop:30}}
                                       source={require('./mobile.png')}/>

                                <View style = {{width:'90%',marginLeft:10}}>

                                    <TextField
                                        label= 'OTP'
                                        value={email}
                                        lineWidth={0}
                                        fontSize={15}
                                        maxLength={4}
                                        keyboardType={'numeric'}
                                        activeLineWidth={0}
                                        onChangeText={ (email) => this.setState({ email }) }
                                        tintColor = {'#800000'}
                                    />

                                </View>


                            </View>

                            <View style = {{backgroundColor:'#c6c6c6',height:1,marginLeft:30,width:'85%',marginTop:0}}>

                            </View>

                            <TouchableOpacity style={{alignSelf:'flex-end',marginRight:20,marginTop:20}} 
                            onPress={()=> this.resendOtp()}>
                            <Text style= {{fontSize:16,fontFamily:'Konnect-Regular',padding:11,color:'#800000'}} >
                                RESEND OTP?
                            </Text>
                            </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.login()}
                    style={{marginTop:40,}}>
                    <View style = {{backgroundColor:'#800000',height:55,borderRadius:27.5,alignSelf:'center',width:300,
                        borderBottomWidth: 0,
                        shadowColor: 'black',
                        shadowOffset: { width: 0, height: 2 },
                        shadowOpacity: 0.8,
                        shadowRadius: 2,
                        flexDirection:'row'}}>


                        <Text style= {{width:'100%',alignSelf:'center',textAlign:'center',fontSize:20,fontFamily:'Konnect-Medium',color:'white',padding:11}} >
                            SUBMIT
                        </Text>

                        <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginLeft:-50,alignSelf:'center'}}
                               source={require('./right.png')}/>
                    </View>
                    </TouchableOpacity>


                    </KeyboardAwareScrollView>
                        </ImageBackground>

                </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor :'white'
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },
})