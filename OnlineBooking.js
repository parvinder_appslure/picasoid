import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    Share,
    ScrollView
} from 'react-native';
const GLOBAL = require('./Global');
import Button from 'react-native-button';
const window = Dimensions.get('window');
import Header from './Header.js';

import { TextField } from 'react-native-material-textfield';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { Dialog, DialogContent, DialogComponent, DialogTitle } from 'react-native-dialog-component';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class OnlineBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recognized: '',
            started: '',
            results: [],
            member:[],
            images: [],
            results:[{
              id:'1',
              mode_name:'Chat',
              is_chosen: '0',
              dec_consultMode: 'chat'             
            },
            {
              id:'2',
              mode_name:'Video call',
              is_chosen: '0',
              dec_consultMode: 'video'
            }]

        };

    }




    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
        }
    }



    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }


    _fancyShareMessage=()=>{
//        console.log(GLOBAL.appointmentArray)
        var a = 'Checkout '+GLOBAL.appointmentArray.name+ ' at PiCaSoid app.'+ '\n'+
        'Current Hospital/Clinic: '+GLOBAL.appointmentArray.hospital_name+'\n'+
        'Department: '+GLOBAL.appointmentArray.dr_depart_array+
        '\n'+'Speciality: '+GLOBAL.appointmentArray.speciality_detail_array+'\n'+
        'Experience: ' +GLOBAL.appointmentArray.experience + ' Years'+ '\n'+
        'Download the app from here:'+'\n'+
        'Android: '+GLOBAL.androidappUrl+'\n'+
        'iOS: '+GLOBAL.iosappUrl;


        console.log(a)
        Share.share({
                message:a , url:GLOBAL.androidappUrl
            },{
                tintColor:'green',
                dialogTitle:'Share this Doctor via...'
            }
        ).then(this._showResult);

    }


    _handleStateChange = (state) => {
        
        GLOBAL.onlinetypes=''
    }

     getInterPrice=()=>{
        const url = GLOBAL.BASE_URL +  'settings'
        fetch(url, {
            method: 'GET',
            
        }).then((response) => response.json())
            .then((responseJson) => {
//                alert(JSON.stringify(responseJson))
                if (responseJson.status == true) {

                    GLOBAL.androidappUrl = responseJson.play_store_url
                    GLOBAL.iosappUrl = responseJson.app_url
                }else {
//                    alert('No Data Found')
                }
            })
            .catch((error) => {
                console.error(error);
            });

     }

    componentDidMount(){
        this.props.navigation.addListener('willFocus',this._handleStateChange);
        this.getInterPrice()
    }


    Calcprice = (dd,get)=>{
        GLOBAL.type = "4"
        GLOBAL.finalType =4
        // GLOBAL.hModule= 'chat'
        // GLOBAL.hDoctor_id = GLOBAL.appointmentArray.id

        GLOBAL.price = dd
        if (get == 1){
            console.log('chat')
            GLOBAL.hModule= 'chat'
            GLOBAL.hDoctor_id = GLOBAL.appointmentArray.id
            GLOBAL.onlinetype = "chat"
       if ( GLOBAL.appointmentArray.can_book_doctor_free  != 0)   {
           this.props.navigation.navigate('OnlineChat')
            console.log('BookingDetailFinal')
       }else{
            console.log('BookingAppointmentDetail')
           this.props.navigation.navigate('BookingAppointmentDetail')
       }

        //     GLOBAL.appointmentArray.can_book_doctor_free  != 0 &&(
        //         this.props.navigation.navigate('BookingAppointmentDetail')
        //     )}
        //
        // GLOBAL.appointmentArray.can_book_doctor_free  == 0 &&(
        //
        // )}



        }else {
            GLOBAL.onlinetype = "video"
            console.log('OnlineVideo')
            this.props.navigation.navigate('OnlineVideo')
        }

    }


    CalcpriceFree = (dd,get)=>{
        GLOBAL.type = "4"
        GLOBAL.finalType =4
        GLOBAL.appointmentArray.online_consult_chat_price = '0'
        GLOBAL.appointmentArray.online_consult_video_price ='0'
        GLOBAL.price = dd
        GLOBAL.onlinetypes = "free"

        if (get == 1){
            console.log('chat')
            GLOBAL.hModule= 'chat'
            GLOBAL.hDoctor_id = GLOBAL.appointmentArray.id
            GLOBAL.onlinetype = "chat"

       if ( GLOBAL.appointmentArray.can_book_doctor_free  != 0)   {
           this.props.navigation.navigate('OnlineChat')
            console.log('BookingDetailFinal')
       }else{
            console.log('BookingAppointmentDetail')
           this.props.navigation.navigate('BookingAppointmentDetail')
       }

        }else {
            GLOBAL.onlinetype = "video"
            console.log('OnlineVideo')
            this.props.navigation.navigate('OnlineVideo')
        }

    }

    selectMode=(item, indexs)=>{


      var a = this.state.results;
      for (var i = 0; i < this.state.results.length; i++) {
        this.state.results[i].is_chosen = "0";
      }
      var index = a[indexs];
      if (index.is_chosen == "0") {
        index.is_chosen = "1";
        this.setState({ mode_id: item.id });
      } else {
        index.is_chosen = "0";
      }
      this.state.results[indexs] = index;
      this.setState({ results: this.state.results });

       this.timeoutCheck = setTimeout(() => {
        //alert(item.dec_consultMode)
        this.dialogComponent.dismiss()
        // GLOBAL.consultMode = item.dec_consultMode
        // this.props.navigation.navigate('SelectDateTime')
        this.CalcpriceFree('0', item.id)
       }, 500);

  }

    modesRender=({item,index})=>{
      return(
        <TouchableOpacity onPress={()=> this.selectMode(item, index)}>
         <View style={{flexDirection:'row', width:'71%', margin:10, 
         justifyContent:'space-between'}}>
         <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',alignSelf:'center'}}>
                        {item.mode_name}
        </Text>
        
        {item.is_chosen == '0' && (
                <Image style={{width:25, height:35 , resizeMode:'contain',}}
                source={require('./unfill.png')}/>
        )}
        {item.is_chosen!='0' && (
                <Image style={{width:25, height:35 , resizeMode:'contain',}}
                source={require('./fill.png')}/>
        )}


        </View>
        </TouchableOpacity>
        )
    }

    freeBook=(gets)=>{


        if(gets == 1){
            this.setState({results : [{
              id:'1',
              mode_name:'Chat',
              is_chosen: '0',
              dec_consultMode: 'chat'             
            },
        ]})
            this.dialogComponent.show()

        } else if(gets ==2 ){
            this.setState({results : [{
              id:'2',
              mode_name:'Video call',
              is_chosen: '0',
              dec_consultMode: 'video'
            }]})
            this.dialogComponent.show()

        }else{
            this.dialogComponent.show()
        }
    }

    render() {


      //  var speciality =  GLOBAL.speciality
        var speciality = GLOBAL.appointmentArray.speciality_detail_array
        console.log(JSON.stringify(GLOBAL.appointmentArray)+'Asdsds')

        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }

        var freeLine;

        if(GLOBAL.appointmentArray.is_free_consult == '1'|| GLOBAL.appointmentArray.is_free_consult == '2' || GLOBAL.appointmentArray.is_free_consult == '3'){
            freeLine = 
                            <View style={{height:120,width:window.width,marginTop:1,backgroundColor:'white',alignItems:'center',flexDirection:'row',marginBottom:70}}>

                                <View style={{flexDirection:'column',width:'70%',height:'90%',marginLeft:'10%'}}>


                                  <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginLeft:'13%'}}>
                                    <Image
                                        style={{height:25,width:25}}
                                        source={require('./cons_free.png')}
                                    />
                                    <Text style={{fontSize:15,fontFamily:'Konnect-Medium',fontWeight:'bold',marginLeft:5}}>Free Consult</Text>

                                  </View>

                                  <View style={{borderBottomWidth:1,borderBottomColor:'#0000004D',width:window.width,marginTop:5,alignSelf:'center',marginLeft:'14%'}}>
                                  </View>

                                    <Text style={{fontSize:12,fontFamily:'Konnect-Regular',alignSelf:'center',marginTop:24}}>Schedule for your preferred date/time</Text>
                                            <Text style={{fontSize:12,textAlign:'center',color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center'}}>15 minutes call duration ₹ 0 Consultation Fee</Text>
                                    </View>

                                    <Button
                                          style={{fontFamily:'Konnect-Medium',fontSize: 12, color: 'white',alignSelf:'center'}}
                                          containerStyle={{backgroundColor:'#800000',marginTop:58,width:'15%',height:26,borderRadius:8,marginBottom: 20,justifyContent:'center'}}
                                          styleDisabled={{color: 'red'}}
                                          onPress={()=> this.freeBook(GLOBAL.appointmentArray.is_free_consult)}>
                                          Book
                                    </Button>
                                </View>

        }else{
            freeLine = <View style={{height:1}}/>
        }


        return (

                <View style={styles.container}>
            <Header navigation={this.props.navigation}
                headerName={'ONLINE APPOINTMENT'}/>

                    <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

                        <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>




                            <View style = {{flexDirection:'row',width :'100%'}}>

                                <View>
                                    <Image style = {{width :60 ,height :60,borderRadius: 30,margin:11,borderWidth:1,borderColor:'#800000'}}
                                           source={{ uri: GLOBAL.appointmentArray.image }}/>


                                    <View style = {{marginLeft:20,backgroundColor:'#800000',borderRadius:4,width:40,height:20,flexDirection:'row',alignItems:'center'}}>
                                        <Image style = {{width :8 ,height :8,marginLeft:5,resizeMode:'contain',}}
                                               source={require('./star.png')}/>

                                        <Text style={{marginLeft : 5,fontSize : 8,color :'white',fontFamily:'Konnect-Medium', marginTop:-0.2}}>

                                            {GLOBAL.appointmentArray.ratting}
                                        </Text>
                                    </View>

                                    {GLOBAL.appointmentArray.doctor_avail_status == 1 && (

                                        <Text style={{marginLeft : 15,marginTop:5,fontSize : 11,color :'#3DBA56',fontFamily:'Konnect-Medium',width:50,textAlign:'center'}}>

                                            Online
                                        </Text>
                                    )}
                                    {GLOBAL.appointmentArray.doctor_avail_status != 1 && (

                                        <Text style={{marginLeft : 15,marginTop:5,fontSize : 11,color :'red',fontFamily:'Konnect-Medium',width:50,textAlign:'center'}}>

                                            Offline
                                        </Text>
                                    )}


                                </View>

                                <View>

                                    <View style = {{flexDirection:'row',width:'100%'}}>
                                        <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'70%',marginTop:20}}>

                                            {GLOBAL.appointmentArray.name}
                                        </Text>

                                        <TouchableOpacity style={{position:'absolute',top:12,right:-7,width:23}}
                                         onPress={() =>this._fancyShareMessage()}>
                                        <Image source={require('./share.png')}
                                         style={{height:23,width:23,resizeMode:'contain'}}/>
                                        </TouchableOpacity>



                                    </View>

                                    <View style = {{flexDirection:'row'}}>
                                        <Text style={{marginLeft : 5,fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',width :'70%'}}>

                                            {speciality}
                                        </Text>


                                    </View>


                               <View style = {{flexDirection:'row'}}>
                                   <Image style = {{width :20 ,height :20,resizeMode:'contain'}}
                                          source={require('./location.png')}/>

                                   <Text style={{marginLeft : 5,width:window.width - 150,height:'auto',fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',}}>

                                       Hospital/Clinic: {GLOBAL.appointmentArray.lat_long_address}
                                   </Text>

                               </View>



                                    <View style = {{flexDirection:'row',justifyContent:'space-between'}}>

                                        <View>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Experience
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.experience} Years
                                            </Text>
                                        </View>

                                        <View>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Likes
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.like}
                                            </Text>
                                        </View>

                                        <View style = {{marginRight:50}}>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Reviews
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.total_review}
                                            </Text>
                                        </View>

                                    </View>

                     {GLOBAL.appointmentArray.online_consult!='3' &&  GLOBAL.appointmentArray.online_consult =='1' &&(

                                        <Text style={{fontSize : 12,color :'#800000',marginTop:5,marginBottom:'5%',fontFamily:'Konnect-Medium',}}>

                                            Consult online for ₹ {GLOBAL.appointmentArray.online_consult_chat_price}/- onwards
                                        </Text>
                     )}
                     {GLOBAL.appointmentArray.online_consult!='3' &&  GLOBAL.appointmentArray.online_consult =='2' &&(
                                        <Text style={{fontSize : 12,color :'#800000',marginTop:5,marginBottom:'5%',fontFamily:'Konnect-Medium',}}>

                                            Consult online for ₹ {GLOBAL.appointmentArray.online_consult_video_price}/- onwards
                                        </Text>

                    )}

                     {GLOBAL.appointmentArray.online_consult == '3' && (
                                        <Text style={{fontSize : 12,color :'#800000',marginTop:5,fontFamily:'Konnect-Medium',}}>

                                            Consult online for ₹ {GLOBAL.appointmentArray.online_consult_video_price}/- onwards
                                        </Text>
                    )}                                        

                                        <Text style={{fontSize : 12,color :'#800000',marginTop:5,marginBottom:'2%',fontFamily:'Konnect-Medium',}}>
                                        Booking Charge is ₹ {GLOBAL.pica_booking_charge_online}/- only
                                        </Text>

                                </View>

                            </View>

                        </View>

                    <View style={{ flexDirection:'column',backgroundColor:'#e3e3e3'}}>

                      <Text style={{fontSize:18,fontFamily:'Konnect-Medium',color:'#000000',alignSelf:'center',marginTop:2}}>Consult Online</Text>



                     {GLOBAL.appointmentArray.online_consult!='3' &&  GLOBAL.appointmentArray.online_consult =='1' &&(
                        <View style={{height:120,width:window.width,marginTop:10,backgroundColor:'white',alignItems:'center',flexDirection:'row'}}>
                              <View style={{flexDirection:'column',width:'70%',height:'90%',marginLeft:'10%'}}>

                                     <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginLeft:'13%'}}>
                                        <Image
                                            style={{height:25,width:25}}
                                            source={require('./chatlogo.png')}
                                        />

                                        <Text style={{fontSize:15,fontFamily:'Konnect-Medium',fontWeight:'bold',marginLeft:5}}>Chat Consult</Text>

                                     </View>

                                     <View style={{borderBottomWidth:1,borderBottomColor:'#0000004D',width:window.width,marginTop:5,alignSelf:'center',marginLeft:'14%'}}>
                                     </View>


                                        <Text style={{fontSize:12,fontFamily:'Konnect-Regular',alignSelf:'center',marginTop:24}}>Schedule for your preferred date/time</Text>


                                            <Text style={{fontSize : 13,textAlign:'center',color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center'}}>
                                                <Text style={{fontSize:12,textAlign:'center',fontFamily:'Konnect-Medium'}}>15 minutes call duration ₹ {GLOBAL.appointmentArray.online_consult_chat_price} Consultation Fee</Text>
                                            </Text>

                                    </View>



                                    <Button
                                          style={{fontFamily:'Konnect-Medium',fontSize: 12, color: 'white',alignSelf:'center'}}
                                          containerStyle={{backgroundColor:'#800000',marginTop:58,width:'15%',height:26,borderRadius:8,marginBottom: 20,justifyContent:'center'}}
                                          styleDisabled={{color: 'red'}}
                                          onPress={()=>this.Calcprice(GLOBAL.appointmentArray.online_consult_chat_price,1)}>
                                          Book
                                      </Button>
                            </View>


                     )}


                     {GLOBAL.appointmentArray.online_consult!='3' &&  GLOBAL.appointmentArray.online_consult =='2' &&(
                            <View style={{height:120,width:window.width,marginTop:10,backgroundColor:'white',alignItems:'center',flexDirection:'row',marginBottom:1}}>

                                <View style={{flexDirection:'column',width:'70%',height:'90%',marginLeft:'10%'}}>


                                  <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginLeft:'13%'}}>
                                    <Image
                                        style={{height:25,width:25}}
                                        source={require('./videologo.png')}
                                    />
                                    <Text style={{fontSize:15,fontFamily:'Konnect-Medium',fontWeight:'bold',marginLeft:5}}>Video Consult</Text>

                                  </View>

                                  <View style={{borderBottomWidth:1,borderBottomColor:'#0000004D',width:window.width,marginTop:5,alignSelf:'center',marginLeft:'14%'}}>
                                  </View>

                                    <Text style={{fontSize:12,fontFamily:'Konnect-Regular',alignSelf:'center',marginTop:24}}>Schedule for your preferred date/time</Text>
                                            <Text style={{fontSize:12,textAlign:'center',color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center'}}>15 minutes call duration ₹{GLOBAL.appointmentArray.online_consult_video_price} Consultation Fee</Text>
                                    </View>

                                    <Button
                                          style={{fontFamily:'Konnect-Medium',fontSize: 12, color: 'white',alignSelf:'center'}}
                                          containerStyle={{backgroundColor:'#800000',marginTop:58,width:'15%',height:26,borderRadius:8,marginBottom: 20,justifyContent:'center'}}
                                          styleDisabled={{color: 'red'}}
                                          onPress={()=>this.Calcprice(GLOBAL.appointmentArray.online_consult_video_price,2)}>
                                          Book
                                    </Button>
                                </View>

                     )}

                     {GLOBAL.appointmentArray.online_consult == '3' && (
                     <View>
                        <View style={{height:120,width:window.width,marginTop:10,backgroundColor:'white',alignItems:'center',flexDirection:'row'}}>
                              <View style={{flexDirection:'column',width:'70%',height:'90%',marginLeft:'10%'}}>

                                     <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginLeft:'13%'}}>
                                        <Image
                                            style={{height:25,width:25}}
                                            source={require('./chatlogo.png')}
                                        />

                                        <Text style={{fontSize:15,fontFamily:'Konnect-Medium',fontWeight:'bold',marginLeft:5}}>Chat Consult</Text>

                                     </View>

                                     <View style={{borderBottomWidth:1,borderBottomColor:'#0000004D',width:window.width,marginTop:5,alignSelf:'center',marginLeft:'14%'}}>
                                     </View>


                                        <Text style={{fontSize:12,fontFamily:'Konnect-Regular',alignSelf:'center',marginTop:24}}>Schedule for your preferred date/time</Text>


                                            <Text style={{fontSize : 13,textAlign:'center',color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center'}}>
                                                <Text style={{fontSize:12,textAlign:'center',fontFamily:'Konnect-Medium'}}>15 minutes call duration ₹ {GLOBAL.appointmentArray.online_consult_chat_price} Consultation Fee</Text>
                                            </Text>

                                    </View>



                                    <Button
                                          style={{fontFamily:'Konnect-Medium',fontSize: 12, color: 'white',alignSelf:'center'}}
                                          containerStyle={{backgroundColor:'#800000',marginTop:58,width:'15%',height:26,borderRadius:8,marginBottom: 20,justifyContent:'center'}}
                                          styleDisabled={{color: 'red'}}
                                          onPress={()=>this.Calcprice(GLOBAL.appointmentArray.online_consult_chat_price,1)}>
                                          Book
                                      </Button>
                            </View>

                            <View style={{height:120,width:window.width,marginTop:1,backgroundColor:'white',alignItems:'center',flexDirection:'row',marginBottom:1}}>

                                <View style={{flexDirection:'column',width:'70%',height:'90%',marginLeft:'10%'}}>


                                  <View style={{flexDirection:'row',alignItems:'center',alignSelf:'center',marginLeft:'13%'}}>
                                    <Image
                                        style={{height:25,width:25}}
                                        source={require('./videologo.png')}
                                    />
                                    <Text style={{fontSize:15,fontFamily:'Konnect-Medium',fontWeight:'bold',marginLeft:5}}>Video Consult</Text>

                                  </View>

                                  <View style={{borderBottomWidth:1,borderBottomColor:'#0000004D',width:window.width,marginTop:5,alignSelf:'center',marginLeft:'14%'}}>
                                  </View>

                                    <Text style={{fontSize:12,fontFamily:'Konnect-Regular',alignSelf:'center',marginTop:24}}>Schedule for your preferred date/time</Text>
                                            <Text style={{fontSize:12,textAlign:'center',color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center'}}>15 minutes call duration ₹{GLOBAL.appointmentArray.online_consult_video_price} Consultation Fee</Text>
                                    </View>

                                    <Button
                                          style={{fontFamily:'Konnect-Medium',fontSize: 12, color: 'white',alignSelf:'center'}}
                                          containerStyle={{backgroundColor:'#800000',marginTop:58,width:'15%',height:26,borderRadius:8,marginBottom: 20,justifyContent:'center'}}
                                          styleDisabled={{color: 'red'}}
                                          onPress={()=>this.Calcprice(GLOBAL.appointmentArray.online_consult_video_price,2)}>
                                          Book
                                    </Button>
                                </View>
                            </View>

                     )}



                     {freeLine}



                            </View>
                    </KeyboardAwareScrollView>
                <DialogComponent
                    dialogStyle = {{backgroundColor:'transparent'}}
                    dialogTitle={<DialogTitle title="Dialog Title" />}
                    dismissOnTouchOutside={true}
                    dismissOnHardwareBackPress={true}
                    ref={(dialogComponent) => { this.dialogComponent = dialogComponent; }}
                >
                    <View style = {{width :window.width - 30 ,alignSelf:'center',backgroundColor:'transparent',flexDirection:'column'}}>

                    <View style = {{backgroundColor:'#6d0000',width:'100%',height :'auto', justifyContent:'space-between', flexDirection:'row'}}>

                        <Text style = {{margin:15,color:'white',fontFamily:'Konnect-Regular',fontSize:17, }}>
                        Choose Type
                        </Text>
                      <TouchableOpacity onPress={()=> this.dialogComponent.dismiss()}>
                      <Image style={{width:25, height:25, resizeMode:'contain',  margin:15}} source={require('./cross.png')}/>
                      </TouchableOpacity>

                        </View>

                        <View style = {{width:window.width - 30,backgroundColor:'white',height:'auto'}}>


                      <FlatList style= {{flexGrow:0,marginBottom:10, alignSelf:'center'}}
                                data={this.state.results}
                                numColumns={1}
                                extraData={this.state}
                                keyExtractor = { (item, index) => index.toString() }
                                renderItem={this.modesRender}/>




                        </View>
                    </View>
                </DialogComponent>

                  </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor :'#e3e3e3',
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },

})