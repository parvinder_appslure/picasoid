import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
    Modal,
    Linking,
    PermissionsAndroid,
    ToastAndroid,
    ScrollView,
    Share
} from 'react-native';
const GLOBAL = require('./Global');
import Button from 'react-native-button';
import Header from './Header.js';
import { RNCamera } from 'react-native-camera';
const window = Dimensions.get('window');
import ImagePicker from 'react-native-image-picker';
import RBSheet from "react-native-raw-bottom-sheet";
import DocumentPicker from 'react-native-document-picker';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import {NavigationActions, StackActions} from "react-navigation";
import RNFetchBlob from "rn-fetch-blob";
var randomString = require('random-string');

type Props = {};
const options = {
    title: 'Upload Prescription',
    maxWidth:300,
    maxHeight:500,
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};
var path_url = ''
var rpath="";



export default class Upload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            path :'',
            results:[1],resultspr:[1], resultslab:[1], resultsothers:[1],
            started: '',tone:0,ttwo:0,tthree:0,tfour:0,
            modalVisible: false,
            pdfFile:'',
            singleFile:'',
            openCamera:0,
            startLimit:0
        };

    }

    componentWillUnmount() {

    }

    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
            
         }
    }



    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }


    _handleStateChange = (state) => {

    }

    componentDidMount(){
        this.props.navigation.addListener('willFocus',this._handleStateChange);
        this.getDocs('bills')
//        this.RBSheet.open()
    }

    getDocs=(types)=>{
        GLOBAL.reportTypes = types        
        var doc_type = types
        const url = GLOBAL.BASE_URL + 'list_uploadimages_by_user'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "user_id": GLOBAL.user_id,
                "limit_from":this.state.startLimit,
                "type": doc_type

            }),
        }).then((response) => response.json())
            .then((responseJson) => {
              //  console.log(JSON.stringify(responseJson))

                if (responseJson.status == true) {
                    if(doc_type == 'bills'){
                    this.setState({results: responseJson.list})
                    this.setState({path : responseJson.path})

                    }else if(doc_type == 'prescription'){
                    this.setState({resultspr: responseJson.list})
                    this.setState({path_urlspr : responseJson.path})

                    }else if(doc_type == 'labreport'){
                    this.setState({resultslab: responseJson.list})
                    this.setState({path_urlslab : responseJson.path})

                    }else if(doc_type == 'others'){
                    this.setState({resultsothers: responseJson.list})
                    this.setState({path_urlsothers : responseJson.path})

                    }

                } else {
                    if(doc_type == 'bills'){
                    this.setState({results: []})
                    this.setState({path_url : ''})

                    }else if(doc_type == 'prescription'){
                    this.setState({resultspr: []})
                    this.setState({path_urlspr : ''})

                    }else if(doc_type == 'labreport'){
                    this.setState({resultslab: []})
                    this.setState({path_urlslab : ''})

                    }else if(doc_type == 'others'){
                    this.setState({resultsothers: []})
                    this.setState({path_urlsothers : ''})

                    }

                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });


    }

    loadMoreData=(types)=>{
      console.log(this.state.startLimit+1)
        GLOBAL.reportTypes = types        
        var doc_type = types
        const url = GLOBAL.BASE_URL + 'list_uploadimages_by_user'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "user_id": GLOBAL.user_id,
                "limit_from":this.state.startLimit+6,
                "type": doc_type

            }),
        }).then((response) => response.json())
            .then((responseJson) => {
               console.log(JSON.stringify(responseJson))

                if (responseJson.status == true) {
                    if(doc_type == 'bills'){
                    this.setState({results: responseJson.list})
                    this.setState({path : responseJson.path})

                    }else if(doc_type == 'prescription'){
                    this.setState({resultspr: responseJson.list})
                    this.setState({path_urlspr : responseJson.path})

                    }else if(doc_type == 'labreport'){
                    this.setState({resultslab: responseJson.list})
                    this.setState({path_urlslab : responseJson.path})

                    }else if(doc_type == 'others'){
                    const interest = [...this.state.resultsothers,...responseJson.list]
                    this.setState({resultsothers: interest})
                    this.setState({path_urlsothers : responseJson.path})

                    }

                } else {
                    if(doc_type == 'bills'){
                    this.setState({results: []})
                    this.setState({path_url : ''})

                    }else if(doc_type == 'prescription'){
                    this.setState({resultspr: []})
                    this.setState({path_urlspr : ''})

                    }else if(doc_type == 'labreport'){
                    this.setState({resultslab: []})
                    this.setState({path_urlslab : ''})

                    }else if(doc_type == 'others'){
                    this.setState({resultsothers: []})
                    this.setState({path_urlsothers : ''})

                    }

                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });


    }

    selectedFirst=()=>{
        console.log('selected')
    }

    setModalVisible=(visible)=> {
    this.setState({modalVisible: visible});
  }


 async selectOneFile(getdoc_cat, getdoc_type) {
//    alert(getdoc_cat+'=='+getdoc_type)
    //Opening Document Picker for selection of one file
    var decideType = []

    if(getdoc_type == 'image'){
        decideType = [DocumentPicker.types.images]
    }else{
        decideType = [DocumentPicker.types.pdf]
    }

    try {
      const res = await DocumentPicker.pick({
        type: decideType,
        //There can me more options as well
        // DocumentPicker.types.allFiles
        // DocumentPicker.types.images
        // DocumentPicker.types.plainText
        // DocumentPicker.types.audio
        // DocumentPicker.types.pdf
      });
      //Printing the log realted to the file
      console.log('res : ' + JSON.stringify(res));
      console.log('URI : ' + res.uri);
      console.log('Type : ' + res.type);
      console.log('File Name : ' + res.name);
      console.log('File Size : ' + res.size);
      //Setting the state to show single file attributes
      this.setState({ singleFile: res });
      this.setState({ fileUri : res.uri})
      this.RBSheet.close()
      this.finalUploadDoc(getdoc_type, res.uri, getdoc_cat, res.type, res.name)
    } catch (err) {
      //Handling any exception (If any)
      if (DocumentPicker.isCancel(err)) {
        //If user canceled the document selection
        this.RBSheet.close()
        console.log('Canceled from single doc picker');
      } else {
        //For Unknown Error
        this.RBSheet.close()
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  }


      finalUploadDoc=(type, path, getdoc_cat, resType, resName)=>{
  //      alert('finalUploadDoc'+type)
                const url = GLOBAL.BASE_URL +  'upload_personal_by_user'
                const data = new FormData();
                data.append('user_id', GLOBAL.user_id);
                data.append('flag',1);
                data.append('type', getdoc_cat);
                data.append('image', {
                    uri: path,
                    type: resType,
                    name: resName
                });
                console.log('------->'+JSON.stringify(data))
                fetch(url, {
                    method: 'post',
                    body: data,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }

                }).then((response) => response.json())
                    .then((responseJson) => {

//                        alert(JSON.stringify(responseJson))

                        if(responseJson.status==true){
                            this.getDocs(getdoc_cat)
                            alert('Report uploaded successfully.')
                        }else{
                            alert('Oops! This type of file is not supported.')
                        }
                    });

            }

      finalUploadCamera=(type, path, getdoc_cat, resType, resName)=>{
  //      alert('finalUploadDoc'+type)
                const url = GLOBAL.BASE_URL +  'upload_personal_by_user'
                const data = new FormData();
                data.append('user_id', GLOBAL.user_id);
                data.append('flag',1);
                data.append('type', GLOBAL.upload_doc_cat);
                data.append('image', {
                    uri: GLOBAL.cameraImage,
                    type: 'image/jpeg',
                    name: 'image.jpg'
                });
                console.log('------->'+JSON.stringify(data))
                fetch(url, {
                    method: 'post',
                    body: data,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }

                }).then((response) => response.json())
                    .then((responseJson) => {

//                        alert(JSON.stringify(responseJson))

                        if(responseJson.status==true){
                            this.getDocs(GLOBAL.upload_doc_cat)
                            alert('Report uploaded successfully!')
                        }else{
                            alert('Oops! This type of file is not supported.')
                        }
                    });

            }


    _handlePressd = () => {

        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);

            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                const source = { uri: response.uri };


                const url = GLOBAL.BASE_URL +  'upload_personal_by_user'
                const data = new FormData();
                data.append('user_id', GLOBAL.user_id);
                data.append('flag',1);
                data.append('image', {
                    uri: response.uri,
                    type: 'image/jpeg',
                    name: 'image.png'
                });
                fetch(url, {
                    method: 'post',
                    body: data,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }

                }).then((response) => response.json())
                    .then((responseJson) => {

                        alert(JSON.stringify(responseJson))
                       //  //       this.hideLoading()
                       // alert('Successful Upload')
                       //  this.props
                       //      .navigation
                       //      .dispatch(StackActions.reset({
                       //          index: 0,
                       //          actions: [
                       //              NavigationActions.navigate({
                       //                  routeName: 'DrawerNavigator',
                       //                  params: { someParams: 'parameters goes here...' },
                       //              }),
                       //          ],


                       //      }))
                       //  this.props.navigation.navigate('MyDocument')

                    });
            }
        });
    }


    deleteImage=(imageId,reportType)=>{
        console.log(imageId, reportType)
            const url = GLOBAL.BASE_URL + 'delete_uploadfiles_by_user'

            fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "user_id": GLOBAL.user_id,
                "id": imageId,
                "type": reportType

            }),
        }).then((response) => response.json())
            .then((responseJson) => {

                console.log('--------'+JSON.stringify(responseJson))
                if (responseJson.status == true) {
               alert("Report deleted successfully!")

                    this.setModalVisible(false)
                    this.getDocs('bills')
                    this.getDocs('prescription')
                    this.getDocs('labreport')
                    this.getDocs('others')
                    // if(GLOBAL.reportTypes == 'bills'){
                    //     this.getDocs(GLOBAL.reportTypes)

                    // }else if(GLOBAL.reportTypes == 'prescription'){
                    //     this.getDocs(GLOBAL.reportTypes)

                    // }else if(GLOBAL.reportTypes == 'labreport'){
                    //     this.getDocs(GLOBAL.reportTypes)

                    // }else if(GLOBAL.reportTypes == 'others'){
                    //     this.getDocs(GLOBAL.reportTypes)

                    // }


                } else {
                    this.setModalVisible(false)

                    if(GLOBAL.reportTypes == 'bills'){
                        this.getDocs(GLOBAL.reportTypes)

                    }else if(GLOBAL.reportTypes == 'prescription'){
                        this.getDocs(GLOBAL.reportTypes)

                    }else if(GLOBAL.reportTypes == 'labreport'){
                        this.getDocs(GLOBAL.reportTypes)

                    }else if(GLOBAL.reportTypes == 'others'){
                        this.getDocs(GLOBAL.reportTypes)

                    }
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });
}


    actualDownload = (item) => {
  //  alert(JSON.stringify(item))
    var docname = 'picasoid_doc'
    var rs = randomString({
     length: 3,
     numeric: true,
     letters: false,
     special: false,

     });

      rpath = docname + rs
     var totalPath=  '/' + rpath + '.png'
      //alert(totalPath)
     var url = item
       this.setState({
         progress: 0,
         loading: false
       });
       let dirs = RNFetchBlob.fs.dirs;
       RNFetchBlob.config({
         // add this option that makes response data to be stored as a file,
         // this is much more performant.
         path: dirs.DownloadDir + totalPath,
         fileCache: true
       })
         .fetch(
           "GET",
           url,
           {
             //some headers ..
           }
         )
         .progress((received, total) => {
           console.log("progress", received / total);
           this.setState({ progress: received / total });
         })
         .then(res => {
           this.setState({
             progress: 100,
             loading: false
           });
           ToastAndroid.showWithGravity(
             "Your file has been downloaded to downloads folder!",
             ToastAndroid.SHORT,
             ToastAndroid.BOTTOM
           );
         });
     };



     async downloadFile(item) {
         try {
           const granted = await PermissionsAndroid.request(
             PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
             {
               title: "Storage Permission",
               message: "App needs access to memory to download the file "
             }
           );
           if (granted === PermissionsAndroid.RESULTS.GRANTED) {
             this.actualDownload(item);
           } else {
             Alert.alert(
               "Permission Denied!",
               "You need to give storage permission to download the file"
             );
           }
         } catch (err) {
           console.warn(err);
         }
       }


    shareImage=(item)=>{
        this._fancyShareMessage(item)
    }

    _fancyShareMessage=(item)=>{

        var a = 'Hey! Chekout my report from PiCaSoid app '+ item

        Share.share({
                message:a ,url:item
            },{
                tintColor:'green',
                dialogTitle:'Share this report via....'
            }
        ).then(this._showResult);
    }


    toggleOne=()=>{
        this.getDocs('bills')
        this.setState({tone: !this.state.tone})
    }

    toggleTwo = ()=>{
        this.getDocs('prescription')
        this.setState({ttwo : !this.state.ttwo})
    }

    toggleThree=()=>{
        this.getDocs('labreport')
        this.setState({tthree : !this.state.tthree})
    }

    toggleFour=()=>{
        this.getDocs('others')
        this.setState({tfour : !this.state.tfour})
    }


    openImage1=(itemData,fullPath )=>{
//    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath
    GLOBAL.modelImageTitle = itemData.item.image
    GLOBAL.modelImageDate  = itemData.item.added_on
    GLOBAL.modelImageId = itemData.item.id
    this.setModalVisible(true)
}

openImage2=(itemData,fullPath )=>{
//    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath
    GLOBAL.modelImageTitle = itemData.item.image
    GLOBAL.modelImageDate  = itemData.item.added_on
    GLOBAL.modelImageId = itemData.item.id    
    this.setModalVisible(true)
}

openImage3=(itemData,fullPath )=>{
//    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath
    GLOBAL.modelImageTitle = itemData.item.image
    GLOBAL.modelImageDate  = itemData.item.added_on
    GLOBAL.modelImageId = itemData.item.id    
    this.setModalVisible(true)
}

openImage4=(itemData,fullPath )=>{
//    alert(JSON.stringify(itemData.item))
    GLOBAL.modelImage = fullPath
    GLOBAL.modelImageTitle = itemData.item.image
    GLOBAL.modelImageDate  = itemData.item.added_on
    GLOBAL.modelImageId = itemData.item.id    
    this.setModalVisible(true)
}



            renderRowItemothers = (itemData) => {
//            alert(JSON.stringify(itemData))
            if (itemData.item.plusImage) {
      return (
        <TouchableOpacity onPress={()=> this.uploadDoc('others')}>
            <View   style  = {{width:'100%',margin:13, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>
          <Image style={{height: 60, width: 60, resizeMode:'contain', alignSelf:'center' ,}}
            source={require("./add_more.png")} />
                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80, textAlign:'center'}}>
                Add More
                </Text>
        </View>
        </TouchableOpacity>
      );
    }

            var fullPath = this.state.path_urlsothers + itemData.item.image
//            console.log(fullPath)
        return (

            <View   style  = {{width:'30%',margin:5, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>

            {itemData.item.file_type=='pdf' && (
            <View>
            <TouchableOpacity style={{position:'absolute', right:5}} onPress={()=> this.deleteImage(itemData.item.id,GLOBAL.reportTypes)}>
            <Image style={{width:20, height:20, resizeMode:'contain'}} source={require('./circle_cross.png')}/>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:10}}onPress={()=> Linking.openURL(fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={require('./pdf_im.png')}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>
                </View>
                )}
            {itemData.item.file_type!='pdf' && (

            <TouchableOpacity style={{margin:10}}onPress={()=> this.openImage1(itemData, fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={{uri: fullPath}}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>

                )}
            </View>


        )
    }


            renderRowItemlab = (itemData) => {
//            alert(JSON.stringify(itemData))
            if (itemData.item.plusImage) {
      return (
        <TouchableOpacity onPress={()=> this.uploadDoc('labreport')}>
            <View   style  = {{width:'100%',margin:13, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>
          <Image style={{height: 60, width: 60, resizeMode:'contain', alignSelf:'center' ,}}
            source={require("./add_more.png")} />
                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80, textAlign:'center'}}>
                Add More
                </Text>
        </View>
        </TouchableOpacity>
      );
    }

            var fullPath = this.state.path_urlslab + itemData.item.image
//            console.log(fullPath)
        return (

            <View   style  = {{width:'30%',margin:5, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>

            {itemData.item.file_type=='pdf' && (
            <View>
            <TouchableOpacity style={{position:'absolute', right:5}} onPress={()=> this.deleteImage(itemData.item.id,GLOBAL.reportTypes)}>
            <Image style={{width:20, height:20, resizeMode:'contain'}} source={require('./circle_cross.png')}/>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:10}}onPress={()=> Linking.openURL(fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={require('./pdf_im.png')}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>
                </View>
                )}
            {itemData.item.file_type!='pdf' && (

            <TouchableOpacity style={{margin:10}}onPress={()=> this.openImage1(itemData, fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={{uri: fullPath}}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>

                )}
            </View>


        )
    }


        renderRowItempr = (itemData) => {
//            alert(JSON.stringify(itemData))
            if (itemData.item.plusImage) {
      return (
        <TouchableOpacity onPress={()=> this.uploadDoc('prescription')}>
            <View   style  = {{width:'100%',margin:13, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>
          <Image style={{height: 60, width: 60, resizeMode:'contain', alignSelf:'center' ,}}
            source={require("./add_more.png")} />
                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80, textAlign:'center'}}>
                Add More
                </Text>
        </View>
        </TouchableOpacity>
      );
    }

            var fullPath = this.state.path_urlspr + itemData.item.image
//            console.log(fullPath)
        return (

            <View   style  = {{width:'30%',margin:5, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>

            {itemData.item.file_type=='pdf' && (
            <View>
            <TouchableOpacity style={{position:'absolute', right:5}} onPress={()=> this.deleteImage(itemData.item.id,GLOBAL.reportTypes)}>
            <Image style={{width:20, height:20, resizeMode:'contain'}} source={require('./circle_cross.png')}/>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:10}}onPress={()=> Linking.openURL(fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={require('./pdf_im.png')}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>
                </View>
                )}
            {itemData.item.file_type!='pdf' && (

            <TouchableOpacity style={{margin:10}}onPress={()=> this.openImage1(itemData, fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={{uri: fullPath}}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>

                )}
            </View>


        )
    }


            renderRowItem1 = (itemData) => {
//            alert(JSON.stringify(itemData))
            if (itemData.item.plusImage) {
      return (
        <TouchableOpacity onPress={()=> this.uploadDoc('bills')}>
            <View   style  = {{width:'100%',margin:13, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>
          <Image style={{height: 60, width: 60, resizeMode:'contain', alignSelf:'center' ,}}
            source={require("./add_more.png")} />
                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80, textAlign:'center'}}>
                Add More
                </Text>
        </View>
        </TouchableOpacity>
      );
    }


            var fullPath = this.state.path + itemData.item.image
//            console.log(fullPath)
        return (

            <View   style  = {{width:'30%',margin:5, height:'auto',shadowColor: "#000",alignItems:'center', backgroundColor:'white', justifyContent:'center'}}>

            {itemData.item.file_type=='pdf' && (
            <View>
            <TouchableOpacity style={{position:'absolute', right:5}} onPress={()=> this.deleteImage(itemData.item.id,GLOBAL.reportTypes)}>
            <Image style={{width:20, height:20, resizeMode:'contain'}} source={require('./circle_cross.png')}/>
            </TouchableOpacity>
            <TouchableOpacity style={{margin:10}}onPress={()=> Linking.openURL(fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={require('./pdf_im.png')}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>
                </View>
                )}
            {itemData.item.file_type!='pdf' && (

            <TouchableOpacity style={{margin:10}}onPress={()=> this.openImage1(itemData, fullPath)}>
                <Image style={{width:60, height:60, resizeMode:'contain', alignSelf:'center'}} source={{uri: fullPath}}/>

                <Text style = {{fontSize:11,fontFamily:'Konnect-Medium',color:'black', marginTop:10, width:80}}>
                    {itemData.item.image}

                </Text>
                </TouchableOpacity>

                )}
            </View>


        )
    }


    uploadDoc=(doc_cat)=>{
    //    alert(doctype)
//        this.getDocs(doctype)
//        this.selectOneFile(doc_cat)
           GLOBAL.upload_doc_cat = doc_cat
           this.RBSheet.open()
    }

    selectOneFiles=(cate, types)=>{
        console.log(cate+' -<<->>' +types)
    }

    selectCamera=(cate, types)=>{
//    this._handlePressd()
      this.props.navigation.navigate('MyCameraPicker',{finalUploadCamera : this.finalUploadCamera})

      this.RBSheet.close()

//    this.setState({openCamera:1})

    }

    selectGallery=(cate, types)=>{
       this.selectOneFile(cate, types)
    }


    selectPdf=(cate, types)=>{
       this.selectOneFile(cate, types)
    }

    takePicture = async() => {
        if (this.camera) {
          const options = { quality: 0.5, base64: false };
          const data = await this.camera.takePictureAsync(options);
          console.log(data.uri);
        }
      };

     YourOwnComponent = () => {

        return(<View style={{width:window.width, height:250, backgroundColor:'transparent'}}>
                    <Text style = {{fontSize:18,fontFamily:'Konnect-Medium',color:'black', margin:10,}}>
                    Upload Prescription
                    </Text>

                    <TouchableOpacity onPress={()=> this.selectCamera(GLOBAL.upload_doc_cat, 'image')}>
                    <View style={{width:'90%', margin:10,backgroundColor:'white', borderRadius:5,flexDirection:'row', height:50, alignSelf:'center',alignItems:'center'}}>
                    <Image style={{width:30, height:30, resizeMode:'contain', marginLeft:20}}
                    source={require('./pcone.png')}/>
                    <Text style = {{fontSize:18,fontFamily:'Konnect-Regular',color:'black',marginLeft:20 }}>
                    Camera
                    </Text>
                    </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> this.selectGallery(GLOBAL.upload_doc_cat, 'image')}>
                    <View style={{width:'90%', margin:10,backgroundColor:'white', borderRadius:5,flexDirection:'row', height:50, alignSelf:'center',alignItems:'center'}}>
                    <Image style={{width:30, height:30, resizeMode:'contain', marginLeft:20}}
                    source={require('./pctwo.png')}/>
                    <Text style = {{fontSize:18,fontFamily:'Konnect-Regular',color:'black',marginLeft:20 }}>
                    Gallery
                    </Text>
                    </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=> this.selectPdf(GLOBAL.upload_doc_cat, 'pdf')}>
                    <View style={{width:'90%', margin:10,backgroundColor:'white', borderRadius:5,flexDirection:'row', height:50, alignSelf:'center',alignItems:'center'}}>
                    <Image style={{width:30, height:30, resizeMode:'contain', marginLeft:20}}
                    source={require('./pcthree.png')}/>
                    <Text style = {{fontSize:18,fontFamily:'Konnect-Regular',color:'black',marginLeft:20 }}>
                    Pdf
                    </Text>
                    </View>
                    </TouchableOpacity>
            </View>
            )

    }


    render() {

        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

                <View style={styles.container}>
                <Header navigation={this.props.navigation}
                headerName={'MEDICAL REPORT'}/>
        <ScrollView>

               <Text style = {{margin:10,color:'black',fontSize:18,fontFamily:'Konnect-Medium'}}>
                        Report Type</Text>
            <TouchableOpacity activeOpacity={0.99} onPress={()=> this.toggleOne()}>
            <View style={{margin:10, elevation:10,borderRadius:6, flexDirection:'row', width:'95%', height:40,backgroundColor:'white', alignSelf:'center', justifyContent:'space-between'}}>
            <Text style={{fontSize : 15,marginLeft:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center'}}>
            Bills
            </Text>
            {this.state.tone == 0 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./expand.png')}/>
                )}
            {this.state.tone == 1 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./close.png')}/>

                )}

            </View>
            </TouchableOpacity>

            {this.state.tone == 0 && (
                <View style={{height:5}}/>

                )}
            {this.state.tone ==1 && (
                <View>
                {this.state.results.length == 0 && (
    <TouchableOpacity onPress={()=> this.uploadDoc('bills')}>
    <View style={{width:150, height:50, flexDirection:'row',alignSelf:'center',borderRadius:4, borderWidth:1, borderColor:'#800000', alignItems:'center', justifyContent:'center', marginBottom:10}}>
            <Image style={{width:25, height:25, resizeMode:'contain'}} source={require('./dplus.png')}/>
            <Text style={{fontSize : 13,color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center',marginLeft:10 }}>
            ADD REPORTS
            </Text>
    </View>
    </TouchableOpacity>            


                    )}

                {this.state.results.length!=0 && (
                <FlatList style = {{marginTop:5,}}
                    data={[...this.state.results, {plusImage: true}]}
                    numColumns={3}
                    keyExtractor={this._keyExtractor}
                    renderItem={this.renderRowItem1}
                    extraData={this.state}
                />

                    )}
                    </View>
                )}

            <TouchableOpacity activeOpacity={0.99} onPress={()=> this.toggleTwo()}>
            <View style={{margin:10, elevation:10,borderRadius:6, flexDirection:'row', width:'95%', height:40,backgroundColor:'white', alignSelf:'center', justifyContent:'space-between'}}>
            <Text style={{fontSize : 15,marginLeft:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center'}}>
            Prescription
            </Text>
            {this.state.ttwo == 0 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./expand.png')}/>
                )}
            {this.state.ttwo == 1 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./close.png')}/>

                )}
            </View>
            </TouchableOpacity>

               {this.state.ttwo == 0 && (
                <View style={{height:5}}/>

                )}
            {this.state.ttwo ==1 && (
                <View>
                {this.state.resultspr.length == 0 && (
    <TouchableOpacity onPress={()=> this.uploadDoc('prescription')}>
    <View style={{width:150, height:50, flexDirection:'row',alignSelf:'center',borderRadius:4, borderWidth:1, borderColor:'#800000', alignItems:'center', justifyContent:'center', marginBottom:10}}>
            <Image style={{width:25, height:25, resizeMode:'contain'}} source={require('./dplus.png')}/>
            <Text style={{fontSize : 13,color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center',marginLeft:10 }}>
            ADD REPORTS
            </Text>
    </View>
    </TouchableOpacity>            

                    )}

                {this.state.resultspr.length!=0 && (
                <FlatList style = {{marginTop:5,}}
                    data={[...this.state.resultspr,{ plusImage: true } ]}
                    numColumns={3}
                    keyExtractor={this._keyExtractor}
                    renderItem={this.renderRowItempr}
                    extraData={this.state}
                />

                    )}
                    </View>
                )}


            <TouchableOpacity activeOpacity={0.99} onPress={()=> this.toggleThree()}>
            <View style={{margin:10, elevation:10,borderRadius:6, flexDirection:'row', width:'95%', height:40,backgroundColor:'white', alignSelf:'center', justifyContent:'space-between'}}>
            <Text style={{fontSize : 15,marginLeft:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center'}}>
            Lab Reports
            </Text>
            {this.state.tthree == 0 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./expand.png')}/>
                )}
            {this.state.tthree == 1 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./close.png')}/>

                )}
            </View>
            </TouchableOpacity>

            {this.state.tthree == 0 && (
                <View style={{height:5}}/>

                )}
            {this.state.tthree ==1 && (
                <View>
                {this.state.resultslab.length == 0 && (
    <TouchableOpacity onPress={()=> this.uploadDoc('labreport')}>
    <View style={{width:150, height:50, flexDirection:'row',alignSelf:'center',borderRadius:4, borderWidth:1, borderColor:'#800000', alignItems:'center', justifyContent:'center', marginBottom:10}}>
            <Image style={{width:25, height:25, resizeMode:'contain'}} source={require('./dplus.png')}/>
            <Text style={{fontSize : 13,color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center',marginLeft:10 }}>
            ADD REPORTS
            </Text>
    </View>
    </TouchableOpacity>            

                    )}

                {this.state.resultslab.length!=0 && (
                <FlatList style = {{marginTop:5,}}
                    data={[...this.state.resultslab,{ plusImage: true } ]}
                    numColumns={3}
                    keyExtractor={this._keyExtractor}
                    renderItem={this.renderRowItemlab}
                    extraData={this.state}
                />

                    )}
                    </View>
                )}

            <TouchableOpacity activeOpacity={0.99} onPress={()=> this.toggleFour()}>
            <View style={{margin:10, elevation:10,borderRadius:6, flexDirection:'row', width:'95%', height:40,backgroundColor:'white', alignSelf:'center', justifyContent:'space-between'}}>
            <Text style={{fontSize : 15,marginLeft:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center'}}>
            Others
            </Text>
            {this.state.tfour == 0 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./expand.png')}/>
                )}
            {this.state.tfour == 1 && (
            <Image style={{width:18, height:18, resizeMode:'contain', margin:10}}
            source={require('./close.png')}/>

                )}
            </View>
            </TouchableOpacity>



            {this.state.tfour == 0 && (
                <View style={{height:5}}/>

                )}
            {this.state.tfour ==1 && (
                <View>
                {this.state.resultsothers.length == 0 && (
    <TouchableOpacity onPress={()=> this.uploadDoc('others')}>
    <View style={{width:150, height:50, flexDirection:'row',alignSelf:'center',borderRadius:4, borderWidth:1, borderColor:'#800000', alignItems:'center', justifyContent:'center', marginBottom:10}}>
            <Image style={{width:25, height:25, resizeMode:'contain'}} source={require('./dplus.png')}/>
            <Text style={{fontSize : 13,color :'#800000',fontFamily:'Konnect-Medium',alignSelf:'center',marginLeft:10 }}>
            ADD REPORTS
            </Text>
    </View>
    </TouchableOpacity>            

                    )}

                {this.state.resultsothers.length!=0 && (

                <FlatList style = {{marginTop:5,}}
                    data={[...this.state.resultsothers,{ plusImage: true } ]}
                    numColumns={3}
                    keyExtractor={(item, index) => String(index)}
                    onEndReachedThreshold={0.1}                   
                    renderItem={this.renderRowItemothers}
                    extraData={this.state}
                />


                )}

                    </View>
                )}


            <RBSheet
                  ref={ref => {
                    this.RBSheet = ref;
                  }}
                  height={300}
                  duration={250}
                  closeOnDragDown={true}
                  customStyles={{
                    container: {
                      justifyContent: "center",
                      alignItems: "center", 
                      borderTopLeftRadius:10,
                      borderTopRightRadius:10,
                      backgroundColor:'#f7f7f7'
                    }
                  }}
                >
                  {this.YourOwnComponent()}
                </RBSheet>

 <Modal
           animationType="slide"
           transparent={true}
           visible={this.state.modalVisible}
           onRequestClose={() => {
//             Alert.alert('Modal has been closed.');
             this.setModalVisible(!this.state.modalVisible)
           }}>
           <TouchableOpacity 
            style={{
                     flex: 1,
                     flexDirection: 'column',
                     justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',
                     alignItems: 'center'}} 
            activeOpacity={1} 
            onPressOut={() => {this.setModalVisible(false)}}
          >
           <View style={{

                     flexDirection: 'column',
                     justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',
                     alignItems: 'center'}}>
               <View style={{
                       width: 300,backgroundColor: 'white',
                       height: 400}}>
                       <View style={{width: '100%', }}>

                       <View style={{flexDirection: 'column',}}>
                       <Image style={{width:'100%',height:'80%'}} source={{uri: GLOBAL.modelImage}}/>

                       <View style={{width:'100%', flexDirection:'row', justifyContent:'space-between'}}>

                       <Text style={{color:'black', fontFamily:'Konnect-Medium',fontSize:15,width:'30%', }}numberOfLines={1}>{GLOBAL.modelImageTitle}</Text>
                       <Text style={{color:'black', fontFamily:'Konnect-Medium',fontSize:15 }}>{GLOBAL.modelImageDate}</Text>
                       </View>                       
                       <View style={{width:'100%', flexDirection:'row', justifyContent:'space-between'}}>
                       <TouchableOpacity onPress={()=> this.downloadFile(GLOBAL.modelImage)}>
                       <View style={{height:40,width:80, flexDirection:'row', backgroundColor:'#800000', margin:10,alignItems:'center', borderRadius:8}}>
                       <Image style={{width:15, height:15, resizeMode:'contain', margin:5}}
                       source={require('./im_download.png')}/>
                       <Text style={{color:'white', fontFamily:'Konnect-Medium',fontSize:11 }}>Downlod</Text>
                       </View>
                       </TouchableOpacity>

                       <TouchableOpacity onPress={()=> this.deleteImage(GLOBAL.modelImageId, GLOBAL.reportTypes)}>
                       <View style={{height:40,width:80, flexDirection:'row', backgroundColor:'#800000', margin:10,alignItems:'center', borderRadius:8, padding:6}}>
                       <Image style={{width:15, height:15, resizeMode:'contain', margin:5}}
                       source={require('./im_delete.png')}/>
                       <Text style={{color:'white', fontFamily:'Konnect-Medium',fontSize:11 }}>Delete</Text>
                       </View>
                       </TouchableOpacity>

                       <TouchableOpacity onPress={()=>this.shareImage(GLOBAL.modelImage)}>
                       <View style={{height:40,width:80, flexDirection:'row', backgroundColor:'#800000', margin:10,alignItems:'center', borderRadius:8, padding:6}}>
                       <Image style={{width:15, height:15, resizeMode:'contain', margin:5}}
                       source={require('./im_share.png')}/>
                       <Text style={{color:'white', fontFamily:'Konnect-Medium',fontSize:11 }}>Share</Text>
                       </View>
                       </TouchableOpacity>

                       </View>

                       </View>

                       </View>


               </View>

           </View>
           </TouchableOpacity>
         </Modal>

    </ScrollView>


                </View>

        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    container: {
        flex:1,
        backgroundColor :'white',
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },

})
