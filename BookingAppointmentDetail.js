import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    Share,
    AsyncStorage,
    Modal
} from 'react-native';
const GLOBAL = require('./Global');
import Button from 'react-native-button';
const window = Dimensions.get('window');
import Header from './Header.js';
import DatePicker from 'react-native-datepicker';

import { TextField } from 'react-native-material-textfield';
import ImagePicker from 'react-native-image-picker';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
type Props = {};

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

var radio_props_one = [
    {label: 'Male', value: 0 },
    {label: 'Female', value: 1 }
];

export default class BookingAppointmentDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
          avatarSource: null,
            imageget:0,
            recognized: '',
            started: '',
            results: [],
            name :GLOBAL.myname,
            value:GLOBAL.mygenderedit,
            select:'',
            create:'',
            dob:GLOBAL.myDobfrom,
            address:GLOBAL.myAddfrom,
            area :'',
            city:GLOBAL.myCityfrom,issue:'',
            member:[],
            images: [],
            modalVisible: false,
            showMemDetails:0,
            memDetails:''
        };

       this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
    }


    static navigationOptions = ({ navigation }) => {
        return {
               header: () => null,
        }
    }

    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }

    setModalVisible=(visible)=> {
        this.setState({modalVisible: visible});
    }


    selectPhotoTapped=()=> {

   const options = {
   title: 'Choose Photo',
   storageOptions: {
     skipBackup: true,
     path: 'images',
   },
  };

  ImagePicker.showImagePicker(options, (response) => {
//  console.log('Response = ', response);

  if (response.didCancel) {
    console.log('User cancelled image picker');
  } else if (response.error) {
    console.log('ImagePicker Error: ', response.error);
  } else if (response.customButton) {
    console.log('User tapped custom button: ', response.customButton);
  } else {
    const source = { uri: response.uri };
//    alert(JSON.stringify(source))
    this.setState({
     avatarSource: source,
   });
//    GLOBAL.hBookingImagesOffline =  source.uri
    console.log(JSON.stringify(source.uri))
     this.setState({imageget :1})
                const url = GLOBAL.BASE_URL +  'image_attchment_upload'
                const data = new FormData();
                data.append('user_id', GLOBAL.user_id);
                data.append('flag',1);
                data.append('image', {
                    uri: response.uri,
                    type: 'image/jpeg', // or photo.type
                    name: 'image.png'
                });
                fetch(url, {
                    method: 'post',
                    body: data,
                    headers: {
                        'Content-Type': 'multipart/form-data',
                    }

                }).then((response) => response.json())
                    .then((responseJson) => {
                        //       this.hideLoading()
                        console.log(JSON.stringify(responseJson.images[0].image))
                        GLOBAL.hBookingImagesOffline =  responseJson.images[0].image

      //                  this.setState({myimages:responseJson.images})

    //                    this.setState({path:responseJson.path})

                    });


 }
});
}


    _handleStateChange = (state) => {

   //         console.log(JSON.stringify(GLOBAL.mymember))



            const interest = this.state.member.concat(GLOBAL.mymember)
//            alert(JSON.stringify(interest))



         //   const interests = [...interest, ...a];
            //
            // var b = interest.concat(a)
            //
             this.setState({member:interest})


    }

     getInterPrice=()=>{
        const url = GLOBAL.BASE_URL +  'settings'
        fetch(url, {
            method: 'GET',
            
        }).then((response) => response.json())
            .then((responseJson) => {
//                alert(JSON.stringify(responseJson))
                if (responseJson.status == true) {

                    GLOBAL.androidappUrl = responseJson.play_store_url
                    GLOBAL.iosappUrl = responseJson.app_url
                }else {
//                    alert('No Data Found')
                }
            })
            .catch((error) => {
                console.error(error);
            });

     }

    componentDidMount(){
        GLOBAL.showMemDetails=0
        this.props.navigation.addListener('willFocus',this._handleStateChange);
        this.getMembers()
        this.getInterPrice()
    }

    getMembers=()=>{
        const url = GLOBAL.BASE_URL +  'list_member'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "user_id":GLOBAL.user_id,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
             // s  alert(JSON.stringify(responseJson))

                if (responseJson.status == true) {
                    this.setState({results:responseJson.member_list})

                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

    _handlePress() {

   //     console.log('---------')
     //   console.log(JSON.stringify(GLOBAL.appointmentArray))
        if (this.state.value == 0){
            GLOBAL.onlinegender = "Male";
        }else{
            GLOBAL.onlinegender = "Female";
        }

      //  console.log('hello '+ GLOBAL.mymember.id)

// 1|5|video|1|self|0|04:30pm|2019-12-12|name|gender|dob|
// address|area|pincode|city_state|1|50|0|0|0|        

// user_id|booking_type|module|doctor_id|booking_for|member_id|booking_time|booking_date|
// name|gender|dob|address|area|pincode|
// city_state|total_amount|booking_amount|wallet_amount|referral_amount|discount_amount|images

    // My parameters Harshit
    GLOBAL.hproblem = this.state.issue
    GLOBAL.finalType= 5
    GLOBAL.hModule= 'video'
    GLOBAL.hDoctor_id = GLOBAL.appointmentArray.id
    GLOBAL.myDobfrom = this.state.dob
//    alert(GLOBAL.myDobfrom)
    if(this.state.showMemDetails == 0){
    
    GLOBAL.hbooking_for = 'self'
    GLOBAL.hmember_id = 0

    }else{
    GLOBAL.hbooking_for = 'member'
    GLOBAL.hmember_id = this.state.memDetails.id

    }
    
        GLOBAL.onlinename = "";
        GLOBAL.onlinedob = "";
        GLOBAL.onlineaddress = "";
        GLOBAL.onlinearea = "";
        GLOBAL.onlinecity = "";



        GLOBAL.onlineMember = this.state.member



        this.props.navigation.navigate('BookingDetailFinal')
    }


    selectedFirst = (indexs) => {
        GLOBAL.typelist=''
        this.props.navigation.navigate('ListMember')

    }

    selectMember = (item, index)=>{
        this.setModalVisible(false)        

        this.setState({memDetails: item})
        this.setState({showMemDetails: 1})

        console.log(JSON.stringify(item))

    }

addMemberButton=()=>{
    alert('hi')
}

    _renderMembers = ({item,index}) => {

        return (
            <TouchableOpacity style={{marginLeft : 5,width:'95%', backgroundColor: 'white',marginRight:5,
                marginTop: 5,marginBottom:5,borderWidth:1,borderColor:'#800000',borderRadius:0,height:100}}
            onPress={() => this.selectMember(item, index)} activeOpacity={0.99}>
                <View style={{width:'100%', backgroundColor: 'white',flexDirection:'row', alignItems:'center'}}>

                <Image style={{width:50, height:50,  marginLeft:10, borderRadius:25, borderColor:'#800000', borderWidth:2}}
                source={require('./user.png')}/>

                <View style={{flexDirection:'column', width:'80%', margin:10}}>
                    <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',}}>
                        {item.member_name}
                    </Text>

                    <Text style={{marginLeft : 5,fontSize : 13,color :'#555755',fontFamily:'Konnect-Medium',}}>
                        {item.email}
                    </Text>
                   <Text style={{marginLeft : 5,fontSize : 13,color :'#555755',fontFamily:'Konnect-Medium',}}>
                        {item.member_mobile}
                    </Text>
                   <Text style={{marginLeft : 5,fontSize : 13,color :'#555755',fontFamily:'Konnect-Medium',}}>
                        {item.relation}
                    </Text>

                </View>                    
                </View>
            </TouchableOpacity>
        )
    }


    _fancyShareMessage=()=>{
        //console.log(GLOBAL.appointmentArray)

        var a = 'Checkout '+GLOBAL.appointmentArray.name+ ' at PiCaSoid app.'+ '\n'+
        'Current Hospital/Clinic: '+GLOBAL.appointmentArray.hospital_name+'\n'+
        'Department: '+GLOBAL.appointmentArray.dr_depart_array+
        '\n'+'Speciality: '+GLOBAL.appointmentArray.speciality_detail_array+'\n'+
        'Experience: ' +GLOBAL.appointmentArray.experience + ' Years'+ '\n'+
        'Download the app from here:'+'\n'+
        'Android: '+GLOBAL.androidappUrl+'\n'+
        'iOS: '+GLOBAL.iosappUrl;

        console.log(a)
        Share.share({
                message:a , url:GLOBAL.androidappUrl
            },{
                tintColor:'green',
                dialogTitle:'Share this Doctor via....'
            }
        ).then(this._showResult);
    }


    _renderItems = ({item,index}) => {

        return (

            <TouchableOpacity onPress={() => this.selectedFirst(index)
            }>

                 <View style = {{backgroundColor:'transparent',margin:1}}>
                                <Image style = {{width :60 ,height :60,margin:10,resizeMode:'contain'}}
                                       source={require('./myself.png')}/>

                                <Text style={{fontSize : 14,color :'#800000',fontFamily:'Konnect-Regular',textAlign:'center'}}>

                                    {item.member_name}
                                </Text>
                 </View>


            </TouchableOpacity>
        )
    }
    render() {
        var speciality = GLOBAL.appointmentArray.speciality_detail_array

        let { name } = this.state;
        let { dob } = this.state;
        let { address } = this.state;
        let { area } = this.state;
        let { city } = this.state;
        let { issue } = this.state;
        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}
                                       size="large" color='#800000' />
                </View>
            )
        }
      //  console.log(this.state.memDetails.member_name)

        return (

                <View style={styles.container}>
                <Header navigation={this.props.navigation}
                headerName={'OFFLINE APPOINTMENT'}/>

                    <KeyboardAwareScrollView keyboardShouldPersistTaps='always'>

                        <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>
                            <View style = {{flexDirection:'row',width :'100%'}}>

                                <View>
                                    <Image style = {{width :60 ,height :60,borderRadius: 30,margin:10, borderColor:'#800000', borderWidth:1}}
                                           source={{ uri: GLOBAL.appointmentArray.image }}/>
                                    <View style = {{marginLeft:18,backgroundColor:'#800000',borderRadius:4,width:40,height:20,flexDirection:'row',alignItems:'center'}}>
                                        <Image style = {{width :8 ,height :8,resizeMode:'contain',marginLeft:5}}
                                               source={require('./star.png')}/>

                                        <Text style={{marginLeft : 5,fontSize : 10,color :'white',fontFamily:'Konnect-Medium',}}>

                                            {GLOBAL.appointmentArray.ratting}
                                        </Text>
                                    </View>
                                    {GLOBAL.appointmentArray.doctor_avail_status == 1 && (

                                        <Text style={{marginLeft : 12,marginTop:5,fontSize : 11,color :'#3DBA56',fontFamily:'Konnect-Medium',width:50,textAlign:'center',marginTop:2,marginBottom:15}}>

                                            Online
                                        </Text>
                                    )}
                                    {GLOBAL.appointmentArray.doctor_avail_status != 1 && (

                                        <Text style={{marginLeft :12,marginTop:5,fontSize : 11,color :'red',fontFamily:'Konnect-Medium',width:50,textAlign:'center',marginTop:2,marginBottom:15}}>

                                            Offline
                                        </Text>
                                    )}


                                </View>

                                <View>

                                    <View style = {{flexDirection:'row',width:'100%'}}>
                                        <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :'70%',marginTop:18,}}>

                                            {GLOBAL.appointmentArray.name}
                                        </Text>

                                      <TouchableOpacity style={{position:'absolute',right:-12,top:10}}
                                      onPress={()=> this._fancyShareMessage()}>
                                        <Image source={require('./share.png')}
                                         style={{height:25,width:25,resizeMode:'contain'}}/>
                                      </TouchableOpacity>


                                    </View>

                                    <View style = {{flexDirection:'row', height:'auto', width:'80%'}}>
                                        <Text style={{marginLeft : 6,fontSize : 12,color :'#8F8F8F',height:'auto',fontFamily:'Konnect-Medium',width :'90%'}}>

                                            {speciality}
                                        </Text>

                                    </View>

                               <View style = {{flexDirection:'row'}}>
                                   <Image style = {{width :20 ,height :20,resizeMode:'contain'}}
                                          source={require('./location.png')}/>

                                   <Text style={{marginLeft : 5,width:window.width - 150,height:'auto',fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',}}>

                                       Hospital/Clinic: {GLOBAL.appointmentArray.lat_long_address}
                                   </Text>

                               </View>

                                    <View style = {{flexDirection:'row',justifyContent:'space-between',marginLeft:6,marginTop:8}}>

                                        <View>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium'}}>

                                                Experience
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.experience} Years
                                            </Text>
                                        </View>

                                        <View>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Likes
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.like}
                                            </Text>
                                        </View>

                                        <View style = {{marginRight:50}}>
                                            <Text style={{fontSize : 12,color :'#AAAAAA',fontFamily:'Konnect-Medium',}}>

                                                Reviews
                                            </Text>
                                            <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                                {GLOBAL.appointmentArray.total_review}
                                            </Text>
                                        </View>

                                    </View>

                                    <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',marginLeft:6,marginTop:5, marginBottom:5}}>

                                        Consult offline for ₹ {GLOBAL.appointmentArray.normal_appointment_price}/- onwards
                                    </Text>

                                    <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',marginLeft:6, marginBottom:5}}>

                                    Booking Charge is ₹ {GLOBAL.pica_booking_charge_offline}/- only
                                    </Text>

                                </View>

                            </View>
                        </View>
                       <TouchableOpacity onPress={()=> {GLOBAL.typelist='';
                        this.setModalVisible(true)}}>

                        <View style={{flexDirection:'row',width:'94%',height:60,alignSelf:'center',marginTop:'1.5%',borderRadius:7,backgroundColor:'white'}}>
 
                        <TextInput
                            style={{ height: 46,fontSize:18,marginLeft:20,width:'60%',color:'#0000004D',marginTop:7,marginLeft:8}}
                            // Adding hint in TextInput using Placeholder option.
                            placeholder="Select Members"
                            placeholderTextColor = 'grey'
                            maxLength={35}
                            editable={false}
                            // Making the Under line Transparent.
                            underlineColorAndroid="transparent"
                            value = {this.state.select}
                            onChangeText={(text) => this.setState({select:text})}
                        />

                          <Image style={{width:22,height:22,marginTop:19,marginLeft:'28%'}}
                            source={require('./rights.png')}/>


                        </View>
                        </TouchableOpacity>
                        
                        <Text style={{fontSize:19,fontFamily:'Konnect-Medium',color:'#000000',marginTop:'8%',marginLeft:'4%'}}>Describe your issue in details below:</Text>

                        <View style={{width:'100%',alignSelf:'center',marginTop:5,backgroundColor:'white',marginBottom:'5%'}}>

                        <View style={{margin:'4%'}}>
                         <TextInput style={{height:75, borderBottomColor:'#bfbfbf', borderBottomWidth:1, fontFamily:'Konnect-Medium'}}
                                value={issue}
                                multiline={true}
                                textAlignVertical={'top'}
                                placeholder={'For e.g. I am losing hair quickly and have tried \nall shampoos but no effect yet. I am not sure what \nelse can I do. Can you please help me with this?'}
                                onChangeText={ (issue) => this.setState({ issue }) }
                                tintColor = {'#800000'}
                            />
                            </View>


                         <TouchableOpacity style={{alignSelf:'center', width:'70%', borderRadius:6,marginTop:5}}
                           onPress={this.selectPhotoTapped.bind(this)}>
                           {this.state.imageget==0 && (

                           <View
                            style={{alignSelf:'center',height:60,width:'100%',borderRadius:6,borderWidth:1,borderColor:'#800000',marginTop:-1}}>
                            <Text style={{fontSize:18,fontFamily:'Konnect-Medium',color:'#800000',marginTop:15,alignSelf:'center'}}>Attach Images/Reports</Text>
                           </View>
                             )}

                           {this.state.imageget==1 && (
                           <Image source={this.state.avatarSource}
                            style={{alignSelf:'center', height:100,width:100, borderRadius:6}}/>

                             )}


                        </TouchableOpacity>

                        <Text style={{fontSize:17,fontFamily:'Konnect-Medium',color:'#0000004D',marginTop:3,marginBottom:10,alignSelf:'center'}}>Attach Images/Documents here</Text>

                        </View>



                    <Text style={{fontSize : 20,color :'#132439',fontFamily:'Konnect-Regular',margin:10}}>

                        Basic Information
                    </Text>

 {this.state.showMemDetails==0 && (
 <View style = {{backgroundColor:'white',borderRadius:8,marginLeft:10,width:window.width - 20}}>
                            <View style = {{marginLeft:10}}>
                            <TextField
                                label= 'Name'
                                value={name}
                                onChangeText={ (name) => this.setState({ name }) }
                                tintColor = {'#800000'}
                            />
                            <Text style={{fontSize : 12,color :'rgba(0,0,0,0.5)',fontFamily:'Konnect-Medium',}}>

                               Gender
                            </Text>

                            <RadioForm style={{ marginTop:12}}
                                       labelStyle={{paddingRight:20}}
                                       radio_props={radio_props_one}
                                       initial={this.state.value}
                                       buttonSize={10}
                                       formHorizontal={true}
                                       buttonColor={'#800000'}
                                       labelHorizontal={true}
                                       animation={false}
                                       labelColor={'black'}
                                       selectedButtonColor={'#800000'}
                                       onPress={(value) => {this.setState({value:value})}}
                            />
{/*                            <TextField
                                label= 'Date of Birth'
                                editable={false}
                                onChangeText={ (dob) => this.setState({ dob }) }
                                tintColor = {'#800000'}
                            />
                        */}
                            <Text style={{fontSize : 12,color :'rgba(0,0,0,0.5)',fontFamily:'Konnect-Medium',marginTop:10}}>
                             Date of Birth
                            </Text>
                             <DatePicker
                              style={{width: 200,marginTop:5}}
                              date={this.state.dob}
                              mode="date"
                              showIcon={false}
                              placeholder={this.state.dob}
                              format="YYYY-MM-DD"
                              minDate="1960-01-01"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              customStyles={{
                                dateInput: {
                                  marginLeft: -130, borderWidth:0, color:'black'
                                }
                              }}
                              onDateChange={(dob) => {this.setState({dob: dob})}}
                            />

                            <View style={{backgroundColor:'#bfbfbf', height:0.39}}/>

                         
                            <TextField
                                label= 'Address'
                                value={address}
                                onChangeText={ (address) => this.setState({ address }) }
                                tintColor = {'#800000'}
                            />

{/*                            <TextField
                                label= 'Area Locality'
                                value={area}
                                onChangeText={ (area) => this.setState({ area }) }
                                tintColor = {'#800000'}
                            /> */}
                            <TextField
                                label= 'City'
                                value={city}
                                onChangeText={ (city) => this.setState({ city }) }
                                tintColor = {'#800000'}
                            />

                        </View>
                        </View>


 )}                   

 {this.state.showMemDetails ==1 &&(
 <View style = {{backgroundColor:'white',borderRadius:8,marginLeft:10,width:window.width - 20}}>
                            <View style = {{marginLeft:10, marginTop:10}}>
                            <TextInput style={{color:'black'}}
                                label= 'Name'
                                editable={false}
                                value={this.state.memDetails.member_name}
                                onChangeText={ (name) => this.setState({ name }) }
                                tintColor = {'#800000'}
                            />
                            <View style={{backgroundColor:'#bfbfbf', height:1}}/>
                            <TextInput style={{color:'black'}}
                                label= 'Gender'
                                value={this.state.memDetails.gender}
                                editable={false}
                                onChangeText={ (gender) => this.setState({ gender }) }
                                tintColor = {'#800000'}
                            />
                            <View style={{backgroundColor:'#bfbfbf', height:1}}/>

                            <TextInput style={{color:'black'}}
                                label= 'Date of Birth'
                                value={this.state.memDetails.member_dob}
                                editable={false}
                                onChangeText={ (dob) => this.setState({ dob }) }
                                tintColor = {'#800000'}
                            />
                            <View style={{backgroundColor:'#bfbfbf', height:1}}/>

                            <TextInput style={{color:'black'}}
                                label= 'Relation'
                                value={this.state.memDetails.relation}
                                onChangeText={ (relation) => this.setState({ relation }) }
                                tintColor = {'#800000'}
                            />
                            <View style={{backgroundColor:'#bfbfbf', height:1, marginBottom:10}}/>

                        </View>
                        </View>
    )}
                        


                        <Button
                          style={{alignSelf:'center', fontSize:20, fontFamily:'Konnect-Medium',color:'white'}}
                          onPress={() => this._handlePress()}
                          containerStyle={{height:46,width:'90%',borderRadius:6,alignSelf:'center',justifyContent:'center',backgroundColor:'#800000',marginTop:18,marginBottom:80}}>
                          PROCEED
                        </Button>
                    </KeyboardAwareScrollView>

         <Modal
           animationType="slide"
           transparent={true}
           visible={this.state.modalVisible}
           onRequestClose={() => {
//             Alert.alert('Modal has been closed.');
             this.setModalVisible(!this.state.modalVisible)
           }}>
                         <TouchableOpacity style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            alignItems: 'center', borderRadius:8}}
                            activeOpacity={1}
                            onPressOut={() => {this.setModalVisible(false)}}
                            >
                            <View style={{width: '80%',backgroundColor: 'white',height: 350, borderRadius:8}}>
                              <View style={{width: '100%',  backgroundColor:'white', borderRadius:8}}>

                              <View style={{flexDirection:'row', width:'100%', backgroundColor:'#800000', height:60,
                               borderTopLeftRadius:8, borderTopRightRadius:8, borderTopLeftWidth:1,justifyContent:'space-between',alignItems:'center',
                               borderTopRightWidth:1, borderTopRightColor:'transparent', borderTopLeftColor:'transparent'}}>
                              <Text style={{fontSize: 17, color:'white', fontFamily: 'Konnect-Regular',margin:10 }}>Select Member</Text>
                              <TouchableOpacity onPress={()=> this.setModalVisible(false)}>
                              <Image style={{width:25, height:25, resizeMode:'contain', marginRight:10}} source={require('./cross.png')}/>
                              </TouchableOpacity>

                                </View>
                {this.state.results.length == 0 && (
            <Text style={{fontSize : 13,marginTop:15,color :'black',fontFamily:'Konnect-Medium',alignSelf:'center', textAlign:'center'}}>
            No Members added yet!
            </Text>
                )}

                {this.state.results.length!=0 &&(
                                    <FlatList style= {{flexGrow:0,marginBottom:10}}
                                              data={this.state.results}
                                              numColumns={1}
                                              extraData={this.state}
                                              keyExtractor = { (item, index) => index.toString() }
                                              renderItem={this._renderMembers}
                                    />
                                )}

{/*                    <Button
                        style={{padding:7,marginTop:20,marginBottom:10,fontSize: 20, color: 'white',backgroundColor:'#800000',marginLeft:'5%',width:'90%',height:40,fontFamily:'Konnect-Medium',borderRadius:4}}
                        styleDisabled={{color: 'red'}}
                        onPress={() => this.addMemberButton()}>
                        ADD MEMBER
                    </Button>
*/}
                                </View>
                            </View>
                        </TouchableOpacity>
         </Modal>
                </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor :'#f1f1f1',
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },

})