import { createAppContainer} from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';

import Splash from './Splash.js';
import Slider from './Slider.js';
import Login from './Login.js';
import Otp from './Otp.js';
import Register from './Register.js';
import Forgot from './Forgot.js';
import ShowMember from './ShowMember.js';
import BasicDetail from './BasicDetail.js';
import ListMember from './ListMember.js';
import MyDocument from './MyDocument.js';
import LabHistory from './LabHistory.js';
import OnlineChat from './OnlineChat.js';
import UploadHealthcare from './UploadHealthcare.js';
import UploadPersonal from './UploadPersonal.js';
import AskConfirmation from './AskConfirmation.js';
import OpdHealth from './OpdHealth.js';
import LabHistoryDetail from './LabHistoryDetail.js';
import RatingView from './RatingView.js';
import Proceed from './Proceed.js';
import Chat from './Chat.js';
import Ccavenue from  './Ccavenue.js';
import RatingPost from './RatingPost.js';
import DoctorVisit   from './DoctorVisit.js';
import DoctorVisitDetail from './DoctorVisitDetail.js';
import Emergency from './Emergency.js';
import BookingAppointment from './BookingAppointment.js';
import Insurance from './Insurance.js';
import BookingAppointmentDetail from './BookingAppointmentDetail.js';
import BookingDetailFinal from './BookingDetailFinal.js';
import Confirmation from './Confirmation.js';
import DoctorDetail from './DoctorDetail.js';
import HospitalList from './HospitalList.js';
import MyCameraPicker from './MyCameraPicker.js';
import AddMember from './AddMember.js';
import HospitalDetail from './HospitalDetail.js';
import AmbulanceBooking from './AmbulanceBooking.js';
import Upload from './Upload.js';
import Location from './Location.js';
import UploadDoctor from './UploadDoctor.js';
import UploadLab from './UploadLab.js';
import Landing from './Landing.js';
import Home from './Home.js';
import Search from './Search.js';
import ArticleDescription from './ArticleDescription.js';
import Labtest from './Labtest.js';
import Pharmacy from './Pharmacy.js';
import VideoCall from './VideoCall.js';
import Support from './Support.js';
import Thankyou from './Thankyou.js';
import Appointment from './Appointment.js';
import AppointmentDetail from './AppointmentDetail.js';
import AppointmentResc from './AppointmentResc.js';
import Payment from './Payment.js';
import OnlineBooking from './OnlineBooking.js';
import Filter from './Filter.js';
import SpecialityFilter from './SpecialityFilter.js';
import HospitalFilter from './HospitalFilter.js';
import Department from './Department.js';
import OnlineVideo from './OnlineVideo.js';
import OfflineBooking from './OfflineBooking.js';
import Speciality from './Speciality.js';
import Notification from './Notification.js';
import SearchSpeciality from './SearchSpeciality.js';
import EditProfile from './EditProfile.js';
import Drawer  from  './Drawer.js';
import ViewVideo from './ViewVideo.js';

import React, {Component} from 'react';

const DrawerNavigator = createDrawerNavigator({
    Home:{
        screen: Home ,

        navigationOptions: ({ navigation }) => ({

            header: () => null,

        }),
    }

},{
    initialRouteName: 'Home',
    contentComponent: Drawer,
    drawerWidth: 270
});

const StackNavigator = createStackNavigator({




        Splash: { screen: Splash },
            Slider: { screen: Slider },
    Filter:{screen:Filter},
        Proceed:{screen:Proceed},
    DrawerNavigator:{screen: DrawerNavigator,
        navigationOptions: ({ navigation }) => ({
            header:null,
        }),
    },
        OnlineBooking:{screen:OnlineBooking},
        SpecialityFilter:{screen:SpecialityFilter},
        HospitalFilter:{screen:HospitalFilter},
        Department:{screen:Department},
        OnlineVideo:{screen:OnlineVideo},
        SearchSpeciality:{screen:SearchSpeciality},
        EditProfile:{screen:EditProfile},
        Ccavenue: { screen: Ccavenue },
        LabHistoryDetail:{screen:LabHistoryDetail},
        ShowMember:{screen:ShowMember},
        VideoCall: { screen: VideoCall },
        Home: { screen: Home },
        LabHistory:{screen:LabHistory},
        Landing: { screen: Landing },
        Search: { screen: Search },
        Payment: { screen: Payment },
        Support:{screen:Support},
        OfflineBooking: { screen: OfflineBooking },
        AddMember:{screen:AddMember},
        Notification:{screen: Notification},
        OnlineChat:{screen:OnlineChat},
        UploadHealthcare:{screen: UploadHealthcare},
        Login: { screen: Login },
        Otp: { screen: Otp },
        Register: { screen: Register },
        Forgot: { screen: Forgot },
        BasicDetail: { screen: BasicDetail },
        Insurance:{screen:Insurance},
        ListMember:{screen:ListMember},
        RatingView:{screen:RatingView},
        Appointment:{screen:Appointment},
        AppointmentDetail:{screen:AppointmentDetail},
        AppointmentResc:{screen:AppointmentResc},
        UploadPersonal:{screen: UploadPersonal},
        UploadDoctor:{screen: UploadDoctor},
        UploadLab:{screen:UploadLab},
        DoctorVisitDetail:{screen:DoctorVisitDetail},
        AskConfirmation:{screen:AskConfirmation},
        Emergency:{screen:Emergency},
        RatingPost:{screen:RatingPost},
        BookingAppointmentDetail:{screen:BookingAppointmentDetail},
        BookingDetailFinal:{screen:BookingDetailFinal},
        Confirmation:{screen:Confirmation},
        DoctorDetail:{screen:DoctorDetail},
        Labtest:{screen:Labtest},
        HospitalDetail:{screen:HospitalDetail},
        Location:{screen:Location},
        AmbulanceBooking:{screen:AmbulanceBooking},
        HospitalList:{screen:HospitalList},
        BookingAppointment:{screen:BookingAppointment},
        DoctorVisit:{screen:DoctorVisit},
        OpdHealth:{screen:OpdHealth},
        MyCameraPicker:{screen:MyCameraPicker},
        MyDocument: { screen: MyDocument },
        Speciality: { screen: Speciality },
        Thankyou: { screen: Thankyou },
        Pharmacy:{screen:Pharmacy},
        Upload:{screen:Upload},
        Chat:{screen:Chat},
        ArticleDescription:{screen:ArticleDescription},
        ViewVideo:{screen:ViewVideo}
    },

   // {headerMode :'none'},
);

export default createAppContainer(StackNavigator);
//LabourLaw