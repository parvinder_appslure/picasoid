import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    Modal,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import store from 'react-native-simple-store';
import Header from './Header.js';
const GLOBAL = require('./Global');
import { TextField } from 'react-native-material-textfield';
type Props = {};
import PopupPay from './PopupPay.js'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import RangeSlider from 'rn-range-slider';
import { Dialog, DialogContent, DialogComponent, DialogTitle } from 'react-native-dialog-component';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
var radio_props = [
  {label: 'Online', value: 0 },
  {label: 'Offline', value: 1 },
 ];

var arrayholder=[];

export default class BookingAppointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            recognized: '',
            started: '',
            low: "0",
            high: "500",
            term: false,
            text :GLOBAL.myStatefrom,
            depart: false,
            department :[],
            speciality :[],
            hospital:[],
            price:[],
            depa :'',
            rating:'',
            speciality : [],
            depaid:'',
            results: [],resultstates:[],
            openPay:false,
            visibleFilter:true
        };
    }

    componentWillUnmount() {

    }

    fetchSpeciality = (res,type,depart) => {
        var myArray = [];
        var speciality = '';
        if (res == null || res.length == 0) {
            this.fetchHospital(res,type,depart,'')
        } else {
            var array = res[0].array
            for (var i = 0; i < array.length; i++) {
                if (array[i].selected == "Y") {
                    speciality = speciality + array[i].id + ','
                    myArray.push(array[i])

                }
            }
            speciality = speciality.slice(0, -1);

            store.get('hospital')
                .then((res) =>
                    //  alert(JSON.stringify(res))
                    this.fetchHospital(res,type,depart,speciality)
                )

        }
        this.setState({speciality:myArray})

    }
    fetchHospital = (res,type,depart,speciality) =>{
        var myArray = [];
        var hospital = '';
        if (res == null || res.length == 0) {
            this.setState({hospital:[]})

        } else {
            var array = res[0].array
            for (var i = 0; i < array.length; i++) {
                if (array[i].selected == "Y") {
                    hospital = hospital + array[i].id + ','
                    myArray.push(array[i])
                }
            }
            this.setState({hospital: myArray})
            hospital = hospital.slice(0, -1);
        }

            const url =  GLOBAL.BASE_URL  + 'fetch_nearest_doctor'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },


                body: JSON.stringify({
                    "user_id":GLOBAL.user_id,
                    "lat":GLOBAL.lat,
                    "long":GLOBAL.long,
                    "doctor_condition":GLOBAL.doctor_condition,
                    "type":type,
                    "departments_filter":depart,
                    "hospital_filter":hospital,
                    "price_range_min":"",
                    "price_range_max":"",
                    "is_favrouite":"",
                    "specialty":speciality,

                }),
            }).then((response) => response.json())
                .then((responseJson) => {

                //    console.log(JSON.stringify(responseJson))
                    if (responseJson.status == true) {
                        // GLOBAL.pica_booking_charge_online = responseJson.booking_price_for_online
                        // GLOBAL.pica_booking_charge_offline = responseJson.booking_price_for_offline
                        GLOBAL.hDoctorBookingAmountOnline = responseJson.booking_price_for_online

                        this.setState({results:responseJson.doctor_list_s})
                        arrayholder = responseJson.doctor_list_s
                  }else{
                        this.setState({results:[]})
                        arrayholder=[]
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });


    }

    fetchDepartment = (res,type) => {
        var myarray = [];
        var depart = '';
        if (res == null || res.length == 0) {
            this.fetchSpeciality(res,type,'')
        } else {
            var array = res[0].array
            for (var i = 0; i < array.length; i++) {
                if (array[i].selected == "Y") {
                    depart = depart + array[i].id + ','
                    myarray.push(array[i])

                }
            }
            depart = depart.slice(0, -1);


            store.get('speciality')
                .then((res) =>
                    //  alert(JSON.stringify(res))
                    this.fetchSpeciality(res,type,depart)
                )
        }
        this.setState({department:myarray})


    }

    getApicall(type)
        {

            store.get('departments')
                .then((res) =>
                    //  alert(JSON.stringify(res))
                    this.fetchDepartment(res,type)
                )



    }

    _handlePressCancel = () =>{
  this.setState({high:'',})
    this.setState({low:''})
    this.setState({depaid:''})
    this.setState({depa:''})
    GLOBAL.depart = []
    this.setState({value:''})
    this.setState({term :false})
    this.setState({visible:false})
      this.dialogComponents.dismiss()

    setTimeout(() => {
             // write your functions
            this.getDoctor()
         },100)

}

_handlePress1 =() =>{
  if(GLOBAL.depart.length != 0){
    this.setState({depaid :GLOBAL.depart.id})
  }
  this.setState({visible:false})
  this.dialogComponents.dismiss()
  // setTimeout(() => {
  //          // write your functions
  //         this.getDoctor()
  //      },100)
}

getDoctor = ()=>{
  var a = ''

if (this.state.term == true){
  a = 'a to z'
}else{
  a = ''
}

//alert(this.state.depaid)
  const url = GLOBAL.BASE_URL +  'fetch_nearest_doctor'
 // this.showLoading()
  fetch(url, {
      method: 'POST',
      headers: {
          'Content-Type': 'application/json',
      },
      body: JSON.stringify({
          user_id: GLOBAL.user_id,
        "lat":GLOBAL.lat,
        "long":GLOBAL.long,
        "doctor_condition":GLOBAL.myonlinetype,
          type: a,
          departments_filter:this.state.depaid,
          specialty:  '',
          hospital_filter: '',
          price_range_min: this.state.low,
          price_range_max:this.state.high,
          is_favrouite: '',
          "state":GLOBAL.myStatefrom

      }),
  }).then((response) => response.json())
      .then((responseJson) => {

       // alert(JSON.stringify(responseJson))
        //  this.hideLoading()
          if (responseJson.status == true) {

            // GLOBAL.pica_booking_charge_online = responseJson.booking_price_for_online
            // GLOBAL.pica_booking_charge_offline = responseJson.booking_price_for_offline
            GLOBAL.hDoctorBookingAmountOnline = responseJson.booking_price_for_online

            this.setState({results:responseJson.doctor_list_s})
            this.arrayholder = responseJson.doctor_list_s



          }
          else{
              alert('No Doctor Found')
              this.setState({speciality:[]})
              this.arrayholder = []
          }
      })
      .catch((error) => {
          console.error(error);
      });
}




    setPrice = (low,high) =>{

      this.setState({low :low})
        this.setState({high :high})
    }

    depart = () =>{
      this.setState({visible:false})
      this.props.navigation.navigate('Department')
    }

    setModalVisible=(visible,get)=> {
        if (typeof get !== 'undefined') {
                   this.setState({text:get.state_name})

  //                  alert(JSON.stringify(get))

            this.callAgain(get)
        }

        this.setState({modalVisible: visible});
    }


    callAgain=(get)=>{
        console.log(get.state_name +'call again' + GLOBAL.myonlinetype)

        const url =  GLOBAL.BASE_URL  + 'fetch_nearest_doctor'

    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },


        body: JSON.stringify({
            "user_id":GLOBAL.user_id,
            "lat":GLOBAL.lat,
            "long":GLOBAL.long,
            "doctor_condition":GLOBAL.myonlinetype,
            "type":"",
            "departments_filter":"",
            "hospital_filter":"",
            "price_range_min":"",
            "price_range_max":"",
            "is_favrouite":"",
            "specialty":"",
            "state":get.state_name
        }),
    }).then((response) => response.json())
        .then((responseJson) => {



            if (responseJson.status == true) {

                // GLOBAL.pica_booking_charge_online = responseJson.booking_price_for_online
                // GLOBAL.pica_booking_charge_offline = responseJson.booking_price_for_offline
                GLOBAL.hDoctorBookingAmountOnline = responseJson.booking_price_for_online
                
                this.setState({results:responseJson.doctor_list_s})
                arrayholder = responseJson.doctor_list_s
            }else{
                alert('No doctors found!')
                this.setState({results:[]})
                arrayholder =[]
            }
        })
        .catch((error) => {
            console.error(error);
            this.hideLoading()
        });

    }


    static navigationOptions = ({ navigation }) => {
        return {
               header: () => null,
        }
    }



    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }

    _handleStateChange = (state) => {
        console.log(GLOBAL.user_id + '--'+ GLOBAL.myStatefrom +'--' +GLOBAL.myonlinetype +'--'+GLOBAL.lat+'---'+GLOBAL.long)
      if (GLOBAL.depart.length != 0){
        this.setState({depa:GLOBAL.depart.name,
            visible:true
        })
//        this.setState({depaid:GLOBAL.depart.id})
      }else{
          this.setState({depa:''})
      }

      // alert(JSON.stringify(GLOBAL.depart))
     // this.setState({text: GLOBAL.myStatefrom})
    if (GLOBAL.appply == 1){
        this.getApicall('')
    }else {
        this.setState({department:[],
            speciality:[],
            hospital:[]
        })
        // alert(GLOBAL.did)
    const url =  GLOBAL.BASE_URL  + 'fetch_nearest_doctor'

    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },


        body: JSON.stringify({
            "user_id":GLOBAL.user_id,
            "lat":GLOBAL.lat,
            "long":GLOBAL.long,
            "doctor_condition":GLOBAL.myonlinetype,
            "type":"",
            "departments_filter":GLOBAL.did,
            "hospital_filter":"",
            "price_range_min":"",
            "price_range_max":"",
            "is_favrouite":"",
            "specialty":"",
            "state":this.state.text
        }),
    }).then((response) => response.json())
        .then((responseJson) => {

                    // console.log(JSON.stringify(responseJson))


            if (responseJson.status == true) {

                // GLOBAL.pica_booking_charge_online = responseJson.booking_price_for_online
                // GLOBAL.pica_booking_charge_offline = responseJson.booking_price_for_offline
                GLOBAL.hDoctorBookingAmountOnline = responseJson.booking_price_for_online

                this.setState({results:responseJson.doctor_list_s})
//                alert(GLOBAL.hDoctorBookingAmountOnline)
                arrayholder = responseJson.doctor_list_s
            }else{
                alert('No doctors found!')
                this.setState({results:[]})
                arrayholder =[]
            }
        })
        .catch((error) => {
            console.error(error);
            this.hideLoading()
        });

}


    this.getStatesList()
    }

    getStatesList=()=>{
           const url =  GLOBAL.BASE_URL  + 'state_list'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },


                body: JSON.stringify({
                        "key":"state"
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
//                    alert(JSON.stringify(responseJson.list))
                    if (responseJson.status == true) {
//                        console.log('yes')
//                        var rece = responseJson.list
                        // const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
                        // console.log(transformed)
                         this.setState({resultstates:responseJson.list})
//                        arrayholder = responseJson.list

                    }else{
                        this.setState({resultstates:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });


    }

    componentDidMount(){
        this.props.navigation.addListener('willFocus',this._handleStateChange);


    }


    login = (s,item) => {
//    console.log(JSON.stringify(item))

        // condition for interstate booking

//     if(GLOBAL.myStatefrom == item.doctor_state){

//         GLOBAL.appointmentArray = item
//         GLOBAL.speciality = s

//         this.props.navigation.navigate('OnlineBooking')

//     }else if(GLOBAL.myPaymentStatus ==1){
// //        alert('Your interstate account is activated')
//         GLOBAL.appointmentArray = item
//         GLOBAL.speciality = s
//         this.props.navigation.navigate('OnlineBooking')
//     }else{
//             this.setState({openPay: true})

//     }

        GLOBAL.appointmentArray = item
        GLOBAL.speciality = s

        GLOBAL.pica_booking_charge_online = item.booking_charge_for_online
        GLOBAL.pica_booking_charge_offline = item.booking_charge_for_offline
                
        this.props.navigation.navigate('OnlineBooking')

    }


    selectedFirst = (item,speciality) => {
        GLOBAL.speciality = speciality
        GLOBAL.appointmentArray = item
        this.props.navigation.navigate('DoctorDetail')

    }

    _renderDepartmentss =  ({item,index}) => {
        return (
            <View style = {{backgroundColor:'white',borderRadius:12 ,margin :2}}>

                <Text style = {{color:'black',fontFamily:'Konnect-Regular',margin:4,fontSize:10}}>
                    {item.name}

                </Text>


            </View>




        )


    }
    _renderDepartments =  ({item,index}) => {
        return (
            <View style = {{backgroundColor:'white',borderRadius:12 ,margin :2}}>

                <Text style = {{color:'black',fontFamily:'Konnect-Regular',margin:4,fontSize:10}}>
                    {item.name}

                </Text>


            </View>




        )


    }


    _renderItemsstates=({item,index})=>{
        return(
    <TouchableOpacity onPress={()=>this.setModalVisible(!this.state.modalVisible,item)}>
           <Text style={{fontSize: 16, color:'black', fontFamily: 'Konnect-Regular'}}>{item.state_name}</Text>
               <View style = {{backgroundColor:'#e1e1e1',width:'100%',height:1,marginTop: 10,marginBottom:10}}>
               </View>
     </TouchableOpacity>
                                     
            )
    }
    _renderDepartment =  ({item,index}) => {
        return (
            <View style = {{backgroundColor:'white',borderRadius:12 ,margin :2}}>

                <Text style = {{color:'black',fontFamily:'Konnect-Regular',margin:4,fontSize:10}}>
                    {item.name}

                </Text>


            </View>




            )


    }

    _renderItems = ({item,index}) => {
        var speciality = item.speciality_detail_array
      //  console.log(JSON.stringify(item))

        return (

            <TouchableOpacity onPress={() => this.selectedFirst(item,speciality)
            }>
                <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>

            <View style = {{flexDirection:'row',width :'100%'}}>

                <View>


                    <Image style = {{width :60 ,height :60,borderRadius: 30,margin:10}}
                           source={{ uri: item.image }}/>
        <View style = {{backgroundColor:'#800000',borderRadius:4,width:40,height:20,marginTop:2,flexDirection:'row',justifyItems:'center',alignItems:'center', alignSelf:'center'}}>
            <Image style = {{width :8 ,height :8,marginLeft:6,resizeMode:'contain',marginLeft:5}}
                   source={require('./star.png')}/>

            <Text style={{marginLeft : 5,fontSize : 10,color :'white',fontFamily:'Konnect-Medium',marginTop:-0.2}}>

                {item.ratting}
            </Text>
        </View>
        {item.doctor_avail_status == 1 && (

            <Text style={{marginTop:5,fontSize : 11,color :'#3DBA56',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                Online
            </Text>
        )}
        {item.doctor_avail_status != 1 && (

            <Text style={{marginTop:5,fontSize : 11,color :'red',fontFamily:'Konnect-Medium',width:50,textAlign:'center', alignSelf:'center'}}>

                Offline
            </Text>
        )}

    </View>

                           <View>

                    <View style = {{flexDirection:'row',width:'100%'}}>
                                    <Text style={{marginLeft : 5,fontSize : 15,color :'black',fontFamily:'Konnect-Medium',width :'80%',marginTop:18}}>
                                    {item.name}
                                    </Text>
                    </View>

                               <View style = {{flexDirection:'row'}}>
                               <Text style={{marginLeft : 5,fontSize : 12,color :'grey',height:'auto',fontFamily:'Konnect-Medium',width :window.width -100}}>

                                   {speciality}
                               </Text>
                               </View>

                               <View style = {{flexDirection:'row'}}>
                                   <Image style = {{width :20 ,height :20,resizeMode:'contain'}}
                                          source={require('./location.png')}/>

                                   <Text style={{marginLeft : 5,width:window.width - 150,height:'auto',fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',}}>

                                       Hospital/Clinic: {item.lat_long_address}
                                   </Text>

                               </View>

                               <View style = {{flexDirection:'row',justifyContent:'space-between'}}>

                                   <View>
                                       <Text style={{fontSize : 12,color :'black',fontFamily:'Konnect-Medium',}}>

                                           Experience
                                       </Text>
                                       <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                           {item.experience} Years
                                       </Text>
                                   </View>

                                   <View>
                                       <Text style={{fontSize : 12,color :'black',fontFamily:'Konnect-Medium',}}>

                                           Likes
                                       </Text>
                                       <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                           {item.like}
                                       </Text>
                                   </View>

                                   <View style = {{marginRight:50}}>
                                       <Text style={{fontSize : 12,color :'black',fontFamily:'Konnect-Medium',}}>

                                           Reviews
                                       </Text>
                                       <Text style={{fontSize : 16,color :'#3A3A3A',fontFamily:'Konnect-Medium',textAlign:'center'}}>

                                           {item.total_review}
                                       </Text>
                                   </View>

                               </View>
                     {item.online_consult!='3' &&  item.online_consult =='1' &&(
                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>

                                       Consult online for ₹ {item.online_consult_chat_price}/- onwards
                                   </Text>
                        )}
                     {item.online_consult!='3' &&  item.online_consult =='2' &&(
                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>

                                       Consult online for ₹ {item.online_consult_video_price}/- onwards
                                   </Text>
                        )}
                     {item.online_consult=='3' && (
                        
                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>

                                       Consult online for ₹ {item.online_consult_video_price}/- onwards
                                   </Text>

                        )}

                                   <Text style={{fontSize : 12,color :'#800000',fontFamily:'Konnect-Medium',}}>
                                    Booking Charge is ₹ {item.booking_charge_for_online}/- only
                                   </Text>

            </View>
        </View>
                    <Button
                        style={{padding:7,marginTop:14,fontSize: 15, color: 'white',backgroundColor:'#800000',marginLeft:'55%',width:'20%',height:34,fontFamily:'Konnect-Medium',borderRadius:12,marginBottom: 20}}
                        styleDisabled={{color: 'red'}}
                        onPress={() => this.login(speciality,item)}>
                        Consult
                    </Button>

                </View>
            </TouchableOpacity>
        )
    }


     SearchFilterFunction(text){
  const newData = arrayholder.filter(function(item){
         const itemData = item.name.toUpperCase()
         const textData = text.toUpperCase()
         return itemData.indexOf(textData) > -1
     })
     this.setState({
         results: newData,
         texts: text


     })

 }

    render() {


        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}
                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

                <View style={styles.container}>
            <Header navigation={this.props.navigation}
                headerName={'ONLINE APPOINTMENT'}/>
                    <View style = {{margin :10,width:window.width - 20 ,height:45,borderRadius:20,flexDirection:'row',backgroundColor:'white',}}>

                        <Image style = {{width :18 ,height: 18,alignSelf:'center',resizeMode: 'contain',marginLeft:13}}
                               source={require('./search.png')}/>

                        <TextInput style={{marginLeft:10 ,width:window.width - 100, height:45}}
                                   placeholderTextColor='rgba(0, 0, 0, 0.4)'
                                   onChangeText={(text) => this.SearchFilterFunction(text)}
                                   placeholder={"Search"}/>



{/*                        <Image style = {{width :18 ,height: 18,alignSelf:'center',resizeMode: 'contain',marginLeft:13}}
                               source={require('./speech.png')}/>
*/}

                    </View>

                    <View style = {{flexDirection:'row',marginTop: 6,marginLeft:12,width:'100%'}}>
                        <View style = {{flexDirection:'row',width:'70%'}}>

                        <Image style = {{width :20 ,height: 20,alignSelf:'center',resizeMode: 'contain',marginLeft:10, }}
                               source={require('./location.png')}/>

                        <Text style = {{color:'black',fontFamily:'Konnect-Medium',fontSize:16,marginTop:4, marginLeft:5}}>
                            {this.state.text}
                        </Text>
                            <TouchableOpacity onPress={()=>this.setModalVisible(true)}>

                        <Image style = {{width :14 ,height: 14,alignSelf:'center',resizeMode: 'contain',marginLeft:10, marginTop:4}}
                               source={require('./drop.png')}/>
                            </TouchableOpacity>
                    </View>

                        <TouchableOpacity style = {{width:'35%'}}
                                              onPress={()=>this.dialogComponents.show()}>

                    <View style = {{flexDirection:'row',width:'30%'}}>
                        <Image style = {{width :30 ,height: 28,alignSelf:'center',resizeMode: 'contain',marginLeft:10}}
                               source={require('./filter.png')}/>


                    <Text style = {{color:'#223B75',fontFamily:'Konnect-Medium',fontSize:16,marginTop:2}}>
                        Filter
                    </Text>

                </View>
                        </TouchableOpacity>

                    </View>

                    <FlatList style= {{flexGrow:0,margin:1}}
                              data={this.state.department}
                              horizontal={true}
                              keyExtractor = { (item, index) => index.toString() }
                              renderItem={this._renderDepartment}
                    />

                    <FlatList style= {{flexGrow:0,margin:1}}
                              data={this.state.speciality}
                              horizontal={true}
                              keyExtractor = { (item, index) => index.toString() }
                              renderItem={this._renderDepartments}
                    />
                    <FlatList style= {{flexGrow:0,margin:1}}
                              data={this.state.hospital}
                              horizontal={true}
                              keyExtractor = { (item, index) => index.toString() }
                              renderItem={this._renderDepartmentss}
                    />




                    <FlatList style= {{flexGrow:0,margin:8,height:window.height - 160}}
                              data={this.state.results}
                              numColumns={1}
                              keyExtractor = { (item, index) => index.toString() }
                              renderItem={this._renderItems}
                    />


                    {this.state.openPay ==true && (

                    <PopupPay navigation={this.props.navigation}
                    setMode={this.state.openPay}/>

                        )}



                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
//             Alert.alert('Modal has been closed.');
                            this.setModalVisible(!this.state.modalVisible)
                        }}>
                         <TouchableOpacity style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            alignItems: 'center', borderRadius:8}}
                            activeOpacity={1}
                            onPressOut={() => {this.setModalVisible(false)}}
                            >
                            <View style={{width: 300,backgroundColor: 'white',height: 240, borderRadius:8}}>
                              <View style={{width: '100%',  backgroundColor:'white', borderRadius:8}}>

                              <View style={{flexDirection:'row', width:'100%', backgroundColor:'#800000', height:60,
                               borderTopLeftRadius:8, borderTopRightRadius:8, borderTopLeftWidth:1,justifyContent:'space-between',alignItems:'center',
                               borderTopRightWidth:1, borderTopRightColor:'transparent', borderTopLeftColor:'transparent'}}>
                              <Text style={{fontSize: 17, color:'white', fontFamily: 'Konnect-Regular',margin:10 }}>Select State</Text>
                              <TouchableOpacity onPress={()=> this.setModalVisible(false)}>
                              <Image style={{width:25, height:25, resizeMode:'contain', marginRight:10}} source={require('./cross.png')}/>
                              </TouchableOpacity>

                                </View>
                                <FlatList style= {{flexGrow:0,margin:8,marginBottom:50}}
                                          data={this.state.resultstates}
                                          numColumns={1}
                                          keyExtractor = { (item, index) => index.toString() }
                                          renderItem={this._renderItemsstates}
                                />

                                </View>

                        </View>
                        </TouchableOpacity>
                    </Modal>


                <DialogComponent
                    dialogStyle = {{backgroundColor:'transparent'}}
                    dismissOnTouchOutside={true}
                    dismissOnHardwareBackPress={true}
                    ref={(dialogComponents) => { this.dialogComponents = dialogComponents; }}>
                    <View style = {{width :window.width - 30 ,height :430,borderRadius:12, backgroundColor:'white', alignSelf:'center', padding:10}}>



                           <Text style={{fontFamily:'Konnect-Medium',fontSize:22,marginTop:3,color:'#8E9198',textAlign:'center'}}>
                             Filter

                           </Text>
                           <View style = {{flexDirection:'row',marginTop:10,justifyContent:'space-between'}}>
                           <Text style={{fontFamily:'Konnect-Medium',fontSize:18,marginTop:3}}>
                             PRICE RANGE

                           </Text>
                           <Text style={{fontFamily:'Konnect-Medium',fontSize:16,marginTop:3}}>
                          ₹ {this.state.low} - ₹ {this.state.high}

                           </Text>

                           </View>

                           <RangeSlider
                               style={{width: '100%', height: 80}}
                               gravity={'center'}
                               min={0}
                               max={500}
                               step={20}
                               selectionColor="#800000"
                               blankColor="#8E9198"
                               onValueChanged={(low, high, fromUser) => {
                               this.setPrice(low,high)}}

                           />


                           <View style = {{width:'95%',height:1,backgroundColor:'#f1f2f6',marginTop:10,marginLeft:10}}>

                           </View>

                           <Text style={{fontFamily:'Konnect-Medium',fontSize:18,marginTop:20}}>
                             SELECT DEPARTMENT

                           </Text>

                             <TouchableOpacity onPress= {()=>this.depart()}>
                            <View style = {{width:'100%' ,borderRadius:4,borderWidth:1,borderColor:'#8E9198',height:40,flexDirection:'row', marginTop:10}}>
                            <TextInput
                                 style={{ height: 40,width:'90%',marginLeft:5,fontFamily:'Konnect-Regular',fontSize:18, marginTop:1}}
                                 placeholder = 'Select Department'
                                 editable = {false}
                                 value = {this.state.depa}

                               />
                               <Image   source={require('./arrowlogo.png')}
                                        style  = {{width:18, height:18,resizeMode:'contain',marginRight:0,marginTop:9,
                                        }}

                               />

                            </View>
                            </TouchableOpacity>

                           <View style = {{width:'95%',height:1,backgroundColor:'#f1f2f6',marginTop:20,marginLeft:10}}>

                           </View>

                           <TouchableOpacity onPress= {()=>this.setState({term :!this.state.term})}>
                            <View style = {{flexDirection:'row',width:'100%',marginTop:20}}>

                            <Text style={{fontFamily:'Konnect-Medium',fontSize:18,marginTop:8,width:'85%'}}>
                              VIEW ALPHABETICALLY

                            </Text>
                            {this.state.term == false && (
                              <Image
                                  source={require('./drop.png')}
                                  style={{width: 15, height: 15,marginLeft:28,marginTop:9,resizeMode:'contain'}}


                              />
                            )}

                            {this.state.term == true && (
                              <Image
                                  source={require('./close.png')}
                                  style={{width: 15, height: 15,marginLeft:28,marginTop:9,resizeMode:'contain'}}


                              />
                            )}

                            </View>
                            </TouchableOpacity>

                           <View style = {{width:'95%',height:1,backgroundColor:'#f1f2f6',marginTop:20,marginLeft:10}}>

                           </View>


                            <View style = {{flexDirection:'row',justifyContent:'space-between',marginTop:20}}>
                            <Button
                                style={{marginLeft:8,paddingTop: 10 ,fontSize: 14,backgroundColor:'#800000',marginRight:40 ,color: 'white',fontFamily:'Konnect-Medium',marginTop:6,height:40,width:120,borderRadius:4}}
                                styleDisabled={{color: 'red'}}
                                onPress={() => this._handlePressCancel()}>
                              RESET
                            </Button>
                            <Button
                                style={{marginLeft:4,paddingTop: 10 ,fontSize: 14,backgroundColor:'#800000', color: 'white',fontFamily:'Konnect-Medium',marginTop:6,height:40,width:120,borderRadius:4}}
                                styleDisabled={{color: 'red'}}
                                onPress={() => this._handlePress1()}>
                              APPLY
                            </Button>



                            </View>
                    </View>



                     </DialogComponent>


                </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor :'#f1f1f1',
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,
        top: window.height/2,
        opacity: 0.5,
        justifyContent: 'center',
        alignItems: 'center'
    },
})
