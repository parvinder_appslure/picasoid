import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
    TouchableOpacity,
    TextInput,
    Image,
    ImageBackground,
    Linking,
    FlatList,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import Header from './Header.js';
import HTML from 'react-native-render-html';
import React, {Component} from 'react';
import Button from 'react-native-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');
import Autolink from 'react-native-autolink';


class ArticleDescription extends React.Component {
    state = {
        name :'',
        email:'',
        phone :'',
        company :'',
        loading:false,results:[],
        visible:false,a_details:GLOBAL.aid
    };


    static navigationOptions = ({ navigation }) => {
        return {
               header: () => null,
        }
    }

    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }


    componentDidMount(){


    }

    confirmCancel=()=>{
        this.showLoading()
        const url = GLOBAL.BASE_URL + 'cancel_appointment'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "booking_id": GLOBAL.appointment_details.booking_id,


            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                this.hideLoading()
//                alert(JSON.stringify(responseJson))


                if (responseJson.status == true) {
                    alert('Appointment cancelled successfully!')
//                    this.loadAppointments()


                } else {
                    alert('Something went wrong!')
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

    onPressCancel=(item, index)=>{
        Alert.alert(
            'Cancel Appointment',
            'Are you sure you want to cancel this appointment?',
            [
                {
                    text: 'Cancel',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                },
                {
                    text: 'Confirm',
                    onPress: () => this.confirmCancel(item,index)
                }
            ],
            {
                cancelable: false
            }
        );
        return true;
    }


    getDirections=()=>{
        var lat= GLOBAL.appointment_details.doctor_lat
        var lot= GLOBAL.appointment_details.doctor_long

        var url = `https://www.google.com/maps?saddr=My+Location&daddr=`+lat+','+lot;
//    alert(url)
        Linking.openURL(url);


    }

    clickResc=()=>{

        this.props.navigation.navigate('AppointmentResc')
    }

    clickRescs=()=> {
        GLOBAL.bookingid = "Booking Id:" + this.state.a_details.booking_id

        this.props.navigation.navigate("VideoCall", {
            channelName: 'Picasoid',
            onCancel: (message) => {
                this.setState({
                    visible: true,
                    message
                });

            }
        })
    }
    render() {
        var yeah = this.state.a_details
        if(this.state.loading){
            return(
                <View style={{
                    flex:1,
                    backgroundColor :'#f1f1f1'

                }}>
                    <ActivityIndicator style = {{
                        position: 'absolute',
                        left: window.width/2 - 30,

                        top: window.height/2,

                        opacity: 0.5,

                        justifyContent: 'center',
                        alignItems: 'center'
                    }}

                   size="large" color='#800000' />
                </View>
            )
        }

        return(
                <View style={{flex:1}}>
                <Header navigation={this.props.navigation}
                headerName={'ARTICLES & ANNOUNCEMENTS'}/>
                <ScrollView >

                <Image style={{width:'100%', height:200, resizeMode:'cover'}}
                source={{uri: yeah.image}}/>
                <Text style = {{fontSize:20,margin:10,fontFamily:'Konnect-Medium',color:'#800000',textAlign:'left',width:'95%',}}>
                        {yeah.title}
                    </Text>
                    {yeah.description == '' && (
                    <Text style = {{fontSize:16,marginLeft:10,marginRight:10,marginBottom:10,fontFamily:'Konnect-Regular',color:'black',textAlign:'left',width:'90%'}}>
                    No description added for this article!

                    </Text>

                    )}

                    {yeah.description!='' &&(

                    <Autolink
                    style={{fontSize:16,marginLeft:10,marginRight:10,marginBottom:10,fontFamily:'Konnect-Regular',color:'black',textAlign:'left',width:'90%'}}
                    text={yeah.description}
                    linkStyle={{color:'#0000EE'}}
                    hashtag="instagram"
                    mention="twitter" />                    


                    )}

                    </ScrollView>
                </View>
        );
    }
}

export default ArticleDescription;