import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    SafeAreaView,
    AsyncStorage
} from 'react-native';
const window = Dimensions.get('window');
import Button from 'react-native-button';
import Header from './Header.js';
import { TextField } from 'react-native-material-textfield';
type Props = {};
const GLOBAL = require('./Global');
import { Dropdown } from 'react-native-material-dropdown';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

export default class Support extends Component {
    state = {
        name :GLOBAL.myname,
        email:GLOBAL.myemail,
        message :'',hosp:'',
        company :'',phone:GLOBAL.mymobile,resultstates:[],rstate:'',atleast:0,atleasth:0,
        resultHospitals:[], rhospital:'',rhospitalid:'',
        loading:false,picaid:GLOBAL.pica_id,
        visible:false,helpline_email:'',helpline_no:''
    };


        static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
        }
    }


    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }




    componentDidMount(){
        this.getSupport()
    }

    getSupport=()=>{
            const url =  GLOBAL.BASE_URL  + 'state_list'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },


                body: JSON.stringify({
                        "key":"state"
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
//                    alert(JSON.stringify(responseJson.list))
                    if (responseJson.status == true) {
//                        console.log('yes')
                        var rece = responseJson.list
                        const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
                        console.log(transformed)
                        this.setState({resultstates:transformed})
//                        arrayholder = responseJson.list

                    }else{
                        this.setState({resultstates:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });


    }


    getHospitals=(getState)=>{
            const url =  GLOBAL.BASE_URL  + 'hospital_treatment'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                        "user_id": GLOBAL.user_id,
                        "state": getState
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
//                    alert(JSON.stringify(responseJson.list))
                    if (responseJson.status == true) {

                        var rece = responseJson.hospital_list
                        const transformed = rece.map(({ id, name }) => ({ label: name, value: id }));
                        console.log(transformed)
                        this.setState({resultHospitals:transformed})
//                        arrayholder = responseJson.list

                    }else{
                        this.setState({resultHospitals:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });


    }

    getIndexs = (index) => {
        this.setState({atleast:1})

        this.setState({rstate:this.state.resultstates[index].label})
        this.setState({resultHospitals: [], rhospital:'', rhospitalid:'', atleasth:0})

        this.getHospitals(this.state.resultstates[index].label)
        // alert(this.state.resultstates[index].label)
//        alert(this.state.resultstates[index].value)
    }

    getIndexh = (index) => {
        this.setState({atleasth:1})

        this.setState({rhospital: this.state.resultHospitals[index].label,
          rhospitalid: this.state.resultHospitals[index].value
        })
//        alert(this.state.rstate)
//        alert(this.state.resultstates[index].value)
    }


    submitEnq=()=>{
        if(this.state.name == ''){
            alert('Please enter name')
        }else if(this.state.email==''){
            alert('Please enter email')
        }else if(this.state.phone==''){
            alert('Please enter mobile number')
        }else if(this.state.atleast ==0){
            alert('Please select preferred State')
        }else if(this.state.atleasth==0){
            alert('Please select preferred hospital')
        }else if(this.state.message ==''){
            alert('Please enter treatment name')
        }else{

          var sendBody= {
                "patient_id": GLOBAL.user_id,
                "name": this.state.name,
                "email": this.state.email,
                "message": this.state.message,
                "picsoidid":GLOBAL.pica_id,
                "state":this.state.rstate,
                "hospital":this.state.rhospital,
                "hospital_id":this.state.rhospitalid,
                "mobile":this.state.phone
                 
            }

           console.log(JSON.stringify(sendBody)) 
        const url = GLOBAL.BASE_URL +  'patient_support'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify(sendBody),
        }).then((response) => response.json())
            .then((responseJson) => {

                if (responseJson.status == true) {
                    this.props.navigation.goBack()
                    alert('Enquiry submitted successfully.')
                }else{
                    alert('Something went wrong!')
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

        }

    }


    render() {


        let { phone } = this.state;
        let { email } = this.state;
        let { name } = this.state;
        let { company } = this.state;
        let { picaid } = this.state;
        let { hosp } = this.state;
        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}

                                       size="large" color='#800000' />
                </View>
            )
        }
        return (
        <View style={{flex:1}}>
          <Header navigation={this.props.navigation}
                headerName={'TREATMENT ENQUIRY'}/>

  <KeyboardAwareScrollView keyboardShouldPersistTaps='handled'>

    <View style={{width : Dimensions.get('window').width,backgroundColor:'#F2F5F7'}}>

    <View style={{backgroundColor:'white',color :'white',flexDirection:'column',flex:1 ,marginTop:15,marginBottom:70,marginLeft:15,marginRight:15, height:'auto',borderRadius :6,width : Dimensions.get('window').width-30, shadowColor: '#D3D3D3',
    shadowOffset: { width: 0, height: 1 },shadowOpacity: 0.6,shadowRadius: 2}}>


    <View style={{flexDirection:'row',marginTop:40,marginLeft:20,alignItems:'center'}}>

    <Text style={{fontSize:15,color:'black',fontFamily:'Konnect-Medium'}}>LEAVE US A MESSAGE</Text>
  </View>

  <View style={{flexDirection:'column',marginTop:40,marginLeft:20}}>
  <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey'}}>FULL NAME</Text>

   <TextInput style={{ fontSize: 16, borderBottomWidth: 1,width:325, borderBottomColor:'lightgrey',fontFamily:'Konnect-Regular'}}
        placeholder=""
       returnKeyType='go'
       value={this.state.name}
        onChangeText={(text)=> this.setState({name : text})}
        secureTextEntry={false}
        autoCorrect={false}
     />


     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey',marginTop:20}}>EMAIL</Text>

      <TextInput style={{ fontSize: 16, borderBottomWidth: 1,width:325, borderBottomColor:'lightgrey',fontFamily:'Konnect-Regular'}}
           placeholder=""
          returnKeyType='go'
          value= {this.state.email}
           onChangeText={(text)=> this.setState({email : text})}
           secureTextEntry={false}
           autoCorrect={false}
        />


     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey',marginTop:20}}>PiCaSoid ID</Text>

      <TextInput style={{ fontSize: 16, borderBottomWidth: 1,width:325, borderBottomColor:'lightgrey',fontFamily:'Konnect-Regular'}}
           placeholder=""
          returnKeyType='go'
          editable={false}
          value={this.state.picaid}
           onChangeText={(text)=> this.setState({picaid : text})}
           autoCorrect={false}
        />


     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey',marginTop:20}}>MOBILE</Text>

      <TextInput style={{ fontSize: 16, borderBottomWidth: 1,width:325, borderBottomColor:'lightgrey',fontFamily:'Konnect-Regular'}}
           placeholder=""
          returnKeyType='go'
          maxLength={10}
          value={this.state.phone}
          keyboardType={'numeric'}
           onChangeText={(text)=> this.setState({phone : text})}
           autoCorrect={false}
        />

     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey',marginTop:20}}>PREFERRED STATE</Text>

        <Dropdown containerStyle={{width:'90%', height:50, marginTop:-10}}
                  fontSize={16}
                  labelFontSize={13}
                  dropdownPosition = {-4.2}
                  onChangeText ={ (value,index) => this.getIndexs(index) }
                  label={''}
                  data={this.state.resultstates}
                />


     <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey',marginTop:30}}>PREFERRED HOSPITAL</Text>

        <Dropdown containerStyle={{width:'90%', height:50, marginTop:-10}}
                  fontSize={16}
                  labelFontSize={13}
                  dropdownPosition = {0}
                  onChangeText ={ (value,index) => this.getIndexh(index) }
                  label={''}
                  value={this.state.rhospital}
                  data={this.state.resultHospitals}
                />


        <Text style={{fontSize:16,fontFamily:'Konnect-Regular',color:'lightgrey',marginTop:20}}>TREATMENT NAME</Text>

         <TextInput style={{ fontSize: 16, borderBottomWidth: 1,width:325, borderBottomColor:'lightgrey',fontFamily:'Konnect-Regular'}}
              placeholder=""
             returnKeyType='go'

              onChangeText={(text)=> this.setState({message : text})}
              secureTextEntry={false}
              autoCorrect={false}
           />
     </View>




        <TouchableOpacity style={{marginTop:40,alignSelf:'center', marginBottom:30}}
        onPress={() => this.submitEnq()}>
        <View style = {{backgroundColor:'#800000',height:55,borderRadius:27.5,alignSelf:'center',width:300,
            borderBottomWidth: 0,
            shadowColor: 'black',
            shadowOffset: { width: 0, height: 2 },
            shadowOpacity: 0.8,
            shadowRadius: 2,
            flexDirection:'row'}}>


            <Text style= {{width:'100%',alignSelf:'center',textAlign:'center',fontSize:20,fontFamily:'Konnect-Medium',color:'white',padding:11}} >
                SUBMIT
            </Text>

            <Image style = {{width :25 ,height: 25,resizeMode: 'contain',marginLeft:-50,alignSelf:'center'}}
                   source={require('./right.png')}/>
        </View>
        </TouchableOpacity>

    </View>

    </View>


  </KeyboardAwareScrollView>
      </View>

        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    container: {

        backgroundColor :'#f1f1f1',
        height: window.height,
    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },

    text: {
        color: '#fff',
        fontSize: 30,
        fontWeight: 'bold',
    },

})