import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
    Modal
} from 'react-native';
import Button from 'react-native-button';
const window = Dimensions.get('window');
import Header from './Header.js';
const GLOBAL = require('./Global');
import { TextField } from 'react-native-material-textfield';
type Props = {};

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class HospitalList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            recognized: '',
            started: '',data:[],
            results: [],
            images: [],
            resultstates:[],
            texts :GLOBAL.myStatefrom,            
            modalVisible:false,
            selectdState : GLOBAL.myStatefrom

        };

    }
    static navigationOptions = ({ navigation }) => {
        return {
              header: () => null,
        }
    }


    _renderItemsstates=({item,index})=>{
        return(
    <TouchableOpacity onPress={()=>this.setModalVisible(!this.state.modalVisible,item)}>
           <Text style={{fontSize: 16, color:'black', fontFamily: 'Konnect-Regular'}}>{item.state_name}</Text>
               <View style = {{backgroundColor:'#e1e1e1',width:'100%',height:1,marginTop: 10,marginBottom:10}}>
               </View>
     </TouchableOpacity>
                                     
            )
    }


    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }

    callAgain=(get)=>{
//        alert(GLOBAL.typesHospital +'---'+ get.state_name)

        const url = GLOBAL.BASE_URL +  'hospital'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                latitude : GLOBAL.lat,
                longitude : GLOBAL.long,
                category_name : GLOBAL.typesHospital,
                user_id: GLOBAL.user_id,
                state: get.state_name 
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
              console.log(JSON.stringify(responseJson.hospital_list))
                if (responseJson.status == true) {
                    this.setState({texts: get.state_name})
                    this.setState({data:responseJson.catgeory})
                    this.setState({results: responseJson.hospital_list})


                }else{
                    alert('No data found!')
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });
    }



    setModalVisible=(visible,get)=> {
        if (typeof get !== 'undefined') {
                   this.setState({text:get.state_name})

                   // alert(JSON.stringify(get))
                 this.setState({selectdState : get.state_name})
            this.callAgain(get)
        }

        this.setState({modalVisible: visible});
    }


    selectedFirst = (item) =>{
        GLOBAL.hids = item.id
        this.props.navigation.navigate('HospitalDetail')
    }
    componentDidMount(){
        this.getData('All')
        this.getStatesList()

    }

     getStatesList=()=>{
           const url =  GLOBAL.BASE_URL  + 'state_list'

            fetch(url, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                        "key":"state"
                }),
            }).then((response) => response.json())
                .then((responseJson) => {
//                    alert(JSON.stringify(responseJson.list))
                    if (responseJson.status == true) {
//                        console.log('yes')
//                        var rece = responseJson.list
                        // const transformed = rece.map(({ id, state_name }) => ({ label: state_name, value: id }));
                        // console.log(transformed)
                         this.setState({resultstates:responseJson.list})
//                        arrayholder = responseJson.list

                    }else{
                        this.setState({resultstates:[]})
                    }
                })
                .catch((error) => {
                    console.error(error);
                    this.hideLoading()
                });
    }


    getData=(types)=>{
//    alert(types)
//            this.showLoading()
        GLOBAL.typesHospital = types
        const url = GLOBAL.BASE_URL +  'hospital'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                latitude : GLOBAL.lat,
                longitude : GLOBAL.long,
                category_name : types,
                user_id: GLOBAL.user_id,
                state: this.state.selectdState 
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log('-----------------')
                console.log(JSON.stringify(responseJson))


                if (responseJson.status == true) {
                    this.setState({data:responseJson.catgeory})
                    this.setState({results: responseJson.hospital_list})


                }else{
                    alert('Something went wrong!')
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

    _handleCategorySelect = (item,index) => {
      var ids = item.id
        let { data } = this.state;
        for(let i = 0; i < data.length; i++){
            data[i].selected = "";
        }


        let targetPost = data[index];
        var catid =  data[index].id
      //  this.getMoviesFromApiAsync()
        // Flip the 'liked' property of the targetPost
        targetPost.selected = 1;

        data[index] = targetPost;

        // Then update targetPost in 'posts'
        // You probably don't need the following line.
        // posts[index] = targetPost;

        // Then reset the 'state.posts' property

        this.setState({ data: data})
//        alert(item.name)
//        alert(data[index].id)
//        this.setState({selectdState : })
        this.getData(item.name)

    }


    _renderItemCateg = (item,index)=>{


        return (
            <TouchableOpacity
                onPress={() => this._handleCategorySelect(item.item,item.index)}
                activeOpacity={0.9}>

                {item.item.selected == 1 && (
                    <View style = {{margin :10 ,height :50,backgroundColor:'black',padding:5,alignSelf: 'center',
                        borderColor:'black',
                        borderRadius:10, justifyContent:'center'}}>

                        <Text style = {{fontSize: 14,color:'white',alignSelf: 'center',paddingLeft:15, paddingRight:15,fontFamily:'Konnect-Medium',}}>
                            {item.item.name}
                        </Text>

                    </View>

                )}

                {item.item.selected != 1 && (
                       <View style = {{margin :10 ,height :50,backgroundColor:'white',padding:5,alignSelf: 'center',
                        borderColor:'#bfbfbf',borderWidth:1.5,
                        borderRadius:10,justifyContent:'center'}}>

                    <Text style = {{fontSize: 14,alignSelf: 'center',color:'#bfbfbf',paddingLeft:15, paddingRight:15,fontFamily:'Konnect-Medium',} }>
                        {item.item.name}
                    </Text>
                    </View>
                )}
            </TouchableOpacity>
        )
    }


    _renderItems = ({item,index}) => {

        return (

            <TouchableOpacity onPress={() => this.selectedFirst(item)
            }>
                <View style={{ flex: 1 ,marginLeft : 5,width:window.width - 10, backgroundColor: 'white',marginTop: 10,marginBottom:10,borderRadius:10}}>
                    <View style = {{flexDirection:'row',width :'100%'}}>
                        <Image style = {{width :60 ,height :60,borderRadius: 30,margin:10}}
                               source={{uri:item.image}}/>

                        <View style = {{width :window.width -120, alignSelf:'center'}}>
                                <Text style={{marginLeft : 5,fontSize : 18,color :'#3A3A3A',fontFamily:'Konnect-Medium',width :window.width -120,}}>

                                    {item.name}
                                </Text>

                            <View style = {{flexDirection:'row',marginBottom:4, marginTop:5}}>
                                <Image style = {{width :20 ,height :20,resizeMode:'contain', marginTop:-2}}
                                       source={require('./location.png')}/>

                                <Text style={{marginLeft : 5,fontSize : 12,color :'#8F8F8F',fontFamily:'Konnect-Medium',}}>

                                    {item.lat_long_address}
                                </Text>

                            </View>
                        </View>

                    </View>

                </View>

            </TouchableOpacity>
        )
    }

    render() {

        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}
                                       size="large" color='#800000' />
                </View>
            )
        }
        return (

                <View style={styles.container}>
            <Header navigation={this.props.navigation}
                headerName={'HEALTHCARE CENTRES'}/>
{/*                    <View style = {{margin :10,width:window.width - 20 ,height:35,borderRadius:20,flexDirection:'row',backgroundColor:'white',}}>

                        <Image style = {{width :18 ,height: 18,alignSelf:'center',resizeMode: 'contain',marginLeft:13}}
                               source={require('./search.png')}/>

                        <TextInput style={{marginLeft:10 ,width:window.width - 100}}
                                   placeholderTextColor='rgba(0, 0, 0, 0.4)'
                                   onChangeText={(text) => this.setState({height:text})

                                   } placeholder={"Search"}/>
                    </View>
*/}

                            <FlatList style= {{flexGrow:0, backgroundColor:'white'}}
                                      data={this.state.data}
                                      horizontal
                                      showsHorizontalScrollIndicator={false}
                                      keyExtractor = { (item, index) => index.toString() }
                                      extraData={this.state}
                                      renderItem={this._renderItemCateg}
                            />


                       <View style = {{flexDirection:'row',width:'70%', margin:10}}>

                        <Image style = {{width :20 ,height: 20,alignSelf:'center',resizeMode: 'contain',marginLeft:10, }}
                               source={require('./location.png')}/>

                        <Text style = {{color:'black',fontFamily:'Konnect-Medium',fontSize:16,marginTop:4, marginLeft:5}}>
                            {this.state.texts}
                        </Text>
                            <TouchableOpacity onPress={()=>this.setModalVisible(true)}>

                                <Image style = {{width :14 ,height: 14,alignSelf:'center',resizeMode: 'contain',marginLeft:10, marginTop:6}}
                                       source={require('./drop.png')}/>
                            </TouchableOpacity>
                        </View>


                    <FlatList style= {{height:window.height- 140}}
                              data={this.state.results}
                              numColumns={1}
                              keyExtractor = { (item, index) => index.toString() }
                              renderItem={this._renderItems}
                    />

                    <Modal
                        animationType="slide"
                        transparent={true}
                        visible={this.state.modalVisible}
                        onRequestClose={() => {
//             Alert.alert('Modal has been closed.');
                            this.setModalVisible(!this.state.modalVisible)
                        }}>
                        <View style={{
                            flex: 1,
                            flexDirection: 'column',
                            justifyContent: 'center',backgroundColor: 'rgba(0, 0, 0, 0.5)',
                            alignItems: 'center'}}>
                            <View style={{
                                width: 300,backgroundColor: 'white',
                                height: 300}}>
                                <View style={{width: '95%', margin: 10}}>
                                    <Text style={{fontSize: 30, color:'black', fontFamily: 'Konnect-Regular', borderBottomWidth: 1, borderBottomColor: '#bfbfbf'}}>Select State</Text>
                    <FlatList style= {{flexGrow:0,margin:8,marginBottom:50}}
                              data={this.state.resultstates}
                              numColumns={1}
                              keyExtractor = { (item, index) => index.toString() }
                              renderItem={this._renderItemsstates}
                    />




                                </View>


                            </View>

                        </View>
                    </Modal>


                </View>

        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    container: {
        flex:1,
        backgroundColor :'#f1f1f1',

    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },
})
