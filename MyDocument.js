import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Alert,
    TouchableOpacity,
    TextInput,
    Image,
    ImageBackground,
    Linking,
    FlatList,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import Header from './Header.js';
import React, {Component} from 'react';
import Button from 'react-native-button';
const GLOBAL = require('./Global');
const window = Dimensions.get('window');


class MyDocument extends React.Component {
    state = {
        name :'',
        email:'',
        phone :'',
        company :'',
        response :[],
        responses :[],
        path :'',
        loading:false,results:[],
        visible:false,a_details:'',
            moviesList :[
            {
                back :require('./healthcares.png'),
                title :'Healthcare Uploads',
                image:require('./healthcare.png')
            },
            {
                back :require('./hc_backss.png'),
                title :'My Uploads',
                image:require('./doctor.png')
            },],
    };


    static navigationOptions = ({ navigation }) => {
        return {
               header: () => null,

        }
    }

    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }


    componentDidMount(){

        const url = GLOBAL.BASE_URL + 'list_upload_images_by_user'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "user_id": GLOBAL.user_id,


            }),
        }).then((response) => response.json())
            .then((responseJson) => {




                if (responseJson.status == true) {
                    this.setState({responses: responseJson.list})
                    this.setState({path: responseJson.path})


                } else {
                    this.setState({results: []})
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });


    }

    selectedFirst = (index)=> {
        if (index == 0){
            this.props.navigation.navigate('UploadHealthcare')
        }else if  (index == 1){
            this.props.navigation.navigate('UploadPersonal')
        }

    }


        renderRowItem1 = (itemData) => {
        return (
            <TouchableOpacity onPress={() => this.selectedFirst(itemData.index)
            }>
            <View   style  = {{width:window.width - 8,margin:4, height:200,borderRadius: 30,shadowColor: "#000",
                shadowOffset: {
                    width: 0,
                    height: 2,
                },
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
               }}
                    >
            <Image source={itemData.item.image}
                             style  = {{width:window.width - 12, height:'100%',borderRadius:22
                              }}

            />
                <Image source={itemData.item.back}
                       style  = {{width:60, height:60,marginTop: -100,marginLeft:10,resizeMode:'contain'
                       }}

                />

                <Text style = {{fontSize:18,margin:10,fontFamily:'Konnect-Regular',color:'white'}}>
                    {itemData.item.title}

                </Text>

            </View>
            </TouchableOpacity>

        )
    }


    getDirections=()=>{
        var lat= GLOBAL.appointment_details.doctor_lat
        var lot= GLOBAL.appointment_details.doctor_long

        var url = `https://www.google.com/maps?saddr=My+Location&daddr=`+lat+','+lot;
//    alert(url)
        Linking.openURL(url);


    }

    clickResc=()=>{

        this.props.navigation.navigate('AppointmentResc')
    }

    renderItems=({item}) => {
        var path = this.state.path + item.image
        return(
            <TouchableOpacity onPress={() => Linking.openURL(path)
            }>
                <View style={{flexDirection: 'row', marginTop:2,marginLeft:2,borderBottomColor:'grey',borderBottomWidth:1,marginBottom:4}}>

                    <Image source={{uri :path}}
                           style  = {{width:window.width-10, height:200,marginTop: 3,marginLeft:5}}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    renderItem=({item}) => {
        return(
            <View style={{flexDirection: 'row', marginTop:2,marginLeft:2,borderBottomColor:'grey',borderBottomWidth:1,marginBottom:4}}>

                <Text style={{color:'grey', fontSize:14,fontFamily:'Poppins-Regular',margin:8}}>{item.test_name}</Text>

            </View>
        );
    }

    render() {

        return(
        <View>
        <Header navigation={this.props.navigation}
                headerName={'MY DOCUMENTS'}/>

            <ScrollView>

                <View style={{width : Dimensions.get('window').width,height: Dimensions.get('window').height, flexDirection:'column'}}>
                <FlatList style = {{marginTop:5,}}
                    data={this.state.moviesList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={this.renderRowItem1}
                    extraData={this.state}
                />

{/*                    <FlatList
                        data={this.state.responses}
                        keyExtractor = { (item, index) => index.toString() }
                        renderItem={this.renderItems}
                    />
*/}

                </View>

            </ScrollView>
         </View>

        );
    }
}

export default MyDocument;