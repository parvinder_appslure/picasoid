import React, {Component} from 'react';
import {
    StyleSheet,
    Text,
    TextInput,
    View,
    Image,
    Alert,
    Modal,
    FlatList,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage, ScrollView
} from 'react-native';
import Button from 'react-native-button';
import { AnimatedCircularProgress } from 'react-native-circular-progress';
const window = Dimensions.get('window');
const GLOBAL = require('./Global');
import { TextField } from 'react-native-material-textfield';
type Props = {};
import StarRating from 'react-native-star-rating';
import moment from 'moment';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export default class Review extends Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            recognized: '',
            started: '',
            text :'',
            results: [],
            practice:[],
            avg_rat:0,
            ratinglist:[],
            rate_type:'',

        };

    }

    componentWillUnmount() {

    }

    setModalVisible=(visible,get)=> {
        this.setState({text:get})
        this.setState({modalVisible: visible});
    }



    static navigationOptions = ({ navigation }) => {
        return {
            header: () => null,
            title: 'BOOKING APPOINTMENT',
            headerTitleStyle :{textAlign: 'center',alignSelf:'center',color :'black'},
            headerStyle:{
                backgroundColor:'white',
            },
            headerTintColor :'#800000',
            animations: {
                setRoot: {
                    waitForRender: false
                }
            }
        }
    }



    showLoading() {
        this.setState({loading: true})
    }


    hideLoading() {
        this.setState({loading: false})
    }
    _renderItems = ({item,index}) => {


        return (
            <View>
                <Text style={{fontSize:15, color:'#grey'}}>{item.hospital_name}</Text>
                <Text style={{fontSize:15, color:'#grey'}}>{item.hospital_address}</Text>
            </View>


        )
    }



    componentDidMount(){
            this.getRatings()
            this.getReviews()
    }

    getRatings=()=>{
        const url =   GLOBAL.BASE_URL +  'list_rating'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "user_id": GLOBAL.user_id,
                "doctor_id": GLOBAL.appointmentArray.id,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson))
                if (responseJson.status == true) {
                    this.setState({ratinglist: responseJson.list})

                }else{
                    this.setState({ratinglist: []})                    
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

    getReviews=()=>{
        const url = GLOBAL.BASE_URL +  'doctor_review'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },


            body: JSON.stringify({
                "id":GLOBAL.appointmentArray.id,
            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.status == true) {
                    this.setState({avg_rat:parseInt(responseJson.avg_rat)})
                    this.setState({rate_type:responseJson.rate_type})
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });        
    }

    confDelete=(item)=>{
//        alert(item.id)
        const url =   GLOBAL.BASE_URL +  'delete_rating'

        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "id": item.id

            }),
        }).then((response) => response.json())
            .then((responseJson) => {
                console.log(JSON.stringify(responseJson))
                if (responseJson.status == true) {
                    alert('Review deleted successfully!')
                    this.getRatings()
                }
            })
            .catch((error) => {
                console.error(error);
                this.hideLoading()
            });

    }

askDelete =(item)=>{
        Alert.alert('Delete Review','Are you sure you want to delete this review?',
            [{text:"Cancel"},
                {text:"Yes", onPress:()=>this.confDelete(item)
                },
            ],
            {cancelable:false}
        )
}


renderItem=({item,index})=>{
    var rdate = item.date
    var rdates;
    rdates= moment(rdate).format('DD-MM-YYYY')
    return(
   <View style={{backgroundColor:'white',color :'white',flexDirection:'row' , margin: 10, shadowColor: '#000',width:window.width-20,
            shadowOffset: { width: 0, height: 1 },
            shadowOpacity: 0.6,
            shadowRadius: 2,
            elevation: 4,
            height:'auto',
            borderRadius:5  }}>

            <View style={{flexDirection:'column', marginTop:5, marginLeft:10, marginRight:10}}>
    {item.is_delete==1 && (
            <Text style={{color:'black', fontFamily:"Konnect-Medium",fontSize:15}}>You</Text>

    )}
    {item.is_delete!=1 && (
                <Text style={{color:'black', fontFamily:"Konnect-Medium",fontSize:15}}>Certified User</Text>

    )}                

    <StarRating containerStyle={{width:'40%', marginTop:5, marginBottom:5}}
        disabled={true}
        maxStars={5}
        fullStarColor={'#800000'}        
        starSize={22}        
        rating={parseInt(item.rating)}
        selectedStar={(rating) => this.onStarRatingPress(rating)}
      />

            <Text style={{color:'black',fontFamily:"Konnect-Regular", fontSize:16, color:'#800000', marginBottom:5}}>{item.review}</Text>
                <Text style={{color:'grey', fontFamily:"Konnect-Regular",fontSize:12}}>Posted on: {rdates}</Text>

    </View>
    {item.is_delete==1 && (
    <TouchableOpacity style={{width:25, height:25, position:'absolute', top:5, right:5}} onPress={()=> this.askDelete(item)}>
    <Image style={{width:25, height:25, resizeMode:'contain',}} source={require('./trash.png')}/>
    </TouchableOpacity>
    )}
    </View>
)
}
    render() {
        if(this.state.loading){
            return(
                <View style={styles.container}>
                    <ActivityIndicator style = {styles.loading}
                                       size="large" color='#800000' />
                </View>
            )
        }
        return (
           <View style = {{flex:1}}>


               <View style = {{flexDirection:'row',width:window.width - 20,alignSelf:'center',marginTop:10,}}>

                   <View style = {{marginLeft:-5,backgroundColor:'white',justifyContent:'center',alignItems:'center',width:window.width/2,
                       borderRadius: 2,
                       borderColor: 'grey',
                       borderBottomWidth: 0,
                       shadowColor: '#000',
                       shadowOffset: { width: 0, height: 1 },
                       shadowOpacity: 0.8,
                       shadowRadius: 2}}>
                   <AnimatedCircularProgress
                       size={100}
                       width={2}
                       fill={this.state.avg_rat}
                       tintColor="#800000"
                       backgroundColor="grey">
                       {
                           (fill) => (
                               <Text style = {{fontSize:30,color:'#800000'}}>
                                   { this.state.avg_rat } %
                               </Text>
                           )
                       }
                   </AnimatedCircularProgress>

                   </View>

                   <View style = {{backgroundColor:'#800000',width:window.width - 200,marginLeft:10}}>

                       <Text style = {{marginLeft:20,fontSize: 20,color: 'white',marginTop:8,fontFamily:'Konnect-Regular'}}>
                           { this.state.avg_rat } / 100

                       </Text>

                       <Text style = {{marginLeft:20,fontSize: 12,color: 'white',fontFamily:'Konnect-Regular',marginTop:4}}>
                          Rating Type : {this.state.rate_type}

                       </Text>
                       <Text style = {{marginLeft:20,fontSize: 12,color: 'white',fontFamily:'Konnect-Regular',marginTop:4}}>
                           Total 2 People Reviewed

                       </Text>
                   </View>

               </View>
                <View style={{flexDirection:'row', marginLeft:10,marginTop:10}}>
                    <Image style={{width:20, height:20, resizeMode:'contain', marginTop:3}} source={require('./dre.png')}/>
                    <View style={{flexDirection:'column', marginLeft:30}}>
                        <Text style={{fontSize:15, color:'black',fontFamily:'Konnect-Medium'}}>Reviews</Text>
                    </View>
                </View>
                {this.state.ratinglist.length == 0 && (
                        <Text style={{fontSize:11, color:'black',fontFamily:'Konnect-Regular', marginTop:10, marginLeft:10}}>
                        No Reviews posted yet!
                        </Text>

                )}

                {this.state.ratinglist.length!=0 &&(
                <FlatList
                    data={this.state.ratinglist}
                    keyExtractor={(item, index) => index.toString() }
                    renderItem={this.renderItem}
                    extraData={this.state}
                />)}

           </View>
        );
    }
}

const styles = StyleSheet.create({
    wrapper: {
    },
    container: {

        backgroundColor :'#f1f1f1',

    },
    loading: {
        position: 'absolute',
        left: window.width/2 - 30,

        top: window.height/2,

        opacity: 0.5,

        justifyContent: 'center',
        alignItems: 'center'
    },
})
